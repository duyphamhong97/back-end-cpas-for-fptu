﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IStudentService
    {
        void Create(Student entity);
        void Update(Student entity);
        void Delete(Student entity);
        Student Get(Expression<Func<Student, bool>> predicated, params Expression<Func<Student, object>>[] includes);
        IQueryable<Student> GetAll(params Expression<Func<Student, object>>[] includes);
    }
}
