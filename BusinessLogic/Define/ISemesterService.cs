﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface ISemesterService
    {
        void Create(Semester entity);
        void Update(Semester entity);
        void Delete(Semester entity);
        Semester Get(Expression<Func<Semester, bool>> predicated, params Expression<Func<Semester, object>>[] includes);
        IQueryable<Semester> GetAll(params Expression<Func<Semester, object>>[] includes);
    }
}
