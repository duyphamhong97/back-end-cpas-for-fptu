﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface ITechniqueService
    {
        void Create(Technique entity);
        void Update(Technique entity);
        void Delete(Technique entity);
        Technique Get(Expression<Func<Technique, bool>> predicated, params Expression<Func<Technique, object>>[] includes);
        IQueryable<Technique> GetAll(params Expression<Func<Technique, object>>[] includes);
    }
}
