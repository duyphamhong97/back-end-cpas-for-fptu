﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IAdvisorTopicService
    {
        void Create(AdvisorTopic entity);
        void Update(AdvisorTopic entity);
        void Delete(AdvisorTopic entity);
        AdvisorTopic Get(Expression<Func<AdvisorTopic, bool>> predicated, params Expression<Func<AdvisorTopic, object>>[] includes);
        IQueryable<AdvisorTopic> GetAll(params Expression<Func<AdvisorTopic, object>>[] includes);
    }
}
