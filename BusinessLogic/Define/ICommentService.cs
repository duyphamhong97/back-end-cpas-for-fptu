﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface ICommentService
    {
        void Create(Comment entity);
        void Update(Comment entity);
        void Delete(Comment entity);
        Comment Get(Expression<Func<Comment, bool>> predicated, params Expression<Func<Comment, object>>[] includes);
        IQueryable<Comment> GetAll(params Expression<Func<Comment, object>>[] includes);
    }
}
