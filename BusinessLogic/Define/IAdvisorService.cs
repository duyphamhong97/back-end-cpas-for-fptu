﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IAdvisorService
    {
        void Create(Advisor entity);
        void Update(Advisor entity);
        void Delete(Advisor entity);
        Advisor Get(Expression<Func<Advisor, bool>> predicated, params Expression<Func<Advisor, object>>[] includes);
        IQueryable<Advisor> GetAll(params Expression<Func<Advisor, object>>[] includes);
    }
}
