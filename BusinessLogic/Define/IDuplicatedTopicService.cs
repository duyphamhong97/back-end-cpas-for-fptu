﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IDuplicatedTopicService
    {
        void Create(DuplicatedTopic entity);
        void AddRange(ICollection<DuplicatedTopic> entities);
        void Update(DuplicatedTopic entity);
        void Delete(DuplicatedTopic entity);
        DuplicatedTopic Get(Expression<Func<DuplicatedTopic, bool>> predicated, params Expression<Func<DuplicatedTopic, object>>[] includes);
        IQueryable<DuplicatedTopic> GetAll(params Expression<Func<DuplicatedTopic, object>>[] includes);
    }
}
