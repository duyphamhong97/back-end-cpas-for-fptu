﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IStudentGroupService
    {
        void Create(StudentGroup entity);
        void Update(StudentGroup entity);
        void Delete(StudentGroup entity);
        StudentGroup Get(Expression<Func<StudentGroup, bool>> predicated, params Expression<Func<StudentGroup, object>>[] includes);
        IQueryable<StudentGroup> GetAll(params Expression<Func<StudentGroup, object>>[] includes);
    }
}
