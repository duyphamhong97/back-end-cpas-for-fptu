﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IWeightPointService
    {
        void Create(WeightPoint entity);
        void Update(WeightPoint entity);
        void Delete(WeightPoint entity);
        WeightPoint Get(Expression<Func<WeightPoint, bool>> predicated, params Expression<Func<WeightPoint, object>>[] includes);
        IQueryable<WeightPoint> GetAll(params Expression<Func<WeightPoint, object>>[] includes);
    }
}
