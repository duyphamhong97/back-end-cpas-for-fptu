﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IGradeTypeService
    {
        void Create(GradeType entity);
        void Update(GradeType entity);
        void Delete(GradeType entity);
        GradeType Get(Expression<Func<GradeType, bool>> predicated, params Expression<Func<GradeType, object>>[] includes);
        IQueryable<GradeType> GetAll(params Expression<Func<GradeType, object>>[] includes);
    }
}
