﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IDepartmentProgramService
    {
        void Create(DepartmentProgram entity);
        void Update(DepartmentProgram entity);
        void Delete(DepartmentProgram entity);
        DepartmentProgram Get(Expression<Func<DepartmentProgram, bool>> predicated, params Expression<Func<DepartmentProgram, object>>[] includes);
        IQueryable<DepartmentProgram> GetAll(params Expression<Func<DepartmentProgram, object>>[] includes);
    }
}
