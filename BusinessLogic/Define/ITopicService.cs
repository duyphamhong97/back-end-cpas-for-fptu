﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    public interface ITopicService
    {
        void Create(Topic entity);
        void Update(Topic entity, IEnumerable<int> techniques, IEnumerable<AdvisorTopic> advisors);
        void Update(Topic entity);
        void Delete(Topic entity);
        Topic Get(Expression<Func<Topic, bool>> predicated, params Expression<Func<Topic, object>>[] includes);
        IQueryable<Topic> GetAll(params Expression<Func<Topic, object>>[] includes);
    }
}
