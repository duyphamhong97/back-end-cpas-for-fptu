﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IGradeService
    {
        void Create(Grade entity);
        void Update(Grade entity);
        void Delete(Grade entity);
        Grade Get(Expression<Func<Grade, bool>> predicated, params Expression<Func<Grade, object>>[] includes);
        IQueryable<Grade> GetAll(params Expression<Func<Grade, object>>[] includes);
    }
}
