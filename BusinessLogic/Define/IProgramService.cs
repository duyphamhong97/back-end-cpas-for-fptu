﻿namespace BusinessLogic.Define
{
    using DataAccess.Entities;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IProgramService
    {
        void Create(Program entity);
        void Update(Program entity);
        void Delete(Program entity);
        Program Get(Expression<Func<Program, bool>> predicated, params Expression<Func<Program, object>>[] includes);
        IQueryable<Program> GetAll(params Expression<Func<Program, object>>[] includes);
    }
}
