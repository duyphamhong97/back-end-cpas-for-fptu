﻿namespace BusinessLogic.Implement
{
    using BusinessLogic.Define;
    using DataAccess.Entities;
    using DataAccess.Repositories;

    public class GradeTypeService : _BaseService<GradeType>, IGradeTypeService
    {
        public GradeTypeService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
