﻿namespace BusinessLogic.Implement
{
    using BusinessLogic.Define;
    using DataAccess.Entities;
    using DataAccess.Repositories;

    public class DepartmentProgramService : _BaseService<DepartmentProgram>, IDepartmentProgramService
    {
        public DepartmentProgramService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
