﻿namespace BusinessLogic.Implement
{
    using BusinessLogic.Define;
    using DataAccess.Entities;
    using DataAccess.Repositories;
    using System.Collections.Generic;
    using System.Linq;
    using System.Transactions;

    public class TopicService : _BaseService<Topic>, ITopicService
    {
        private readonly IRepository<Technique> _techniqueRepository;
        private readonly IRepository<TopicTechnique> _topicTechniqueRepository;
        private readonly IRepository<AdvisorTopic> _advisorTopicRepository;

        public TopicService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _techniqueRepository = unitOfWork.GetRepository<Technique>();
            _topicTechniqueRepository = unitOfWork.GetRepository<TopicTechnique>();
            _advisorTopicRepository = unitOfWork.GetRepository<AdvisorTopic>();
        }

        public override void Create(Topic entity)
        {
            // Check duplicate

            base.Create(entity);
        }

        public void Update(Topic topic, IEnumerable<int> techniques, IEnumerable<AdvisorTopic> advisors)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                //tempory delete
                var currentTechnique = topic.TopicTechniques.ToList();
                currentTechnique.ForEach(x => _topicTechniqueRepository.Delete(x));

                techniques.ToList().ForEach(x =>
                {
                    var newTechnique = new TopicTechnique { TechniqueId = x, TopicId = topic.Id };

                    int index = currentTechnique.IndexOf(newTechnique);
                    if (index < 0)
                    {
                        _topicTechniqueRepository.Create(newTechnique);
                    }//add new techniques
                    else
                    {
                        _topicTechniqueRepository.Unchange(currentTechnique[index]);
                    }//cancel delete operation
                });

                var currentAdvisor = topic.AdvisorTopics.ToList();
                currentAdvisor.ForEach(x => _advisorTopicRepository.Delete(x));

                advisors.ToList().ForEach(x =>
                {
                    var newAdvisor = new AdvisorTopic { AdvisorId = x.AdvisorId, TopicId = topic.Id, Role = x.Role };

                    int index = currentAdvisor.IndexOf(newAdvisor);
                    if (index < 0)
                    {
                        _advisorTopicRepository.Create(newAdvisor);
                    }
                    else
                    {
                        _advisorTopicRepository.Unchange(currentAdvisor[index]);
                    }
                });


                iUnitOfWork.Save();
                scope.Complete();
            }
        }
    }
}
