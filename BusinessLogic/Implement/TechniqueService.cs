﻿namespace BusinessLogic.Implement
{
    using BusinessLogic.Define;
    using DataAccess.Entities;
    using DataAccess.Repositories;

    public class TechniqueService : _BaseService<Technique>, ITechniqueService
    {
        public TechniqueService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
