﻿namespace BusinessLogic.Implement
{
    using BusinessLogic.Define;
    using DataAccess.Entities;
    using DataAccess.Repositories;

    public class GradeService : _BaseService<Grade>, IGradeService
    {
        public GradeService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
