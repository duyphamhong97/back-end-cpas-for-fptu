﻿namespace BusinessLogic.Implement
{
    using BusinessLogic.Define;
    using DataAccess.Entities;
    using DataAccess.Repositories;

    public class ProgramService : _BaseService<Program>, IProgramService
    {
        public ProgramService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
