﻿namespace BusinessLogic.Implement
{
    using BusinessLogic.Define;
    using DataAccess.Entities;
    using DataAccess.Repositories;

    public class StudentGroupService : _BaseService<StudentGroup>, IStudentGroupService
    {
        public StudentGroupService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
