﻿namespace BusinessLogic.Implement
{
    using BusinessLogic.Define;
    using DataAccess.Entities;
    using DataAccess.Repositories;

    public class WeightPointService : _BaseService<WeightPoint>, IWeightPointService
    {
        public WeightPointService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
