﻿namespace BusinessLogic.Implement
{
    using BusinessLogic.Define;
    using DataAccess.Entities;
    using DataAccess.Repositories;

    public class AdvisorTopicService : _BaseService<AdvisorTopic>, IAdvisorTopicService
    {
        public AdvisorTopicService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
