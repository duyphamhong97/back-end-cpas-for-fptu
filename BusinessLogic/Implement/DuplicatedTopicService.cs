﻿namespace BusinessLogic.Implement
{
    using System.Collections.Generic;
    using BusinessLogic.Define;
    using DataAccess.Entities;
    using DataAccess.Repositories;

    public class DuplicatedTopicService : _BaseService<DuplicatedTopic>, IDuplicatedTopicService
    {
        public DuplicatedTopicService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public void AddRange(ICollection<DuplicatedTopic> entities)
        {
            foreach (var item in entities)
            {
                this.Create(item);
            }
        }
    }
}
