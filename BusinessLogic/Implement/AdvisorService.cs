﻿namespace BusinessLogic.Implement
{
    using BusinessLogic.Define;
    using DataAccess.Entities;
    using DataAccess.Repositories;

    public class AdvisorService : _BaseService<Advisor>, IAdvisorService
    {
        public AdvisorService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
