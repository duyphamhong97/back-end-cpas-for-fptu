﻿namespace BusinessLogic.Implement
{
    using BusinessLogic.Define;
    using DataAccess.Entities;
    using DataAccess.Repositories;

    public class SemesterService : _BaseService<Semester>, ISemesterService
    {
        public SemesterService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
