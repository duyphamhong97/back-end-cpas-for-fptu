﻿using BusinessLogic.Define;
using CapstoneUI.Extensions;
using CapstoneUI.Firebase;
using CapstoneUI.Utils;
using CapstoneUI.ViewModels;
using DataAccess.Entities;
using DocumentFormat.OpenXml.Spreadsheet;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Drawing;
using DataAccess.Resources;

namespace CapstoneUI.Controllers
{
    public class TrainingStaffController : BaseController
    {
        private readonly ITopicService _topicService;
        private readonly ISemesterService _semesterService;
        private readonly IGradeService _gradeService;
        private readonly IGradeTypeService _gradeTypeService;
        private readonly IStudentGroupService _groupService;
        private readonly IStudentService _studentService;
        private readonly IProgramService _programService;
        private readonly IAdvisorService _advisorService;
        private readonly IAccountService _accountService;

        public TrainingStaffController(ITopicService topicService, IStudentService studentService,
            IProgramService programService, IStudentGroupService groupService, IGradeService gradeService,
            IGradeTypeService gradeTypeService, IAdvisorService advisorService, ISemesterService semesterService,
            IAccountService accountService)
        {
            _topicService = topicService;
            _studentService = studentService;
            _programService = programService;
            _groupService = groupService;
            _gradeService = gradeService;
            _advisorService = advisorService;
            _gradeTypeService = gradeTypeService;
            _semesterService = semesterService;
            _accountService = accountService;
        }

        [HttpPost, Route("api/ts/topic/status")]
        public IHttpActionResult ChangeTSStatus(List<TopicTSChangeStatusVM> viewModels)
        {
            try
            {
                foreach (var viewModel in viewModels)
                {
                    var topic = _topicService.Get(_ => _.Id == viewModel.Id);

                    if (topic == null) return NotFound();
                    if ((Status)viewModel.Status != Status.Published && (Status)viewModel.Status != Status.Unpublished)
                        return BadRequest("Invalid status");

                    topic.Status = (Status)viewModel.Status;
                    _topicService.Update(topic);
                }
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("api/ts/topic")]
        public IHttpActionResult GetTopicByTrainingStaff(int programId, Status status)
        {
            try
            {
                if (status != Status.Published && status != Status.Unpublished)
                {
                    return BadRequest("Status is invalid!");
                }

                ICollection<TopicViewVM> result = null;

                var p = _programService.Get(_ => _.Id == programId);
                if (p == null) return BadRequest("Program not found!");

                result = ModelMapper.ConvertToViewModel(
                _topicService.GetAll(_ => _.TopicTechniques.Select(__ => __.Technique),
                _ => _.AdvisorTopics.Select(__ => __.Advisor.Account), _ => _.Program)
                .Where(_ => (_.Status == status)
                && _.ProgramId == programId).ToList());

                return Ok(result);

            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut]
        [Route("api/ts/student/send-dh")]
        public async Task<IHttpActionResult> SendStudentDH(List<string> viewModel)
        {
            try
            {
                var list = _studentService.GetAll().Where(_ => viewModel.Any(z => z == _.Code)).ToList();
                foreach (var item in list)
                {
                    item.Status = StudentStatus.Grouping;
                    item.GradeStatus = GradeStatus.Studying;
                    _studentService.Update(item);

                    _gradeTypeService.GetAll().ToList().ForEach(_ =>
                    {
                        _gradeService.Create(new Grade
                        {
                            StudentCode = item.Code,
                            GradeTypeId = _.Id,
                        });
                    });

                }

                if (list.Count > 0)
                {
                    int programId = list.FirstOrDefault().ProgramId;
                    var dhead = _accountService.Get(_ => _.AccountRoleId == 5);

                    await FirebaseDatabase.GetFirebaseDatabase().sendMessage(new FireBaseVM
                    {
                        IsReading = false,
                        UserId = dhead.Code,
                        CountDuplicate = list.Count,
                        Status = NotiStatus.TS_SEND_STUDENT_DH,
                        CreatedDate = DateTimeOffset.Now.ToUnixTimeMilliseconds()
                    });
                }

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }



        [HttpGet, Route("api/ts/schedule/groupStudent")]
        public IHttpActionResult GetScheduleGroup()
        {
            try
            {
                var semester = _semesterService.Get(_ => _.EndDate > DateTimeOffset.Now);
                
                var result = _groupService.GetAll(_ => _.Students.Select(s => s.Program), 
                    _ => _.Students.Select(z => z.Account))
                    .Where(_ => _.Students.Any(s => s.Status == StudentStatus.GroupDone)
                    && _.TopicId == null && _.SemesterId == semester.Id)
                    .OrderBy(_ => _.Name).ToList()
                    .Select(_ => new GroupScheduleVM
                    {
                        Id = _.Id,
                        Name = _.Name,
                        ProgramName = _.Students.FirstOrDefault().Program.ShortName,
                        Students = _.Students.Select(s => new StudentGroupVM
                        {
                            Code = s.Code,
                            FullName = s.Account.FullName,
                            IsLeader = s.LeaderCode == null ? true : false
                        }).ToList(),
                        Duration = (long)_.Duration.TotalMilliseconds,
                        EnrollTime = _.EnrollTime.ToUnixTimeMilliseconds()
                    });
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/ts/download-all")]
        public IHttpActionResult GetAllTopicsAndZip()
        {
            string text = "<html><body>abc</body></html>";
            try
            {
                List<ZipfileUtils.ZipFileModel> zipFileVMs = new List<ZipfileUtils.ZipFileModel>();
                for (int i = 1; i < 10; i++)
                {
                    string name = "topics_" + i;
                    byte[] result = ExportWord.HtmlToWord(text);
                    zipFileVMs.Add(new ZipfileUtils.ZipFileModel(name, result));
                }
                return ZipfileUtils.ZipFile(zipFileVMs);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/ts/export-word")]
        public IHttpActionResult GetAllTopicsAndZip(string topics) //1,2,3
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest();

                List<ZipfileUtils.ZipFileModel> zipFileVMs = new List<ZipfileUtils.ZipFileModel>();
                var topicIds = topics.Split(',');
                foreach (var idString in topicIds)
                {
                    var id = int.Parse(idString);
                    var topic = _topicService.Get(_ => _.Id == id, _ => _.AdvisorTopics.Select(s => s.Advisor.Account),
                _ => _.StudentGroups.Select(s => s.Students.Select(z => z.Account)));
                    var advisor = topic.AdvisorTopics.FirstOrDefault(_ => _.Role == "Supervisor").Advisor;

                    var name = advisor.Account.FullName.ToLower().UTF8toASCII().Split(' ');
                    var ful = name.Aggregate("", (re, it) => re + it.ElementAt(0));
                    string advisorSName = name.Last() + ful.Substring(0, ful.Length - 1);

                    var html = ExportWord.ToHtml(topic, advisor, topic.StudentGroups.LastOrDefault());

                    string filename = topic.Name_En + "_" + advisorSName.ToUpper();
                    byte[] result = ExportWord.HtmlToWord(html);
                    zipFileVMs.Add(new ZipfileUtils.ZipFileModel(filename, result));
                }

                return ZipfileUtils.ZipFile(zipFileVMs);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }



        [HttpGet, Route("api/topic/{id}/export-word")]
        public IHttpActionResult ReadFile(int id)
        {
            var topic = _topicService.Get(_ => _.Id == id, _ => _.AdvisorTopics.Select(s => s.Advisor.Account),
                _ => _.StudentGroups.Select(s => s.Students.Select(z => z.Account)));
            var advisor = topic.AdvisorTopics.FirstOrDefault(_ => _.Role == "Supervisor").Advisor;

            var name = advisor.Account.FullName.ToLower().UTF8toASCII().Split(' ');
            var ful = name.Aggregate("", (re, it) => re + it.ElementAt(0));
            string advisorSName = name.Last() + ful.Substring(0, ful.Length - 1);

            var html = ExportWord.ToHtml(topic, advisor, topic.StudentGroups.LastOrDefault());

            try
            {
                List<ZipfileUtils.ZipFileModel> zipFileVMs = new List<ZipfileUtils.ZipFileModel>();
                string filename = topic.Name_En + "_" + advisorSName.ToUpper();
                byte[] result = ExportWord.HtmlToWord(html);
                zipFileVMs.Add(new ZipfileUtils.ZipFileModel(filename, result));
                return ZipfileUtils.ZipFile(zipFileVMs);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpPost, Route("api/enrollTime")]
        public IHttpActionResult SetEntollTime(int groupId, long time, long duration)
        {
            try
            {
                if (groupId == 0)
                {
                    var gg = _groupService.GetAll();
                    if (gg == null) return NotFound();
                    gg.ToList().ForEach(g =>
                    {
                        g.EnrollTime = DateTimeOffset.FromUnixTimeMilliseconds(time);
                        g.Duration = TimeSpan.FromMilliseconds(duration);
                        _groupService.Update(g);
                    });
                } else
                {
                    var g = _groupService.Get(_ => _.Id == groupId);
                    if (g == null) return NotFound();
                    g.EnrollTime = DateTimeOffset.FromUnixTimeMilliseconds(time);
                    g.Duration = TimeSpan.FromMilliseconds(duration);
                    _groupService.Update(g);
                }
                

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }


        }

        [HttpPost, Route("api/ts/schedule/groupStudent")]
        public async Task<IHttpActionResult> PublishSchedulerVer2(GroupSchedulev2 viewModel)
        {
            try
            {
                //  int programId = 1;
                

                if (viewModel.StartTime >= viewModel.EndTime)
                    return BadRequest("Invalid time");
                if (viewModel.StartTime < DateTimeOffset.Now.ToUnixTimeMilliseconds())
                    return BadRequest("Time is over");

                var startTime = viewModel.StartTime;
                var groups = _groupService.GetAll(_ => _.Students)
                    .Where(_ => _.Students.Any(s => s.Status == StudentStatus.GroupDone // && s.ProgramId == programId
                    )).ToList();

                groups.Shuffle();

                long timeduration = (viewModel.EndTime - viewModel.StartTime) / (long)groups.Count;

                if (timeduration > 86400000)
                {
                    return BadRequest("Duration time should less than 24 hours!");
                }

                foreach (var _ in groups)
                {
                    _.Duration = TimeSpan.FromMilliseconds(timeduration);
                    _.EnrollTime = DateTimeOffset.FromUnixTimeMilliseconds(startTime);
                    startTime = startTime + timeduration;
                    _groupService.Update(_);

                    foreach (var student in _.Students)
                    {
                        Task.Run(async () => await FirebaseDatabase.GetFirebaseDatabase()
                            .SendStudent(student.Code, NotiStatus.TS_SCHEDULE_STUDENT, _.EnrollTime));
                    }
                }

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost, Route("api/ts/announcement"), Authorize]
        public IHttpActionResult SetAnnouncement(AnnouncementVM viewModel)
        {
            try
            {
                Announcement.Authorize_DATA = viewModel.Authorize;
                Announcement.Unauthorize_DATA = viewModel.Unauthorize;
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/ts/announcement")]
        public IHttpActionResult GetAnnouncement()
        {
            try
            {
                return Ok(new {
                    Authorize = Announcement.Authorize_DATA,
                    Unauthorize = Announcement.Unauthorize_DATA
                });
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpPost, Route("api/ts/groupStudent/start-schedule")]
        public IHttpActionResult StartSchedule()
        {
            try
            {
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }



        [HttpGet, Route("api/ts/groupStudent/topic/{semesterId}/{programId}")]
        public IHttpActionResult Topiced(int semesterId, int programId)
        {
            try
            {
                int groupCount = 1;
                var result = _studentService.GetAll(_ => _.StudentGroup.Topic.AdvisorTopics.Select(s => s.Advisor.Account),
                    _ => _.Account)
                    .Where(_ => _.ProgramId == programId && _.StudentGroup.SemesterId == semesterId
                    && _.StudentGroup.TopicId != null).OrderBy(_ => _.StudentGroupId).ThenBy(_ => _.LeaderCode).ToList();

                List<GroupFullVM> list = new List<GroupFullVM>();

                result.ForEach(_ =>
                {
                    if (_.LeaderCode == null)
                    {
                        list.Add(new GroupFullVM
                        {
                            Code = _.Code,
                            FullName = _.Account.FullName,
                            Role = "Leader",
                            AdvisorName = _.StudentGroup.Topic.AdvisorTopics.FirstOrDefault(s => s.Role == "Supervisor").Advisor.Account.FullName,
                            Group = groupCount + "",
                            TopicName = _.StudentGroup.Topic.Name_En,
                            Email = _.Account.Email,
                            Phone = _.Phone
                        });
                        groupCount++;
                    }
                    else
                    {
                        list.Add(new GroupFullVM
                        {
                            Code = _.Code,
                            FullName = _.Account.FullName,
                            Role = "Member",
                            AdvisorName = "",
                            Group = "",
                            TopicName = "",
                            Email = _.Account.Email,
                            Phone = _.Phone
                        });
                    }
                });

                return Ok(list);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        [HttpGet, Route("api/ts/export-excel/{semesterId}/{programId}")]
        public IHttpActionResult ExportExcel(int semesterId, int programId) //1,2,3
        {
            try
            {
                int groupCount = 1;
                var semester = _semesterService.Get(_ => _.EndDate > DateTimeOffset.Now);

                var result = _studentService.GetAll(_ => _.StudentGroup.Topic.AdvisorTopics.Select(s => s.Advisor.Account),
                    _ => _.Account)
                    .Where(_ => _.StudentGroup.SemesterId == semester.Id //  _.ProgramId == programId && 
                    && _.StudentGroup.TopicId != null).OrderBy(_ => _.StudentGroupId).ThenBy(_ => _.LeaderCode).ToList();

                List<GroupFullVM> list = new List<GroupFullVM>();

                result.ForEach(_ =>
                {
                    if (_.LeaderCode == null)
                    {
                        list.Add(new GroupFullVM
                        {
                            Code = _.Code,
                            FullName = _.Account.FullName,
                            Role = "Leader",
                            AdvisorName = _.StudentGroup.Topic.AdvisorTopics.FirstOrDefault(s => s.Role == "Supervisor").Advisor.Account.FullName,
                            Group = groupCount + "",
                            TopicName = _.StudentGroup.Topic.Name_En,
                            Email = _.Account.Email,
                            Phone = _.Phone
                        });
                        groupCount++;
                    }
                    else
                    {
                        list.Add(new GroupFullVM
                        {
                            Code = _.Code,
                            FullName = _.Account.FullName,
                            Role = "Member",
                            AdvisorName = "",
                            Group = "",
                            TopicName = "",
                            Email = _.Account.Email,
                            Phone = _.Phone
                        });
                    }
                });
                Stream stream = null;
                using (var excelPackage = new ExcelPackage(new MemoryStream()))
                {
                    // Add author file Excel
                    excelPackage.Workbook.Properties.Author = "FPT Education";
                    // Add title file Excel
                    excelPackage.Workbook.Properties.Title = "Students";
                    // Add Sheet vào file Excel
                    excelPackage.Workbook.Worksheets.Add("IS Sheet");
                    // Lấy Sheet bạn vừa mới tạo ra để thao tác 
                    var workSheet = excelPackage.Workbook.Worksheets[1];
                    // Đổ data vào Excel file
                    workSheet.Cells[1, 1].LoadFromCollection(list, true, OfficeOpenXml.Table.TableStyles.Dark9);
                    BindingFormatForExcel(workSheet, list);
                    workSheet.Cells.AutoFitColumns();


                    excelPackage.Save();
                    stream = excelPackage.Stream;
                    var buffer = stream as MemoryStream;
                    return new XlsxResult(buffer, Request, @"Students.xlsx");
                }
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        private void BindingFormatForExcel(ExcelWorksheet workSheet, List<GroupFullVM> list)
        {
            using (var range = workSheet.Cells["A1:I1"])
            {
                // Set PatternType
                range.Style.Fill.PatternType = ExcelFillStyle.DarkGray;
                // Set Màu cho Background
                range.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.White);
                // Set Color font
                range.Style.Font.Color.SetColor(System.Drawing.Color.Black);
                // Canh giữa cho các text
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                // Set Font cho text  trong Range hiện tại
                range.Style.Font.SetFromFont(new System.Drawing.Font("Times New Roman", 12));
                range.Style.Font.Bold = true;
                // Set Border
                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }

            using (var range = workSheet.Cells[2, 1, list.Count + 1, 9])
            {
                range.Style.Fill.PatternType = ExcelFillStyle.DarkGray;
                range.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.White);
                range.Style.Font.SetFromFont(new System.Drawing.Font("Times New Roman", 12));
                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }

            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                workSheet.Cells[i + 2, 1].Value = i + 1;
                workSheet.Cells[i + 2, 2].Value = item.Code;
                workSheet.Cells[i + 2, 3].Value = item.FullName;
                workSheet.Cells[i + 2, 4].Value = item.Role;
                workSheet.Cells[i + 2, 5].Value = item.Group;
                workSheet.Cells[i + 2, 6].Value = item.TopicName;
                workSheet.Cells[i + 2, 7].Value = item.AdvisorName;
                workSheet.Cells[i + 2, 8].Value = item.Phone;
                workSheet.Cells[i + 2, 9].Value = item.Email;
                
                if (item.Role.Equals("Leader"))
                {
                    workSheet.Cells[i + 2, 1, i + 2, 9].Style.Font.Bold = true;
                }
            }
        }
        [HttpGet, Route("api/ts/groupStudent/{semesterId}/{programId}")]
        public IHttpActionResult Topicedd(int semesterId, int programId)
        {
            try
            {
                int groupCount = 1;
                var result = _studentService.GetAll(_ => _.StudentGroup.Topic.AdvisorTopics.Select(s => s.Advisor.Account),
                    _ => _.Account)
                    .Where(_ => _.ProgramId == programId && _.StudentGroup.SemesterId == semesterId)
                    .OrderBy(_ => _.StudentGroupId).ThenBy(_ => _.LeaderCode).ToList();

                List<GroupFullVM> list = new List<GroupFullVM>();

                result.ForEach(_ =>
                {
                    if (_.LeaderCode == null)
                    {
                        list.Add(new GroupFullVM
                        {
                            Code = _.Code,
                            FullName = _.Account.FullName,
                            Role = "Leader",
                            Group = groupCount + "",
                        });
                        groupCount++;
                    }
                    else
                    {
                        list.Add(new GroupFullVM
                        {
                            Code = _.Code,
                            FullName = _.Account.FullName,
                            Role = "Member",
                            Group = "",
                        });
                    }
                });

                return Ok(list);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        [HttpGet, Route("api/ts/groupStudent/export-excel/{semesterId}/{programId}")]
        public IHttpActionResult ExportExcelAfterImport(int semesterId, int programId) //1,2,3
        {
            try
            {
                int groupCount = 1;

                var semester = _semesterService.Get(_ => _.EndDate > DateTimeOffset.Now);

                var result = _studentService.GetAll(_ => _.StudentGroup.Topic.AdvisorTopics.Select(s => s.Advisor.Account),
                    _ => _.Program, _ => _.Account)
                    .Where(_ => _.StudentGroup.SemesterId == semester.Id) //_.ProgramId == programId && 
                    .OrderBy(_ => _.StudentGroupId).ThenBy(_ => _.LeaderCode).ToList();

                var programs = _programService.GetAll();

                List<GroupNotFullHelperVM> list = new List<GroupNotFullHelperVM>();

                result.ForEach(_ =>
                {
                    if (_.LeaderCode == null)
                    {
                        list.Add(new GroupNotFullHelperVM
                        {
                            Code = _.Code,
                            FullName = _.Account.FullName,
                            Role = "Leader",
                            Group = groupCount + "",
                            Program = _.Program.ShortName
                        });
                        groupCount++;
                    }
                    else
                    {
                        list.Add(new GroupNotFullHelperVM
                        {
                            Code = _.Code,
                            FullName = _.Account.FullName,
                            Role = "Member",
                            Group = "",
                            Program = _.Program.ShortName
                        });
                    }
                });
                Stream stream = null;
                using (var excelPackage = new ExcelPackage(new MemoryStream()))
                {
                    // Add author file Excel
                    excelPackage.Workbook.Properties.Author = "FPT Education";
                    // Add title file Excel
                    excelPackage.Workbook.Properties.Title = "Students";
                    int sheetIndex = 1;
                    list.GroupBy(_ => _.Program, (program, groupItem) => new { program, groupItem }).ToList()
                        .ForEach(_ =>
                        {
                            var listItem = _.groupItem.Select(z => new GroupNotFullVM
                            {
                                Code = z.Code,
                                FullName = z.FullName,
                                Role = z.Role,
                                Group = z.Group
                            }).ToList();

                            // Add Sheet vào file Excel
                            excelPackage.Workbook.Worksheets.Add(_.program + " groups");
                            // Lấy Sheet bạn vừa mới tạo ra để thao tác 
                            var workSheet = excelPackage.Workbook.Worksheets[sheetIndex];
                            // Đổ data vào Excel file
                            workSheet.Cells[1, 1].LoadFromCollection(listItem, true, OfficeOpenXml.Table.TableStyles.Dark9);
                            BindingFormatForExcelStudentAfterImport(workSheet, listItem);
                            workSheet.Cells.AutoFitColumns();

                            sheetIndex++;
                        });
                    

                    excelPackage.Save();
                    stream = excelPackage.Stream;
                    var buffer = stream as MemoryStream;
                    return new XlsxResult(buffer, Request, @"StudentPublic.xlsx");
                }
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        private void BindingFormatForExcelStudentAfterImport(ExcelWorksheet workSheet, List<GroupNotFullVM> list)
        {
            using (var range = workSheet.Cells["A1:E1"])
            {
                // Set PatternType
                range.Style.Fill.PatternType = ExcelFillStyle.DarkGray;
                // Set Màu cho Background
                range.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.White);
                // Set Color font
                range.Style.Font.Color.SetColor(System.Drawing.Color.Black);
                // Canh giữa cho các text
                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                // Set Font cho text  trong Range hiện tại
                range.Style.Font.SetFromFont(new System.Drawing.Font("Times New Roman", 12));
                range.Style.Font.Bold = true;
                // Set Border
                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            }
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                workSheet.Cells[i + 2, 1].Value = i + 1;
                workSheet.Cells[i + 2, 2].Value = item.Code;
                workSheet.Cells[i + 2, 3].Value = item.FullName;
                workSheet.Cells[i + 2, 4].Value = item.Role;
                workSheet.Cells[i + 2, 5].Value = item.Group;

                using(var range = workSheet.Cells[i + 2, 1, i + 2, 5])
                {
                    range.Style.Fill.PatternType = ExcelFillStyle.DarkGray;
                    range.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.White);
                    range.Style.Font.SetFromFont(new System.Drawing.Font("Times New Roman", 12));
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                }
                

                if (item.Role.Equals("Leader"))
                {
                    workSheet.Cells[i + 2, 1, i + 2, 5].Style.Font.Bold = true;
                }
            }
        }
    }
    public class XlsxResult : IHttpActionResult
    {
        MemoryStream stream;
        string excelFileName;
        HttpRequestMessage httpRequestMessage;
        HttpResponseMessage httpResponseMessage;
        public XlsxResult(MemoryStream data, HttpRequestMessage request, string filename)
        {
            stream = data;
            httpRequestMessage = request;
            excelFileName = filename;
        }
        public System.Threading.Tasks.Task<HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            httpResponseMessage = httpRequestMessage.CreateResponse(HttpStatusCode.OK);
            stream.Position = 0;

            httpResponseMessage.Content = new StreamContent(stream);
            httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = @"StudentGroups.xlsx"
            };
            httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            //httpResponseMessage.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            //httpResponseMessage.Content.Headers.ContentDisposition.FileName = excelFileName;
            //httpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return System.Threading.Tasks.Task.FromResult(httpResponseMessage);
        }
    }
}
