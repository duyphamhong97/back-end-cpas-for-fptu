﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CapstoneUI.Controllers
{
    public class CommonController : BaseController
    {
        public CommonController()
        {

        }

        [HttpGet, Route("api/config")]
        public IHttpActionResult GetClassifyConstant()
        {
            return Ok(new {
                GroupClassifyMin = ConfigurationManager.AppSettings.Get("GroupClassifyMin"),
                GroupClassifyMax = ConfigurationManager.AppSettings.Get("GroupClassifyMax")
            });
        }
    }
}
