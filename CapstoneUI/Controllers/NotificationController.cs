﻿using CapstoneUI.Firebase;
using CapstoneUI.ViewModels;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace CapstoneUI.Controllers
{
    public class NotificationController : BaseController
    {
        [HttpPost]
        public async Task<IHttpActionResult> Post(FireBaseVM viewModel)
        {
            try
            {
                var res = await FirebaseDatabase.GetFirebaseDatabase().sendMessage(viewModel);
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
          
        }
    }
}
