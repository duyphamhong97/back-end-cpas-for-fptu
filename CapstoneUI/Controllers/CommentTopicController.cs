﻿using BusinessLogic.Define;
using CapstoneUI.Extensions;
using CapstoneUI.ViewModels;
using DataAccess.Entities;
using DataAccess.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CapstoneUI.Controllers
{
    public class CommentTopicController : BaseController
    {
        private readonly ICommentService _commentService;
        private readonly IAccountService _accountService;
        private readonly ITopicService  _topicService;
        public CommentTopicController(ICommentService commentService, IAccountService accountService,
            ITopicService topicService)
        {
            _commentService = commentService;
            _accountService = accountService;
            _topicService = topicService;
        }
       
        [HttpGet, Route("api/comment/{topicId}"), Authorize]
        public IHttpActionResult GetAll(int topicId)
        {
            try
            {
                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                string committeeId = account.Code;


                var result = _commentService.GetAll().Where(_ => _.TopicId == topicId && _.CommitteeId == committeeId)
                    .ToList()
                    .Select(_ => new QuestionViewVM {
                        Id = _.QuestionType,
                        IsApproved = _.IsApproved,
                        Comment = _.Content,
                        Question = CommitteeQuestion.QUESTIONS[_.QuestionType].Question
                    });

                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        

    }
}
