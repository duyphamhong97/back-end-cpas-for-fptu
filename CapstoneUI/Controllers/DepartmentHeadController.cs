﻿using BusinessLogic.Define;
using CapstoneUI.Extensions;
using CapstoneUI.Firebase;
using CapstoneUI.Utils;
using CapstoneUI.ViewModels;
using DataAccess.Entities;
using DataAccess.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CapstoneUI.Controllers
{

    public class DepartmentHeadController : BaseController
    {
        private readonly ITopicService _topicService;
        private readonly ISemesterService _semesterService;
        private readonly IAdvisorTopicService _advisorTopicService;
        private readonly IAdvisorService _advisorService;
        private readonly IAccountService _accountService;
        private readonly IDuplicatedTopicService _duplicatedTopicService;
        private readonly ICommentService _commentService;
        private readonly IStudentGroupService _groupService;
        private readonly IStudentService _studentService;
        private readonly IWeightPointService _weightPointService;


        public DepartmentHeadController(ITopicService topicService, IAdvisorTopicService advisorTopicService,
            IAdvisorService advisorService, IDuplicatedTopicService duplicatedTopicService,
            ICommentService commentService, IStudentGroupService groupService, IStudentService studentService,
            IAccountService accountService, IWeightPointService weightPointService, ISemesterService semesterService)
        {
            _topicService = topicService;
            _advisorTopicService = advisorTopicService;
            _advisorService = advisorService;
            _accountService = accountService;
            _duplicatedTopicService = duplicatedTopicService;
            _commentService = commentService;
            _groupService = groupService;
            _studentService = studentService;
            _weightPointService = weightPointService;
            _semesterService = semesterService;
        }

        private void classyfi()
        {
            try
            {
                //{{hardcode}} xác định thân phận rùi ngành đồ các kiểu nha (departmentProgram)

                var studentss = _studentService.GetAll(_ => _.Program)
                    .Where(_ => _.Status == StudentStatus.PointDone
                    && _.Program.DepartmentProgramId == 1).ToList();

                var studentProgram = studentss.GroupBy(_ => _.ProgramId);

                var semester = _semesterService.Get(_ => _.EndDate > DateTimeOffset.Now);

                int groupName = 1;

                foreach (var students in studentProgram)
                {
                    #region Chia nhóm rồi lưu DB

                    var result = ClassifyGroupUtil.Classify(students.ToList());

                    result.ForEach(group =>
                    {
                        var newGroup = new StudentGroup
                        {
                            Name = "Group " + groupName,
                            SemesterId = semester.Id,
                        };
                        _groupService.Create(newGroup);

                        //reset group leader
                        group.ForEach(z =>
                        {
                            z.LeaderCode = null;
                        });


                        var leader = group.FirstOrDefault().Code;
                        group.ForEach(z =>
                        {
                            if (z.Code != leader)
                            {
                                z.LeaderCode = leader;
                            }

                            z.StudentGroupId = newGroup.Id;
                            _studentService.Update(z);
                        });

                        groupName++;


                    });

                    #endregion
                }


            }
            catch (Exception)
            {

            }
        }



        [HttpPost, Route("api/dh/topic/review"), Authorize]
        public async Task<IHttpActionResult> ChangeDHStatus(TopicDHVM viewModel)
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                var user = _advisorService.Get(_ => _.Code == account.Code);

                #endregion

                var topic = _topicService.Get(_ => _.Id == viewModel.Id, _ => _.AdvisorTopics);
                if (topic == null) return NotFound();

                if (viewModel.IsApproved)
                {
                    topic.Status = Status.DuplicateApproved;
                }
                else
                {
                    topic.Status = Status.DuplicateRejected;
                    topic.Note = viewModel.Content;

                    int type = 1;

                    foreach (var item in viewModel.Comments.Questions)
                    {
                        _commentService.Create(new Comment
                        {
                            CommitteeId = user.Code,
                            TopicId = topic.Id,
                            QuestionType = type++,
                            Deadline = new DateTimeOffset(),
                            Content = item.Comment,
                            IsApproved = item.IsApproved
                        });
                    }
                    

                    await FirebaseDatabase.GetFirebaseDatabase().sendMessage(new FireBaseVM
                    {
                        IsReading = false,
                        TopicId = topic.Id,
                        TopicName = topic.Name_En,
                        UserId = topic.AdvisorTopics.FirstOrDefault(a => a.Role == "Supervisor").AdvisorId,
                        UserName = account.FullName,
                        Status = NotiStatus.DH_REJECT_AD_TOPIC,
                        CreatedDate = DateTimeOffset.Now.ToUnixTimeMilliseconds()
                    });
                }

                _topicService.Update(topic);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpGet, Route("api/dh/topic/review")]
        public IHttpActionResult GetDHTopics()
        {
            try
            {
                //var id = User.Identity;


                var result = _topicService.GetAll(_ => _.AdvisorTopics.Select(__ => __.Advisor.Account))
                    .Where(_ => _.Status == Status.Pending).ToList()
                    .Select(_ => new TopicDuplicateVM
                    {
                        Id = _.Id,
                        Name_En = _.Name_En,
                        AdvisorName = _.AdvisorTopics.FirstOrDefault(x => x.Role == "Supervisor").Advisor.Account.FullName,
                        CreatedDate = _.CreatedDate.ToUnixTimeMilliseconds(),
                    }).OrderByDescending(_ => _.CreatedDate).ToList();

                foreach (var topic in result)
                {
                    var duplicateIds = _duplicatedTopicService.GetAll()
                        .Where(_ => _.TopicId == topic.Id).Select(_ => _.DuplicatedTopicId).ToList();
                    if (duplicateIds == null) break;
                    var duplicateTopic = _topicService.GetAll(_ => _.AdvisorTopics.Select(__ => __.Advisor.Account))
                        .Where(_ => duplicateIds.Contains(_.Id)).ToList().Select(_ => new TopicDuplicateItemVM
                        {
                            Id = _.Id,
                            Name_En = _.Name_En,
                            CreatedDate = _.CreatedDate.ToUnixTimeMilliseconds(),
                            AdvisorName = _.AdvisorTopics.FirstOrDefault(x => x.Role == "Supervisor").Advisor.Account.FullName,
                            
                        }).ToList();
                    topic.DuplicatedTopics = duplicateTopic;
                }
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/dh/topic/{id}/review")]
        public IHttpActionResult GetDHTopicsById(int id)
        {
            try
            {
                var currentTopic = _topicService.Get(_ => _.Id == id, _ => _.AdvisorTopics.Select(__ => __.Advisor.Account));

                var topic = new TopicDuplicateVM
                {
                    Id = currentTopic.Id,
                    Name_En = currentTopic.Name_En,
                    AdvisorName = currentTopic.AdvisorTopics.FirstOrDefault(x => x.Role == "Supervisor").Advisor.Account.FullName,
                    CreatedDate = currentTopic.CreatedDate.ToUnixTimeMilliseconds(),

                };


                var duplicateTopics = _duplicatedTopicService.GetAll()
                    .Where(_ => _.TopicId == topic.Id).ToList();

                var duplicateIds = duplicateTopics.Select(_ => _.DuplicatedTopicId);

                if (duplicateTopics != null)
                {
                    var duplicateTopic = _topicService.GetAll(_ => _.AdvisorTopics.Select(__ => __.Advisor.Account))
                        .Where(_ => duplicateIds.Contains(_.Id)).ToList()
                        .Select(_ => new TopicDuplicateItemVM
                        {
                            Id = _.Id,
                            Name_En = _.Name_En,
                            CreatedDate = _.CreatedDate.ToUnixTimeMilliseconds(),
                            AdvisorName = _.AdvisorTopics.FirstOrDefault(x => x.Role == "Supervisor").Advisor.Account.FullName,

                        }).ToList();
                    foreach (var item in duplicateTopic)
                    {
                        var dupTopic = duplicateTopics.FirstOrDefault(__ => __.TopicId == id
                            && __.DuplicatedTopicId == item.Id);
                        item.DuplicateWords = dupTopic.Keywords.Split(',').ToList();
                        item.Percent = dupTopic.Percentage;

                        // find sentences duplicate
                        
                    }

                    topic.DuplicatedTopics = duplicateTopic;
                }
                return Ok(topic);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost, Route("api/dh/send-to-committee"), Authorize]
        public async Task<IHttpActionResult> SendCommittee(TopicDHCommitteeVM viewModel)
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                var user = _advisorService.Get(_ => _.Code == account.Code);

                #endregion

                if (!ModelState.IsValid) return BadRequest();
                var deadline = DateTimeOffset.FromUnixTimeMilliseconds(viewModel.Deadline);

                foreach (var committee in viewModel.Committees)
                {
                    foreach (var topic in viewModel.Topics)
                    {
                        foreach (var question in CommitteeQuestion.QUESTIONS)
                        {
                            _commentService.Create(new Comment
                            {
                                CommitteeId = committee,
                                TopicId = topic,
                                QuestionType = question.Key,
                                Deadline = deadline
                            });
                        }
                    }
                }

                // Change status
                var topicStatus = _topicService.GetAll()
                    .Where(_ => viewModel.Topics.Contains(_.Id)).ToList();
                foreach (var topic in topicStatus)
                {
                    topic.Status = Status.ValidatePending;
                    _topicService.Update(topic);
                }

                // Cap quyen
                var committees = _accountService.GetAll()
                    .Where(_ => viewModel.Committees.Contains(_.Code)).ToList();
                foreach (var acc in committees)
                {

                    await FirebaseDatabase.GetFirebaseDatabase().sendMessage(new FireBaseVM
                    {
                        IsReading = false,
                        UserId = acc.Code,
                        UserName = account.FullName,
                        Status = NotiStatus.DH_SEND_COMMITEE,
                        CreatedDate = DateTimeOffset.Now.ToUnixTimeMilliseconds()
                    });
                }

                return Ok(committees);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet] // Get pending topics
        [Route("api/dh/topic/status/duplicate-approved")]
        public IHttpActionResult GetTopicByDHToCheck()
        {
            try
            {

                var result = ModelMapper.ConvertToViewModel(
                    _topicService.GetAll(_ => _.TopicTechniques.Select(__ => __.Technique),
                    _ => _.AdvisorTopics.Select(__ => __.Advisor.Account), _ => _.Program)
                    .Where(_ => _.Status == Status.DuplicateApproved).ToList());
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("api/dh/topic/status/validation-approved")]
        public IHttpActionResult GetTopicFromCommittee()
        {
            try
            {

                var result = ModelMapper.ConvertToViewModel(
                    _topicService.GetAll(_ => _.TopicTechniques.Select(__ => __.Technique),
                    _ => _.AdvisorTopics.Select(__ => __.Advisor.Account), _ => _.Program)
                    .Where(_ => _.Status == Status.ValidateApproved || _.Status == Status.ValidateRejected).ToList());
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("api/dh/topic/status/publish-pending")]
        public IHttpActionResult GetTopicStorage()
        {
            try
            {

                var result = ModelMapper.ConvertToViewModel(
                    _topicService.GetAll(_ => _.TopicTechniques.Select(__ => __.Technique),
                    _ => _.AdvisorTopics.Select(__ => __.Advisor.Account), _ => _.Program)
                    .Where(_ => _.Status == Status.PublishPending).ToList());
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("api/dh/topic/send-to-training-staff"), Authorize]
        public async Task<IHttpActionResult> SendTS(TopicDHTSVM viewModel)
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                var user = _advisorService.Get(_ => _.Code == account.Code);

                #endregion

                var topics = _topicService.GetAll().Where(_ => viewModel.Topics.Contains(_.Id)).ToList();

                foreach (var topic in topics)
                {
                    topic.Status = Status.Unpublished;
                    _topicService.Update(topic);
                }

                await FirebaseDatabase.GetFirebaseDatabase()
                    .sendTrainingStaff(account.FullName, topics.Count, NotiStatus.DH_SEND_TOPIC_TS);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("api/dh/topic/send-to-storage"), Authorize]
        public async Task<IHttpActionResult> SendTSa(TopicDHTSVM viewModel)
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                var user = _advisorService.Get(_ => _.Code == account.Code);

                #endregion

                var topics = _topicService.GetAll().Where(_ => viewModel.Topics.Contains(_.Id)).ToList();

                foreach (var topic in topics)
                {
                    topic.Status = Status.PublishPending;
                    _topicService.Update(topic);
                }

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpGet] // Get topic comment
        [Route("api/dh/topic/{id}/comment")]
        public IHttpActionResult GetTopicComment(int id)
        {
            try
            {
                var committees = _advisorService.GetAll(_ => _.Comments, _ => _.Account)
                    .Where(_ => _.Comments.Any(x => x.TopicId == id)).ToList()
                    .Select(_ => new
                    {
                        CommitteeId = _.Code,
                        CommitteeName = _.Account.FullName,
                        Questions = ModelMapper.ConvertToViewModel(_.Comments.Where(x => x.TopicId == id).ToList())
                    });



                return Ok(committees);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut, Route("api/dh/topic/{id}/rewrite"), Authorize]
        public async Task<IHttpActionResult> RewriteTopic(int id)
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();

                #endregion

                var topic = _topicService.Get(_ => _.Id == id, _ => _.AdvisorTopics);
                if (topic == null) return NotFound();

                topic.Status = Status.Rewrite;
                _topicService.Update(topic);

                await FirebaseDatabase.GetFirebaseDatabase().sendMessage(new FireBaseVM
                {
                    IsReading = false,
                    TopicId = topic.Id,
                    TopicName = topic.Name_En,
                    UserId = topic.AdvisorTopics.FirstOrDefault(a => a.Role == "Supervisor").AdvisorId,
                    UserName = account.FullName,
                    Status = NotiStatus.DH_REWRITE_AD,
                    CreatedDate = DateTimeOffset.Now.ToUnixTimeMilliseconds()
                });

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut, Route("api/dh/topic/{id}/to-committee")]
        public IHttpActionResult ToCommitteeTopic(int id)
        {
            try
            {
                var topic = _topicService.Get(_ => _.Id == id);
                if (topic == null) return NotFound();

                topic.Status = Status.DuplicateApproved;
                _topicService.Update(topic);

                // delete all previous comment of committee
                _commentService.GetAll().Where(_ => _.TopicId == id).ToList().ForEach(_ =>
                {
                    _commentService.Delete(_);
                });

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut]
        [Route("api/dh/topic/{id}/disable")]
        public IHttpActionResult DisableTopic(int id)
        {
            try
            {
                var topic = _topicService.Get(_ => _.Id == id);

                if (topic == null) return NotFound();
                topic.Status = Status.Disable;
                _topicService.Update(topic);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/dh/comment/schedule")]
        public IHttpActionResult GetCommentSchedule()
        {
            try
            {
                // {{hardcode}}
                var result = _topicService.GetAll(_ => _.Comments)
                    .Where(_ => (_.Status == Status.ValidatePending)).ToList()
                    .Select(_ => new
                    {
                        _.Id,
                        _.Name_En,
                        CreatedDate = _.CreatedDate.ToUnixTimeMilliseconds(),
                        ExpiredTime = _.Comments.FirstOrDefault() == null ? 0 : _.Comments.FirstOrDefault().Deadline.ToUnixTimeMilliseconds()
                    });
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut, Route("api/dh/comment/schedule")]
        public IHttpActionResult SetCommentSchedule(SetDeadlineVM viewModel)
        {
            try
            {
                var topic = _topicService.Get(_ => _.Id == viewModel.TopicId);
                if (topic == null) return NotFound();
                if (DateTimeOffset.Now.ToUnixTimeMilliseconds() > viewModel.Deadline)
                    return BadRequest("Expired Time");

                _commentService.GetAll()
                    .Where(_ => _.TopicId == viewModel.TopicId).ToList()
                    .ForEach(_ =>
                    {
                        _.Deadline = DateTimeOffset.FromUnixTimeMilliseconds(viewModel.Deadline);
                        _commentService.Update(_);
                    });


                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/dh/student/weight")]
        public IHttpActionResult GetStudentDHD()
        {

            var resul = _weightPointService.GetAll(_ => _.Student.Account, _ => _.Advisor.Account)
                .Where(_ => _.Student.Status == StudentStatus.PointDeadline
                || _.Student.Status == StudentStatus.PointPending).ToList()
                .GroupBy(_ => _.StudentCode, (code, reviews) => new WeightCalculate
                {
                    Code = code,
                    WeightPoint = reviews.First().Student.WeightPoint,
                    Reviews = reviews
                }).ToList();

            if (resul.Count == 0)
                return Ok(new
                {
                    Students = new List<int>(),
                    Deadline = 0
                });

            List<StudentPointVM> viewModel = new List<StudentPointVM>();
            foreach (var student in resul)
            {
                var vm = new StudentPointVM
                {
                    Code = student.Code,
                    FullName = student.Reviews.FirstOrDefault().Student.Account.FullName
                };
                var count = student.Reviews.Where(z => z.Point != 0).Count();

                if (student.WeightPoint > 0)
                {
                    vm.AveragePoint = student.WeightPoint;
                } else
                {
                    vm.AveragePoint = student.Reviews.Aggregate(
                        0.0,
                        (re, item) => re + item.Point,
                        re => student.Reviews.Count(s => s.Point != 0) > 0 ? re / count : 0.0
                    );
                }
                
                vm.Reviews = student.Reviews.Select(_ => new StudentPointReviewVM
                {
                    AdvisorName = _.Advisor.Account.FullName,
                    WeightPoint = _.Point,
                    Note = _.Note
                }).ToList();

                viewModel.Add(vm);
            }

            var deadline = _weightPointService.Get(_ => _.Student.Status == StudentStatus.PointDeadline
                || _.Student.Status == StudentStatus.PointPending, _ => _.Student).Deadline.ToUnixTimeMilliseconds();

            return Ok(new { 
                Students = viewModel,
                Deadline = deadline
            });
        }

        [HttpPost, Route("api/dh/student/weight/all")]
        public IHttpActionResult SetWeightPoint(List<StudentDHVM> viewModel)
        {
            try
            {

                foreach (var item in viewModel)
                {
                    var student = _studentService.Get(_ => _.Code == item.Code);
                    if (student == null) return NotFound();

                    student.WeightPoint = item.WeightPoint;
                    student.Status = StudentStatus.PointDone;
                    _studentService.Update(student);

                }
                
                //classify
                classyfi();

                
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut, Route("api/dh/student/weight")]
        public IHttpActionResult SaveWeightPoint(List<StudentDHVM> viewModel)
        {
            try
            {
                foreach (var item in viewModel)
                {
                    var student = _studentService.Get(_ => _.Code == item.Code);
                    if (student == null) return NotFound();
                    student.WeightPoint = item.WeightPoint;
                    _studentService.Update(student);
                }
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost, Route("api/dh/send-to-advisor"), Authorize]
        public async Task<IHttpActionResult> SendToAdvisor(SendToAdvisorVM viewModel)
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                var user = _advisorService.Get(_ => _.Code == account.Code);

                #endregion

                // remove data

                _weightPointService.GetAll().Where(_ => viewModel.Students.Contains(_.StudentCode)).ToList()
                .ForEach(_ =>
                {
                    _weightPointService.Delete(_);
                });

                var deadline = DateTimeOffset.FromUnixTimeMilliseconds(viewModel.Deadline);
                foreach (var advisor in viewModel.Advisors)
                {

                    foreach (var student in viewModel.Students)
                    {
                        // {{softcode}}
                        var s = _studentService.Get(_ => _.Code == student);
                        if (s == null) return NotFound();

                        

                        _weightPointService.Create(new WeightPoint
                        {
                            AdvisorId = advisor,
                            StudentCode = student,
                            Deadline = deadline,
                            Point = 4
                        });

                        // change student status
                        s.Status = StudentStatus.PointPending;
                        _studentService.Update(s);


                    }

                    // Notify cho advisor
                    await FirebaseDatabase.GetFirebaseDatabase().sendMessage(new FireBaseVM
                    {
                        IsReading = false,
                        UserId = advisor,
                        UserName = account.FullName,
                        Status = NotiStatus.DH_SEND_ADVISOR,
                        CreatedDate = DateTimeOffset.Now.ToUnixTimeMilliseconds()
                    });

                }
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpGet, Route("api/dh/student/grouping/{programId}")]
        public IHttpActionResult LoadGroupStudent(int programId)
        {
            try
            {

                var result = _groupService.GetAll(_ => _.Students.Select(s => s.Account))
                    .Where(_ => _.Students.Any(s => s.ProgramId == programId)).ToList()
                    .Select(_ => new GroupVM
                    {
                        Id = _.Id,
                        Name = _.Name,
                        Students = _.Students.Select(s => new StudentGroupVM
                        {
                            Code = s.Code,
                            FullName = s.Account.FullName,
                            IsLeader = s.LeaderCode == null ? true : false,
                            WeightPoint = s.WeightPoint,
                            IsChangeable = s.Status < StudentStatus.GroupDone ? true : false
                        }).OrderBy(w => w.WeightPoint).ToList()
                    });
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost, Route("api/dh/student/grouping")]
        public IHttpActionResult UpdateGroupStudent(List<GroupVM> viewModel)
        {
            try
            {
                var semester = _semesterService.Get(_ => _.EndDate > DateTimeOffset.Now);

                foreach (var group in viewModel)
                {
                    int groupId = 0;
                    if (group.Id == 0)
                    {
                        // create new group
                        var gtmp = new StudentGroup
                        {
                            Name = group.Name,
                            SemesterId = semester.Id,
                        };

                        _groupService.Create(gtmp);

                        groupId = gtmp.Id;
                    }
                    else
                    {
                        var db = _groupService.Get(_ => _.Id == group.Id);
                        if (db == null) return NotFound();

                        groupId = db.Id;

                        // Change group name
                        db.Name = group.Name;
                        _groupService.Update(db);

                    }

                    var leader = group.Students.FirstOrDefault();

                    if (group.Students.Any(_ => _.IsChangeable == false))
                    {
                        leader = group.Students.FirstOrDefault(_ => _.IsLeader && _.IsChangeable == false);
                    }

                    group.Students.ToList().ForEach(s =>
                    {
                        var stmp = _studentService.Get(_ => _.Code == s.Code);

                        if (s.Code == leader.Code)
                            stmp.LeaderCode = null;
                        else
                            stmp.LeaderCode = leader.Code;

                        stmp.StudentGroupId = groupId;
                        _studentService.Update(stmp);
                    });

                    
                }

                _groupService.GetAll(_ => _.Students).Where(_ => _.Students.Count == 0)
                    .ToList().ForEach(_ =>
                    {
                        _groupService.Delete(_);
                    });

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost, Route("api/dh/student/grouping/send-ts"), Authorize]
        public async Task<IHttpActionResult> SendTSGroupAsync(List<int> groupIds)
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();

                #endregion

                var semester = _semesterService.Get(_ => _.EndDate > DateTimeOffset.Now);
                int hasGroup = 0;

                foreach (var group in groupIds)
                {
                    var db = _groupService.Get(_ => _.Id == group, _ => _.Students);
                    if (db == null) return NotFound();
                    if (db.Students.Any(_ => _.Status == StudentStatus.PointDone))
                    {
                        hasGroup++;

                        db.Students.ToList().ForEach(async s =>
                        {
                            var stmp = _studentService.Get(_ => _.Code == s.Code);

                            if (stmp.Status == StudentStatus.PointDone)
                            {
                                stmp.Status = StudentStatus.GroupDone;

                                if (db.TopicId != null)
                                {
                                    stmp.Status = StudentStatus.TopicTaken;
                                }
                                _studentService.Update(stmp);

                                await FirebaseDatabase.GetFirebaseDatabase()
                                .SendStudent(stmp.Code, NotiStatus.DH_GROUP_STUDENT);
                            }
                            
                        });
                    }
                }

                if (hasGroup > 0)
                {
                    await FirebaseDatabase.GetFirebaseDatabase()
                        .sendTrainingStaff(account.FullName, hasGroup, NotiStatus.DH_SEND_STUDENT_TS);
                }

                return Ok();
            }
            catch (Exception e)
            {
                return Ok("Tam linh" + e);
            }
        }

        [HttpGet, Route("api/dh/student/new")]
        public IHttpActionResult GetStudentDH()
        {
            try
            {

                var result = ModelMapper.ConvertToViewModel(_studentService.GetAll(_ => _.Program, _ => _.Account)
                    .Where(_ => _.Status == StudentStatus.Grouping).ToList());
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpDelete, Route("api/dh/student/{code}")]
        public IHttpActionResult DeleteStudent(string code)
        {
            try
            {
                var student = _studentService.Get(_ => _.Code == code);
                if (student == null) return NotFound();
                student.Status = StudentStatus.Disable;
                student.LeaderCode = null;
                student.StudentGroupId = null;
                _studentService.Update(student);

                //disable account login
                var account = _accountService.Get(_ => _.Email == student.Account.Email);
                account.IsActive = false;
                _accountService.Update(account);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpDelete, Route("api/dh/group/{id}")]
        public IHttpActionResult DeleteGroupStudent(int id)
        {
            try
            {
                var group = _groupService.Get(_ => _.Id == id);
                if (group == null) return NotFound();

                var student = _studentService.GetAll().Where(_ => _.StudentGroupId == id).ToList();
                student.ForEach(_ =>
                {
                    _.Status = StudentStatus.Disable;
                    _.StudentGroupId = null;
                    _.LeaderCode = null;

                    _studentService.Update(_);

                    var account = _accountService.Get(__ => __.Email == _.Account.Email);
                    if (account != null)
                    {
                        account.IsActive = false;
                        _accountService.Update(account);
                    }
                });

                _groupService.Delete(group);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpPost, Route("api/dh/advisor")]
        public IHttpActionResult CreateAccount(AccountVM account)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                var acc = _accountService.Get(_ => _.Email == account.Email.ToLower());
                if (acc == null)
                {
                    var code = account.Email.SubBefore("@");

                    _accountService.Create(new Account
                    {
                        Code = code,
                        Email = account.Email,
                        AccountRoleId = 3,
                        FullName = account.FullName,
                        IsActive = true,
                    });

                    _advisorService.Create(new Advisor
                    {
                        Code = code,
                        Title = account.Title,
                        Phone = account.Phone,
                        DepartmentProgramId = 1
                    });

                }
                else
                {
                    return BadRequest("Account email is existed!");
                }

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/dh/advisor")]
        public IHttpActionResult GetAllAccount()
        {
            try
            {
                var result = _accountService.GetAll(_ => _.Advisor)
                    .Where(_ => _.AccountRoleId == 3)
                    .Select(_ => new {
                        _.Code,
                        _.FullName,
                        _.Email,
                        _.Advisor.Title,
                        _.Advisor.Phone,
                        _.IsActive,
                    });

                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut, Route("api/dh/advisor")]
        public IHttpActionResult GetAllAccount(AccountUpdateVM account)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                var acc = _accountService.Get(_ => _.Code == account.Code, _ => _.Advisor);
                if (acc == null)
                {
                    return NotFound();
                }
                else
                {
                    acc.FullName = account.FullName;
                    acc.IsActive = account.IsActive;
                    _accountService.Update(acc);

                    acc.Advisor.Phone = account.Phone;
                    acc.Advisor.Title = account.Title;

                    _advisorService.Update(acc.Advisor);
                }

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        public class WeightCalculate
        {
            public string Code { get; set; }
            public double WeightPoint { get; set; }
            public IEnumerable<WeightPoint> Reviews { get; set; }
        }

    }


}
