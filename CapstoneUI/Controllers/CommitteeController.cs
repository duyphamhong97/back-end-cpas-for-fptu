﻿using BusinessLogic.Define;
using CapstoneUI.Extensions;
using CapstoneUI.Firebase;
using CapstoneUI.ViewModels;
using DataAccess.Entities;
using DataAccess.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CapstoneUI.Controllers
{
    public class CommitteeController : BaseController
    {
        private readonly ICommentService _commentService;
        private readonly ITopicService _topicService;
        private readonly IAdvisorService _advisorService;
        private readonly IAccountService _accountService;

        public CommitteeController(ICommentService commentService, ITopicService topicService,
            IAdvisorService advisorService, IAccountService accountService)
        {
            _commentService = commentService;
            _topicService = topicService;
            _advisorService = advisorService;
            _accountService = accountService;
        }

        [HttpGet, Route("api/committee/topic"), Authorize]
        public IHttpActionResult GetCommitteeTopics()
        {
            try
            {
                // account
                #region IdentityUser
                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();

                #endregion

                string committeeId = account.Code;

                var result = ModelMapper.ConvertToCommitteeVM(
                    _topicService.GetAll(_ => _.AdvisorTopics.Select(__ => __.Advisor.Account),
                    _ => _.Comments)
                    .Where(_ => _.Comments.Any(x => x.CommitteeId == committeeId)
                    && _.Status == Status.ValidatePending)
                    .OrderByDescending(_ => _.CreatedDate)
                    .ToList(), committeeId);

                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpGet, Route("api/committee/question")]
        public IHttpActionResult GetQuestion()
        {
            try
            {
                return Ok(CommitteeQuestion.QUESTIONS.Select(_ => new
                {
                    id = _.Key,
                    content = _.Value.Question
                }));

            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut, Route("api/committee/question"), Authorize] // submit check validation
        public IHttpActionResult SetQuestion(CommentVM viewModel)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest();


                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);
                
                var committeeId = account.Code;

                var comments = _commentService.GetAll().Where(_ => _.TopicId == viewModel.TopicId
                               && _.CommitteeId == committeeId).ToList();

                comments.ForEach(_ =>
                {
                    var question = viewModel.Questions.FirstOrDefault(x => x.Id == _.QuestionType);
                    if (question != null)
                    {
                        _.IsApproved = question.IsApproved;
                        _.Content = question.Comment;
                    }

                    _commentService.Update(_);
                });


                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut, Route("api/committee/deadline")]
        public async Task<IHttpActionResult> TopicQuestionDeadline()
        {
            try
            {
                var result = _topicService.GetAll(_ => _.Comments)
                    .Where(_ => _.Comments.Any(__ => __.Deadline <= DateTimeOffset.Now)
                    && _.Status == Status.ValidatePending).ToList();


                if (result.Count > 0)
                {
                    var programId = result.FirstOrDefault().ProgramId;
                    
                    #region Determine aprrove percentage

                    foreach (var topic in result)
                    {
                        var numbers = topic.Comments;
                        double average = numbers.Aggregate(
                             0.0,
                             (reValue, item) => {
                                 double weight = CommitteeQuestion.QUESTIONS
                                 .FirstOrDefault(_ => _.Key == item.QuestionType).Value.Weight;
                                 
                                 return reValue + (item.IsApproved ? weight : 0);
                             },
                             reValue => reValue / (numbers.Count / 5));

                        if (average >= 0.6)
                            topic.Status = Status.ValidateApproved;
                        else
                            topic.Status = Status.ValidateRejected;

                        _topicService.Update(topic);
                    }
                    #endregion

                    #region Send Notify
                    var dhead = _accountService.Get(_ => _.AccountRoleId == 5);

                    await FirebaseDatabase.GetFirebaseDatabase().sendMessage(new FireBaseVM
                    {
                        IsReading = false,
                        UserId = dhead.Code,
                        Status = NotiStatus.CM_DEADLINE_SEND_DH,
                        CreatedDate = DateTimeOffset.Now.ToUnixTimeMilliseconds()
                    });
                    #endregion
                }

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
        
    }
}
