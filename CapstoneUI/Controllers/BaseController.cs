﻿using CapstoneUI.ViewModels;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Http;

namespace CapstoneUI.Controllers
{
    public class BaseController : ApiController
    {
        private _ModelMapping _modelMapper;

        public _ModelMapping ModelMapper => _modelMapper ?? (_modelMapper = new _ModelMapping());


    }
}
