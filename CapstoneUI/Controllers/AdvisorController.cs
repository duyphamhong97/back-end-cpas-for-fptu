﻿using BusinessLogic.Define;
using CapstoneUI.Extensions;
using CapstoneUI.Firebase;
using CapstoneUI.ViewModels;
using DataAccess.Entities;
using DataAccess.Resources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using static CapstoneUI.Controllers.DepartmentHeadController;

namespace CapstoneUI.Controllers
{
    public class AdvisorController : BaseController
    {
        private readonly IAdvisorService _advisorService;
        private readonly ITopicService _topicService;
        private readonly IGradeService _gradeService;
        private readonly IStudentService _studentService;
        private readonly IAccountService _accountService;
        private readonly IWeightPointService _weightPointService;

        public AdvisorController(IAdvisorService advisorService, IStudentService studentService,
            IAccountService accountService, IWeightPointService weightPointService, ITopicService topicService,
            IGradeService gradeService)
        {
            _advisorService = advisorService;
            _studentService = studentService;
            _accountService = accountService;
            _weightPointService = weightPointService;
            _topicService = topicService;
            _gradeService = gradeService;
        }

        [HttpPost, Route("api/advisor/script/weight-point")]
        public IHttpActionResult Script(List<AdvisorWPVM> viewModel)
        {
            try
            {
                foreach (var item in viewModel)
                {
                    var a = _weightPointService.Get(_ => _.StudentCode == item.StudentCode && _.AdvisorId == item.AdvisorId);
                    if (a == null) continue;
                    a.Point = item.Point;
                    _weightPointService.Update(a);
                }
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                var result = _advisorService.GetAll(_ => _.Account).
                    Select(_ => new {
                        Id = _.Code,
                        _.Account.FullName,
                        _.Account.AccountRoleId
                    });
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/advisor/student-point"), Authorize]
        public IHttpActionResult GetStudentList()
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                var user = _advisorService.Get(_ => _.Code == account.Code);

                #endregion

                var resul = _weightPointService.GetAll(_ => _.Student.Account).Where(_ => _.AdvisorId == user.Code
                && _.Student.Status == StudentStatus.PointPending)
                    .Select(_ => new
                    {
                        Code = _.StudentCode,
                        _.Student.Account.FullName,
                        WeightPoint = _.Point,
                        _.Note,
                        _.Deadline
                    }).ToList();
                if (resul.Count == 0) return Ok();
                var re = new
                {
                    Deadline = resul.FirstOrDefault().Deadline.ToUnixTimeMilliseconds(),
                    StudentPoints = resul
                };

                return Ok(re);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost, Route("api/advisor/student-point"), Authorize]
        public IHttpActionResult AdvisorPointStudent(List<StudentAdvisorCreateVM> viewModel)
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                var user = _advisorService.Get(_ => _.Code == account.Code);

                #endregion

                var result = _weightPointService.GetAll().Where(_ => _.AdvisorId == user.Code).ToList();

                result.ForEach(_ =>
                {
                    var vm = viewModel.FirstOrDefault(z => z.Code == _.StudentCode);
                    if (vm != null)
                    {
                        _.Note = vm.Note;
                        _.Point = vm.WeightPoint;

                        _weightPointService.Update(_);
                    }
                });

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/advisor/deadline")]
        public async Task<IHttpActionResult> DeadlineAsync()
        {
            try
            {
                _weightPointService.GetAll(_ => _.Student).Where(_ => _.Deadline <= DateTimeOffset.Now 
                && _.Student.Status == StudentStatus.PointPending)
                .GroupBy(_ => _.StudentCode, (code, _) => code).ToList().ForEach(_ =>
                {
                    var student = _studentService.Get(s => s.Code == _);
                    if (student != null)
                    {
                        student.Status = StudentStatus.PointDeadline;
                        _studentService.Update(student);
                    }
                });

                // set point

                var resul = _weightPointService.GetAll(_ => _.Student.Account, _ => _.Advisor)
                .Where(_ => _.Deadline <= DateTimeOffset.Now && _.Student.Status == StudentStatus.PointDeadline).ToList()
                .GroupBy(_ => _.StudentCode, (code, reviews) => new WeightCalculate
                {
                    Code = code,
                    Reviews = reviews
                }).ToList();

                if (resul.Count > 0)
                {
                    int programId = 1;

                    #region Save point
                    foreach (var student in resul)
                    {
                        var vm = new StudentPointVM
                        {
                            Code = student.Code,
                            FullName = student.Reviews.FirstOrDefault().Student.Account.FullName
                        };
                        var count = student.Reviews.Where(z => z.Point != 0).Count();

                        vm.AveragePoint = student.Reviews.Aggregate(
                                0.0,
                                (re, item) => re + item.Point,
                                re => student.Reviews.Count(s => s.Point != 0) > 0 ? re / count : 0.0
                            );

                        var ss = _studentService.Get(_ => _.Code == student.Code);
                        ss.WeightPoint = vm.AveragePoint;
                        _studentService.Update(ss);

                        programId = ss.ProgramId;
                    }
                    #endregion

                    #region Send Notify
                    

                    var dhead = _accountService.Get(_ => _.AccountRoleId == 5);

                    await FirebaseDatabase.GetFirebaseDatabase().sendMessage(new FireBaseVM
                    {
                        IsReading = false,
                        UserId = dhead.Code,
                        Status = NotiStatus.AD_DEADLINE_SEND_DH,
                        CreatedDate = DateTimeOffset.Now.ToUnixTimeMilliseconds()
                    });
                    #endregion
                }



                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/advisor/topic/{id}/student")]
        public IHttpActionResult Klskef(int id)
        {
            var result = _topicService.Get(_ => _.Id == id, _ => _.StudentGroups.Select(s => s.Students.Select(
                z => z.Grades.Select(__ => __.GradeType))), _ => _.StudentGroups.Select(s => s.Students.Select(
                z => z.Account)));

            if (result.StudentGroups.Count == 0)
                return Ok();

            var sul = new
            {
                Id = id,
                result.Name_En,
                Students = result.StudentGroups.FirstOrDefault().Students.Select(ss => new
                {
                    ss.Account.FullName,
                    Grades = ss.Grades.Select(_ => new
                    {
                        GradeId = _.Id,
                        GradeName = _.GradeType.Name,
                        _.GradeType.Percent,
                        _.Value
                    })
                })
            };

            return Ok(sul);
        }


        [HttpPost, Route("api/advisor/topic/student")]
        public IHttpActionResult SaveStudentGrade(List<StudentGradeVM> viewModel)
        {
            try
            {
                foreach (var item in viewModel)
                {
                    var grade = _gradeService.Get(_ => _.Id == item.GradeId);
                    grade.Value = item.Value;
                    _gradeService.Update(grade);
                }
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/advisor/student/{code}/grade")]
        public IHttpActionResult GetStudentGrades(string code)
        {
            var grades = _gradeService.GetAll(_ => _.GradeType).Where(_ => _.StudentCode == code)
                .OrderBy(_ => _.GradeTypeId)
                .Select(_ => new GradeItemVM
                {
                    GradeId = _.Id,
                    Name = _.GradeType.Name,
                    Percent = _.GradeType.Percent,
                    Value = _.Value,
                    Note = _.Note
                });

            return Ok(new GradeStudentVM
            {
                FinalProject = grades.FirstOrDefault(),
                Reports = grades.Skip(1).ToList(),
                FinalProjectResit = grades.FirstOrDefault(_ => _.Name == "Final Project Resit"),
            });
        }

        [HttpPost, Route("api/advisor/student/grade")]
        public IHttpActionResult UpdateStudentGrade(GradeStudentVM viewModel)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest();

                var gradeItem = _gradeService.Get(_ => _.Id == viewModel.FinalProject.GradeId);
                if (gradeItem == null) return NotFound();
                gradeItem.Note = viewModel.FinalProject.Note;
                gradeItem.Value = viewModel.FinalProject.Value;
                _gradeService.Update(gradeItem);

                foreach (var grade in viewModel.Reports)
                {
                    var item = _gradeService.Get(_ => _.Id == grade.GradeId);
                    if (item == null) return NotFound();
                    item.Note = grade.Note;
                    item.Value = grade.Value;
                    _gradeService.Update(item);
                }
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

    }
}
