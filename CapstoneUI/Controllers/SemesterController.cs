﻿using BusinessLogic.Define;
using CapstoneUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CapstoneUI.Controllers
{
    public class SemesterController : BaseController
    {
        private readonly ISemesterService _semesterService;

        public SemesterController(ISemesterService semesterService)
        {
            _semesterService = semesterService;
        }


        [HttpPost]
        public IHttpActionResult Create(SemesterVM viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                _semesterService.Create(ModelMapper.ConvertToModel(viewModel));
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            try
            {
                var result = ModelMapper.ConvertToViewModel(_semesterService.GetAll().ToList());
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpPut]
        public IHttpActionResult Create(SemesterViewVM viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var semester = _semesterService.Get(_ => _.Id == viewModel.Id);
                semester.StartDate = DateTimeOffset.FromUnixTimeMilliseconds(viewModel.StartDate);
                semester.EndDate = DateTimeOffset.FromUnixTimeMilliseconds(viewModel.EndDate);
                semester.Name = viewModel.Name;

                _semesterService.Update(semester);
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
