﻿using BusinessLogic.Define;
using CapstoneUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CapstoneUI.Controllers
{
    public class TechniqueController : BaseController
    {
        private readonly ITechniqueService _techniqueService;

        public TechniqueController(ITechniqueService techniqueService)
        {
            _techniqueService = techniqueService;
        }

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            try
            {
                var result = ModelMapper.ConvertToViewModel(_techniqueService.GetAll().ToList());
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost]
        public IHttpActionResult Create(TechniqueCreateVM viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = ModelMapper.ConvertToModel(viewModel);

                var check = _techniqueService.Get(_ => _.Name == result.Name);
                if (check == null)
                {
                    _techniqueService.Create(result);
                }
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut]
        public IHttpActionResult Update(TechniqueUpdateVM viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var technique = _techniqueService.Get(_ => _.Id == viewModel.Id);
                if (technique == null)
                    return NotFound();
                technique.Name = viewModel.Name;
                _techniqueService.Update(technique);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


    }
}
