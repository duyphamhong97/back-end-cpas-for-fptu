﻿using BusinessLogic.Define;
using CapstoneUI.Extensions;
using DataAccess.Entities;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web.Http;
namespace CapstoneUI.Controllers
{
    [RoutePrefix("api/auth")]
    public class AuthController : ApiController
    {
        private readonly IAccountService _accountService;
        private readonly IAdvisorService _advisorService;

        public AuthController(IAccountService accountService, IAdvisorService advisorService)
        {
            _accountService = accountService;
            _advisorService = advisorService;
        }


        [HttpGet, Route("api/account")]
        public IHttpActionResult GetAll()
        {
            try
            {
                return Ok(_accountService.GetAll().ToList());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost, Route("login")]
        public IHttpActionResult Login([FromBody] GoogleAccountModel ggAccount)
        {
            string token = "";
            try
            {
                var account = _accountService.Get(_ => _.Uid == ggAccount.Uid);

                if (account == null)
                {
                    var userAuthorized = _accountService.Get(_ => _.Email == ggAccount.Email);
                    if (userAuthorized == null)
                    {

                        return Ok(createTokenUnauthorize(ggAccount));
                    }

                    if (!userAuthorized.IsActive) return Unauthorized();

                    userAuthorized.Uid = ggAccount.Uid;
                    _accountService.Update(userAuthorized);
                }

                token = createToken(ggAccount);
                if (token == null)
                    return InternalServerError();

                //return the token
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
            return Ok<string>(token);
        }

        private string createTokenUnauthorize(GoogleAccountModel ggAccount)
        {
            //Set issued at date
            DateTime issuedAt = DateTime.UtcNow;
            //set the time when it expires
            DateTime expires = DateTime.UtcNow.AddDays(7);

            //http://stackoverflow.com/questions/18223868/how-to-encrypt-jwt-security-token
            var tokenHandler = new JwtSecurityTokenHandler();


            //create a identity and add claims to the user which we want to log in
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                //add more field to get back client
                new Claim("uid", ggAccount.Uid),
                new Claim(ClaimTypes.Role, "6"),
                new Claim(ClaimTypes.Email, ggAccount.Email)
            });

            const string sec = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
            var now = DateTime.UtcNow;
            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));
            var signingCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);


            //create the jwt
            var token =
                (JwtSecurityToken)
                    tokenHandler.CreateJwtSecurityToken(issuer: "http://localhost:54856", audience: "http://localhost:54856",
                        subject: claimsIdentity, notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }


        private string createToken(GoogleAccountModel ggAccount)
        {
            //Set issued at date
            DateTime issuedAt = DateTime.UtcNow;
            //set the time when it expires
            DateTime expires = DateTime.UtcNow.AddDays(7);

            //http://stackoverflow.com/questions/18223868/how-to-encrypt-jwt-security-token
            var tokenHandler = new JwtSecurityTokenHandler();

            // Authorize
            var account = _accountService.Get(_ => _.Uid == ggAccount.Uid);
            if (account == null) return null;

            string roleName = "";
            string userId = "";

            switch (account.AccountRoleId)
            {
                case 5:
                    roleName = "5";
                    userId = account.Code;
                    break;
                case 2:
                    roleName = "2";
                    userId = account.Code;
                    break;
                case 4:
                    roleName = "4";
                    userId = account.Code;
                    break;
                case 3:
                    roleName = "3";
                    userId = account.Code;
                    break;
                case 6:
                    roleName = "0";
                    userId = account.Code;
                    break;
                case 1:
                    roleName = "1";
                    break;
            }



            //create a identity and add claims to the user which we want to log in
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                //add more field to get back client
                new Claim("uid", ggAccount.Uid),
                new Claim(ClaimTypes.Name, ggAccount.FullName),
                new Claim("userId", userId),
                new Claim("isAdvisor", account.AccountRoleId == 0? "false" : "true"),
                new Claim(ClaimTypes.Role, roleName),
                new Claim(ClaimTypes.Email, ggAccount.Email)
            });

            const string sec = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
            var now = DateTime.UtcNow;
            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));
            var signingCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);


            //create the jwt
            var token =
                (JwtSecurityToken)
                    tokenHandler.CreateJwtSecurityToken(issuer: "http://localhost:54856", audience: "http://localhost:54856",
                        subject: claimsIdentity, notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }
    }
    public class GoogleAccountModel
    {
        public string Uid { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
    }
}
