﻿    using BusinessLogic.Define;
using CapstoneUI.Extensions;
using CapstoneUI.Firebase;
using CapstoneUI.Utils;
using CapstoneUI.ViewModels;
using DataAccess.Entities;
using DataAccess.Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;

namespace CapstoneUI.Controllers
{
    public class TopicController : BaseController
    {
        private readonly ITopicService _topicService;
        private readonly IAdvisorTopicService _advisorTopicService;
        private readonly IAdvisorService _advisorService;
        private readonly IDuplicatedTopicService _duplicatedTopicService;
        private readonly ICommentService _commentService;
        private readonly IAccountService _accountService;

        protected HttpClient client;


        public TopicController(ITopicService topicService, IAdvisorTopicService advisorTopicService,
            IAdvisorService advisorService, IDuplicatedTopicService duplicatedTopicService,
            ICommentService commentService, IAccountService accountService)
        {
            _topicService = topicService;
            _advisorTopicService = advisorTopicService;
            _advisorService = advisorService;
            _duplicatedTopicService = duplicatedTopicService;
            _commentService = commentService;
            _accountService = accountService;

            // get api
            client = new HttpClient();
            client.BaseAddress = new Uri("https://capstone-utils.azurewebsites.net/api/azuretest?code=Rwf0LXFCJF1YdPrfjFcMVmgkg8p0CKAS/m2ggh7yiseiGaGgE/aXHg==");
            //client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        #region Advisor

        [HttpPost, Authorize]
        public async Task<IHttpActionResult> Create(TopicCreateVM viewModel)
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                //var user = _advisorService.Get(_ => _.Code == account.Code);

                #endregion

                if (!ModelState.IsValid)
                    return BadRequest();

                var topic = ModelMapper.ConvertToModel(viewModel);

                if (!(viewModel.Status == Status.Draft || viewModel.Status == Status.Pending))
                {
                    return BadRequest("Status is invalid");
                }

                topic.Status = viewModel.Status;
                _topicService.Create(topic);

                // Submit topic
                if (topic.Status == Status.Pending)
                {
                    Task.Run(async () => await CheckDuplicate(topic, account, true));

                }
                else if (topic.Status == Status.Draft)
                {
                    Task.Run(async () => await CheckDuplicate(topic, account, false));
                }
                return Ok(topic.Id);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpGet, Route("api/topic/{id}/duplicateTopic/{duplicateId}")]
        public IHttpActionResult GetTopicDetail(int id, int duplicateId)
        {
            try
            {
                var target = _topicService.Get(_ => _.Id == id);
                var duplicate = _topicService.Get(_ => _.Id == duplicateId);
                if (target == null || duplicate == null) return NotFound();

                // bat dau to mau ne
                var keyword = _duplicatedTopicService.Get(_ => _.DuplicatedTopicId == duplicateId && _.TopicId == id).Keywords.Split(',');

                #region Abstraction
                //var result = Regex.Split(html, "\\<\\/?(li|p)\\>");

                var sentences = target.AbstractionHtml.Split(' ');
                foreach (var sentence in sentences)
                {
                    var count = 0;
                    foreach (var key in keyword)
                    {
                        if (sentence.ToLower().Contains(key) && key != "")
                        {
                            count++;
                        }
                    }
                    var s = sentence.RemoveStopWord().Split(' ');
                    if (((double)count / (double)s.Count()) > 0.5)
                    {
                        //sentence = sentence.HighlightHTML(sentence);
                    }
                }
                #endregion

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut, Authorize]
        public async Task<IHttpActionResult> Update(TopicUpdateVM viewModel)
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                //var user = _advisorService.Get(_ => _.Code == account.Code);

                #endregion

                if (!ModelState.IsValid)
                    return BadRequest();

                var entity = _topicService.Get(_ => _.Id == viewModel.Id, x => x.TopicTechniques,
                    x => x.AdvisorTopics);

                if (entity == null)
                    return NotFound();

                if (!(viewModel.Status == Status.Draft || viewModel.Status == Status.Pending || viewModel.Status == Status.Rewrite))
                {
                    return BadRequest("Status is invalid");
                }

                if (entity.Status == Status.Rewrite)
                {
                    if (viewModel.Status == Status.Pending)
                    {
                        entity.Status = Status.ValidateApproved;
                    }
                }
                else
                {
                    entity.Status = viewModel.Status;
                }

                if (entity.Status == Status.Draft)
                {
                    entity.Name_En = viewModel.Name_En;
                    entity.Name_Vi = viewModel.Name_Vi;
                    entity.ShortName = viewModel.ShortName;
                    entity.Abstraction = viewModel.Abstraction;
                    entity.AbstractionHtml = viewModel.AbstractionHtml;
                    entity.MainFunction = viewModel.MainFunction;
                    entity.MainFunctionHtml = viewModel.MainFunctionHtml;
                    entity.Theory = viewModel.Theory;
                    entity.TheoryHtml = viewModel.TheoryHtml;
                    entity.OtherComments = viewModel.OtherComments;
                    entity.OtherCommentsHtml = viewModel.OtherCommentsHtml;
                    entity.OtherProducts = viewModel.OtherProducts;
                    entity.OtherProductsHtml = viewModel.OtherProductsHtml;
                    entity.ProgramId = viewModel.ProgramId;

                }




                var newAdvisors = ModelMapper.ConvertToModel(viewModel.SubAdvisors);
                newAdvisors.Add(new AdvisorTopic
                {
                    AdvisorId = viewModel.AdvisorId,
                    Role = "Supervisor"
                });


                entity.Id = viewModel.Id;
                _topicService.Update(entity, viewModel.Techniques.Select(x => x.Id), newAdvisors);

                if (entity.Status == Status.Pending)
                {
                    Task.Run(async () => await CheckDuplicate(entity, account, true));

                }
                else if (entity.Status == Status.Draft)
                {
                    Task.Run(async () => await CheckDuplicate(entity, account, false));
                }
                return Ok();

            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpGet, Route("api/test/duplicate")]
        public async Task<IHttpActionResult> Tests(int topicId)
        {
            try
            {
                var topics = _topicService.GetAll().ToList();
                foreach (var topic in topics)
                {
                    await CheckDuplicate(topic, null, false);

                }

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        private async Task CheckDuplicate(Topic topic, Account user, bool isPending)
        {
            

            if (isPending)
            {
                await DuplicationChecker.Execute(topic, _topicService.GetAll()
                    .Where(_ => _.Id != topic.Id).ToList(),
                        client, _duplicatedTopicService);
                // Send notification to DH
                var dhead = _accountService.Get(_ => _.AccountRoleId == 5);

                await FirebaseDatabase.GetFirebaseDatabase().sendMessage(new FireBaseVM
                {
                    TopicId = topic.Id,
                    TopicName = topic.Name_En,
                    CreatedDate = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                    TopicStatus = (int)topic.Status,
                    IsReading = false,
                    UserId = dhead.Code,
                    UserName = user.FullName,
                    Status = NotiStatus.AD_SUBMIT_TOPIC
                });
            }
            else
            {
                // Check duplicate
                await DuplicationChecker.Execute(topic, _topicService.GetAll()
                    .Where(_ => _.Id != topic.Id).ToList(),
                        client, _duplicatedTopicService);
                if (user == null) { return; }

                var count = _duplicatedTopicService.GetAll().Where(_ => _.TopicId == topic.Id).Count();

                await FirebaseDatabase.GetFirebaseDatabase().sendMessage(new FireBaseVM
                {
                    TopicId = topic.Id,
                    TopicName = topic.Name_En,
                    CreatedDate = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                    TopicStatus = (int)topic.Status,
                    IsReading = false,
                    UserId = user.Code,
                    UserName = user.FullName,
                    Status = NotiStatus.AD_SAVE_DRAFT_TOPIC,
                    CountDuplicate = count
                });
            }
        }

        [HttpGet, Route("api/topic/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {

                var result = ModelMapper.ConvertToViewModel(
                    _topicService.Get(_ => _.Id == id, _ => _.TopicTechniques.Select(__ => __.Technique),
                    _ => _.AdvisorTopics.Select(__ => __.Advisor.Account), _ => _.Program));
                if (result == null)
                {
                    return BadRequest("Invalid topic id");
                }
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/advisor/topic"), Authorize]
        public IHttpActionResult GetTopicByAdvisor(int pageIndex, int pageSize, string searchValue = "", 
            string sort = "createdDate", bool isAsc = false)
        {
            try
            {
                #region Identify User


                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();

                #endregion

                var ids = account.Code;

                #region Query

                var result = _topicService.GetAll(_ => _.AdvisorTopics.Select(x => x.Advisor))
                                        .Where(x => x.AdvisorTopics
                                        .Any(y => y.AdvisorId == ids && y.Role == "Supervisor")
                                        && x.Name_En.Contains(searchValue))
                                        .Select(_ => new
                                        {
                                            _.Id,
                                            _.Name_En,
                                            _.Name_Vi,
                                            _.ShortName,
                                            _.CreatedDate,
                                            _.Status,
                                            _.Note,
                                        }).ToList()
                                        .Select(_ => new
                                        {
                                            _.Id,
                                            _.Name_En,
                                            _.Name_Vi,
                                            _.ShortName,
                                            CreatedDate = _.CreatedDate.ToUnixTimeMilliseconds(),
                                            _.Status,
                                            _.Note,
                                        }).ToList();
                #endregion

                switch (sort)
                {
                    case "createdDate":
                        if (isAsc) result = result.OrderBy(_ => _.CreatedDate).ToList();
                        else result = result.OrderByDescending(_ => _.CreatedDate).ToList();
                        break;
                    case "name_En":
                        if (isAsc) result = result.OrderBy(_ => _.Name_En).ToList();
                        else result = result.OrderByDescending(_ => _.Name_En).ToList();
                        break;
                    case "status":
                        if (isAsc) result = result.OrderBy(_ => _.Status).ToList();
                        else result = result.OrderByDescending(_ => _.Status).ToList();
                        break;
                }

                Pager pager = new Pager(result.Count, pageIndex, pageSize);
                var paginationResult = result.Skip(pager.StartIndex).Take(pager.PageSize);

                return Ok(new { result = paginationResult, totalItems = result.Count });
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        

        #endregion


        [HttpGet, Authorize]
        public IHttpActionResult GetAll()
        {
            try
            {
                if (!(User.IsInRole("4") || User.IsInRole("5") || User.IsInRole("2") || User.IsInRole("3")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();


                var result = ModelMapper.ConvertToViewModel(
                    _topicService.GetAll(_ => _.TopicTechniques.Select(__ => __.Technique),
                    _ => _.AdvisorTopics.Select(__ => __.Advisor), _ => _.Program)
                    .Where(_ => _.AdvisorTopics.Any(__ => __.AdvisorId == account.Code))
                    .OrderByDescending(_ => _.CreatedDate).ToList());
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/getall")]
        public IHttpActionResult GetAllTest()
        {
            try
            {


                var result = ModelMapper.ConvertToViewModel(
                    _topicService.GetAll(_ => _.TopicTechniques.Select(__ => __.Technique),
                    _ => _.AdvisorTopics.Select(__ => __.Advisor), _ => _.Program)
                    .Where(_ => _.Id == 1)
                    .OrderByDescending(_ => _.CreatedDate).ToList());
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/topics/search")]
        public IHttpActionResult SearchTopic(string name, int pageIndex, int pageSize,
            string sort = "createdDate", bool isAsc = false)
        {
            try
            {
                var result = ModelMapper.ConvertToSearchVM(_topicService.GetAll(_ => _.AdvisorTopics.Select(s => s.Advisor.Account))
                    .Where(_ => _.Name_En.ToLower().Contains(name.ToLower())).ToList());

                switch (sort)
                {
                    case "createdDate":
                        if (isAsc) result = result.OrderBy(_ => _.CreatedDate).ToList();
                        else result = result.OrderByDescending(_ => _.CreatedDate).ToList();
                        break;
                    case "name_En":
                        if (isAsc) result = result.OrderBy(_ => _.Name_En).ToList();
                        else result = result.OrderByDescending(_ => _.Name_En).ToList();
                        break;
                }


                Pager pager = new Pager(result.Count, pageIndex, pageSize);
                var paginationResult = result.Skip(pager.StartIndex).Take(pager.PageSize);

                return Ok(new { result = paginationResult, totalItems = result.Count });
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

    }
}
