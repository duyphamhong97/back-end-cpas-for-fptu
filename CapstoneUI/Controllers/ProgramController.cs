﻿using BusinessLogic.Define;
using CapstoneUI.ViewModels;
using DataAccess.Entities;
using System;
using System.Linq;
using System.Web.Http;

namespace CapstoneUI.Controllers
{
    public class ProgramController : BaseController
    {
        private readonly IProgramService _programService;

        public ProgramController(IProgramService programService)
        {
            _programService = programService;
        }

        [HttpPost]
        public IHttpActionResult Create(ProgramCreateVM viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                _programService.Create(new Program
                {
                    DepartmentProgramId = 1,
                    Name = viewModel.Name,
                    ShortName = viewModel.ShortName,
                });
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut]
        public IHttpActionResult Update(ProgramViewVM viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = _programService.Get(_ => _.Id == viewModel.Id);
                result.Name = viewModel.Name;
                result.ShortName = viewModel.ShortName;

                _programService.Update(result);
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            try
            {
                var result = ModelMapper.ConvertToViewModel(_programService.GetAll().ToList());
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
