﻿using BusinessLogic.Define;
using CapstoneUI.Extensions;
using CapstoneUI.Firebase;
using CapstoneUI.ViewModels;
using DataAccess.Entities;
using DataAccess.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CapstoneUI.Controllers
{
    public class StudentController : BaseController
    {
        private readonly IStudentService _studentService;
        private readonly ITopicService _topicService;
        private readonly IAccountService _accountService;
        private readonly IStudentGroupService _groupService;
        private readonly IGradeService _gradeService;

        public static bool notiFlag = true;

        public StudentController(IStudentService studentService, ITopicService topicService,
            IAccountService accountService, IStudentGroupService groupService, IGradeService gradeService)
        {
            _studentService = studentService;
            _topicService = topicService;
            _accountService = accountService;
            _groupService = groupService;
            _gradeService = gradeService;
        }

        [HttpPost]
        public IHttpActionResult Create(List<StudentCreateVM> viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }
                var result = ModelMapper.ConvertToStudentModel(viewModel.ToList());
                foreach (var item in viewModel)
                {
                    var existed = _studentService.Get(_ => _.Code == item.Code, _ => _.Account);
                    if (existed != null)
                    {
                        if (existed.Status == StudentStatus.Disable)
                        {
                            existed.Status = StudentStatus.New;
                            _studentService.Update(existed);

                            var account = existed.Account;
                            account.IsActive = true;
                            _accountService.Update(account);
                        }
                    } else
                    {
                        // create email
                        var name = item.FullName.ToLower().UTF8toASCII().Split(' ');
                        var ful = name.Aggregate("", (re, it) => re + it.ElementAt(0));
                        var email = name.Last() + ful.Substring(0, ful.Length - 1) + item.Code.ToLower() + "@fpt.edu.vn";
                        
                        // create account
                        var account = _accountService.Get(_ => _.Email == email);
                        if (account == null)
                        {
                            _accountService.Create(new Account
                            {
                                Email = email,
                                IsActive = true,
                                AccountRoleId = 6,
                                Code = item.Code,
                                FullName = item.FullName
                            });
                        }


                        // create student
                        _studentService.Create(new Student {
                            Code = item.Code,
                            ProgramId = item.ProgramId,
                            Status = StudentStatus.New,
                            GradeStatus = GradeStatus.Studying
                        });

                        
                    }
                }
                return Ok();

            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut, Route("api/script/group/topic")]
        public IHttpActionResult Script()
        {
            try
            {
                _studentService.GetAll(_ => _.StudentGroup).Where(_ => _.TopicPickId == null)
                    .GroupBy(_ => _.StudentGroupId, (a, b) => b).ToList()
                    .ForEach(_ =>
                    {
                        var topicTaken = _studentService.GetAll(q => q.StudentGroup.Topic)
                        .Where(z => z.TopicPickId != null).Select(a => a.TopicPickId).ToList();
                        var topicId = _topicService.Get(d => !topicTaken.Contains(d.Id) && d.Status == Status.Published);
                        if (topicId != null)
                        {
                            _.ToList().ForEach(s =>
                            {
                                s.TopicPickId = topicId.Id;

                                _studentService.Update(s);
                            });
                        }
                    });

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        public IHttpActionResult GetAllStudent()
        {
            try
            {

                var result = ModelMapper.ConvertToViewModel(_studentService.GetAll(_ => _.Program, _ => _.Account)
                    .ToList());
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        

        [HttpPut, Route("api/student/deadline")]
        public async Task<IHttpActionResult> DeadlineeAsync()
        {
            try
            {
                var groups = _groupService.GetAll(_ => _.Students)
                    .Where(_ => _.TopicId == null).ToList()
                    .Where(_ => _.EnrollTime.Add(_.Duration) < DateTimeOffset.Now);
                foreach (var group in groups)
                {
                    if (group.Students.Count == 0)
                        continue;
                    // softcode
                    var topicChoice = group.Students.ToList().GroupBy(_ => _.TopicPickId, (topicId, stus)
                        => new { TopicId = topicId, Count = stus.Count() });

                    var topicMax = topicChoice.FirstOrDefault(_ => _.Count == topicChoice.Max(__ => __.Count));

                    if (topicMax.TopicId != null)
                    {
                        if (((double)topicMax.Count / group.Students.Count) >= (double)2 / 3)
                        {
                            group.TopicId = topicMax.TopicId;
                            _groupService.Update(group);

                            if (topicMax.TopicId != null)
                            {
                                var topic = _topicService.Get(t => t.Id == topicMax.TopicId);
                                topic.Status = Status.Taken;
                                _topicService.Update(topic);
                            }

                            group.Students.ToList().ForEach(_ =>
                            {
                                _.Status = StudentStatus.TopicTaken;
                                _studentService.Update(_);
                            });

                            var topicDB = _topicService.Get(z => z.Id == topicMax.TopicId, _ => _.AdvisorTopics);

                            await FirebaseDatabase.GetFirebaseDatabase().sendMessage(new FireBaseVM
                            {
                                IsReading = false,
                                UserId = topicDB.AdvisorTopics.FirstOrDefault(x => x.Role == "Supervisor").AdvisorId,
                                TopicName = topicDB.Name_En,
                                Status = NotiStatus.STUDENT_PICK_TOPIC_AD,
                                CreatedDate = DateTimeOffset.Now.ToUnixTimeMilliseconds()
                            });

                            notiFlag = true;
                        }
                    }
                    
                }

                var groupLast = _groupService.GetAll().OrderByDescending(_ => _.EnrollTime).Take(1).ToList();
                if (groupLast.Count > 0)
                {
                    if (groupLast[0].EnrollTime.Add(groupLast[0].Duration) <= DateTimeOffset.Now && notiFlag)
                    {
                        await FirebaseDatabase.GetFirebaseDatabase().sendTrainingStaff("", 0, NotiStatus.SYS_DEADLINE_TS);
                        notiFlag = false;
                    }
                }

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/ts/student")]
        public IHttpActionResult GetStudentTrainingStaff(bool isNew)
        {
            try
            {
                if (isNew)
                {
                    return Ok(ModelMapper.ConvertToViewModel(_studentService.GetAll(_ => _.Program, _ => _.Account)
                    .Where(_ => _.Status == StudentStatus.New).ToList()));
                }
                var result = ModelMapper.ConvertToViewModel(_studentService.GetAll(_ => _.Program, _ => _.Account)
                    .Where(_ => _.Status >= StudentStatus.Grouping && _.Status <= StudentStatus.PointDone).ToList());
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        [HttpPut, Route("api/student/topic"), Authorize]
        public IHttpActionResult PickTopic(PickTopicVM viewModel)
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("0")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                var user = _studentService.Get(_ => _.Code == account.Code);

                #endregion

                var group = _groupService.Get(_ => _.Students.Any(__ => __.Code == user.Code));

                if (DateTimeOffset.Now < group.EnrollTime)
                    return BadRequest("Chưa tới giờ");

                // save phone and email
                var student = _studentService.Get(_ => _.Code == user.Code);
                student.Phone = viewModel.Phone;
                student.TopicPickId = viewModel.TopicId;
                _studentService.Update(student);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/student/topic/pick-time"), Authorize]
        public IHttpActionResult GetPickTime()
        {
            try
            {
                //Authorize
                #region Identify user

                if (!(User.IsInRole("0")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                var user = _studentService.Get(_ => _.Code == account.Code);

                #endregion

                var group = _groupService.Get(_ => _.Students.Any(__ => __.Code == user.Code), _ => _.Students,
                    _ => _.Topic);
                

                if (group == null || group.EnrollTime < DateTimeOffset.Parse("01/01/2000"))
                {
                    return Ok();
                }

                if(group.EnrollTime > DateTimeOffset.Now)
                {
                    return Ok(new
                    {
                        EnrollTime = (long)group.EnrollTime.ToUnixTimeMilliseconds(),
                        Duration = (long)group.Duration.TotalMilliseconds
                    });
                }

                var deadline = group.EnrollTime.Add(group.Duration) - DateTimeOffset.Now;
                

                return Ok(new
                {
                    EnrollTime = (long)group.EnrollTime.ToUnixTimeMilliseconds(),
                    Duration = (long)deadline.TotalMilliseconds,
                    TopicName = group.Topic == null ? null : group.Topic.Name_En,
                    group.TopicId
                });


            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("api/student/topic"), Authorize]
        public async Task<IHttpActionResult> GetStudentTopics()
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("0")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                var user = _studentService.Get(_ => _.Code == account.Code);

                #endregion

                

                var group = _groupService.Get(_ => _.Students.Any(__ => __.Code == user.Code), _ => _.Students);

                if (group == null) return Ok(new List<int>());

                var timePick = group.EnrollTime.Add(group.Duration);
                if (timePick < DateTimeOffset.Now)
                {
                    return Ok(new List<int>());
                }

                if (group.EnrollTime > DateTimeOffset.Now)
                {
                    var topics = _topicService.GetAll(_ => _.AdvisorTopics.Select(__ => __.Advisor.Account))
                        .Where(_ => _.Status == Status.Published && _.ProgramId == user.ProgramId).ToList();
                    return Ok(ModelMapper.ConvertToStudentVM(topics));
                }
                else
                {
                    var topics = _topicService.GetAll(_ => _.AdvisorTopics.Select(__ => __.Advisor.Account),
                        _ => _.StudentGroups)
                    .Where(_ => (_.Status == Status.Published || _.StudentGroups.Any(g => g.Id == group.Id))
                    && _.ProgramId == user.ProgramId).ToList();

                    topics.ForEach(_ => {
                        if (user.TopicPickId == _.Id)
                        {
                            _.Status = Status.Taken;
                        } else
                        {
                            _.Status = Status.Available;
                        }
                    });

                    return Ok(ModelMapper.ConvertToStudentVM(topics));
                }

                

            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/student/group"), Authorize]
        public IHttpActionResult GetGroup()
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("0")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                var user = _studentService.Get(_ => _.Code == account.Code);

                #endregion

                var result = _groupService.Get(_ => _.Students.Any(s => s.Code == user.Code), 
                    _ => _.Students.Select(s => s.Account));
                if (result == null)
                {
                    return Ok();
                }

                return Ok(result.Students.Select(_ => new { 
                    _.Code,
                    _.Account.FullName,
                    _.Account.Email,
                    _.LeaderCode
                }));
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet, Route("api/student/announcement"), Authorize]
        public IHttpActionResult GetAnnouncement()
        {
            return Ok(Announcement.Authorize_DATA );
        }

        [HttpGet, Route("api/unauthorize/announcement")]
        public IHttpActionResult GetAnnouncementUnauthorize()
        {
            return Ok(Announcement.Unauthorize_DATA);
        }

        [HttpGet, Route("api/student/grade"), Authorize]
        public IHttpActionResult GetGrade()
        {
            try
            {
                #region Identify user

                if (!(User.IsInRole("0")))
                {
                    return Unauthorized();
                }

                var uid = User.Identity.GetClaim("uid");
                var account = _accountService.Get(_ => _.Uid == uid);

                if (account == null) return Unauthorized();
                var user = _studentService.Get(_ => _.Code == account.Code);

                #endregion

                var grades = _gradeService.GetAll(_ => _.GradeType).Where(_ => _.StudentCode == user.Code)
                .OrderBy(_ => _.GradeTypeId)
                .Select(_ => new GradeItemVM
                {
                    GradeId = _.Id,
                    Name = _.GradeType.Name,
                    Percent = _.GradeType.Percent,
                    Value = _.Value,
                    Note = _.Note
                }).ToList();

                return Ok(new GradeStudentVM
                {
                    FinalProject = grades.FirstOrDefault(),
                    Reports = grades.Skip(1).Take(9).ToList(),
                    FinalProjectResit = grades.FirstOrDefault(_ => _.Name == "Final Project Resit"),
                    Status = (int)user.GradeStatus
                });

            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

    }
}
