﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapstoneUI.Firebase
{
    public class FirebaseMessageModel
    {
        public int topicId { get; set; }
        public string userId { get; set; }
        public string topicName { get; set; }
        public int topicStatus { get; set; }
        public string userName { get; set; }
        public bool isReading { get; set; }
        public long createdDate { get; set; }
        public int status { get; set; }
        public int count { get; set; }

        public FirebaseMessageModel(int topicId, string userId, string topicName, 
            int topicStatus, string userName, bool isReading, long createdDate, int status, int count)
        {
            this.topicId = topicId;
            this.userId = userId;
            this.topicName = topicName;
            this.topicStatus = topicStatus;
            this.userName = userName;
            this.isReading = isReading;
            this.createdDate = createdDate;
            this.status = status;
            this.count = count;
        }
        
    }
}