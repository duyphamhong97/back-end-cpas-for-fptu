﻿using CapstoneUI.Controllers;
using CapstoneUI.ViewModels;
using Firebase.Database;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace CapstoneUI.Firebase
{
    public class FirebaseDatabase
    {
        private static readonly String urlFireBase = "https://capstone-web-firebase.firebaseio.com";
        private static FirebaseDatabase _instance;
        private FirebaseClient client;

        // Lock synchronization object

        private static object syncLock = new object();

        // Constructor (protected)
        protected FirebaseDatabase()
        {
            this.client = new FirebaseClient(urlFireBase);
        }

        public static FirebaseDatabase GetFirebaseDatabase()
        {
            // Support multithreaded applications through

            // 'Double checked locking' pattern which (once

            // the instance exists) avoids locking each

            // time the method is invoked

            if (_instance == null)
            {
                lock (syncLock)
                {
                    if (_instance == null)
                    {
                        _instance = new FirebaseDatabase();
                    }
                }
            }
            return _instance;
        }

        public async Task<FirebaseObject<string>> sendMessage(FireBaseVM viewModel)
        {
            // default sending message
            viewModel.IsReading = false;

            var messageTopic = "messages/" + viewModel.UserId;
            var obj = await client
                .Child(messageTopic)
                .PostAsync(
                JsonConvert.SerializeObject(new FirebaseMessageModel(viewModel.TopicId, viewModel.UserId, 
                viewModel.TopicName, viewModel.TopicStatus, viewModel.UserName, viewModel.IsReading, 
                viewModel.CreatedDate, (int)viewModel.Status, viewModel.CountDuplicate)));
            return obj;
        }

        public async Task<FirebaseObject<string>> sendTrainingStaff(string userName, int count, NotiStatus status)
        {
            // default sending message

            var messageTopic = "messages/" + "T";
            var obj = await client
                .Child(messageTopic)
                .PostAsync(
                JsonConvert.SerializeObject(new
                {
                    userId = "T",
                    userName = userName,
                    count = count,
                    isReading = false,
                    createdDate = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                    status = (int)status
                }));
            return obj;
        }

        public async Task<FirebaseObject<string>> SendStudent(string code, NotiStatus status, DateTimeOffset enrollTime = new DateTimeOffset())
        {
            // default sending message

            var messageTopic = "messages/" + code;
            var obj = await client
                .Child(messageTopic)
                .PostAsync(
                JsonConvert.SerializeObject(new
                {
                    userId = code,
                    enrollTime = enrollTime.ToString("dd/MM/yyyy hh:mm:ss"),
                    isReading = false,
                    createdDate = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                    status = (int)status
                }));
            return obj;
        }
    }
}