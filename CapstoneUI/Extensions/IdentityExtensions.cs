﻿using System.Security.Claims;
using System.Security.Principal;

namespace CapstoneUI.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetClaim(this IIdentity identity, string nameOfClaim)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst(nameOfClaim);
            return (claim != null) ? claim.Value : string.Empty;
        }
    }
}