﻿using BusinessLogic.Define;
using CapstoneUI.Extensions;
using DataAccess.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CapstoneUI.Utils
{
    public class DuplicationChecker
    {

        public async static Task Execute(Topic target, ICollection<Topic> allTopic,
            HttpClient client, IDuplicatedTopicService duplicatedTopicService)
        {
            string doc = target.Abstraction.Makeup();

            string abstraction = target.Abstraction.Makeup();
            string mainfunction = target.MainFunction.Makeup();

            duplicatedTopicService.GetAll().Where(_ => _.TopicId == target.Id).ToList()
                .ForEach(_ =>
                {
                    duplicatedTopicService.Delete(_);
                });

            foreach (var topic in allTopic)
            {
                // Abstraction

                #region Abstraction

                string abstractionTarget = topic.Abstraction.Makeup();
                if (!(abstraction.Length < 20 || abstractionTarget.Length < 20))
                {
                    // Check duplicate levenshein
                    var step1 = LevenshteinDistance.CalculateSimilarity(abstraction, abstractionTarget);
                    if (step1 >= 0.9)
                    {
                        var a = abstraction.Split(' ');
                        var b = abstractionTarget.Split(' ');

                        var duplica = a.Intersect(b);
                        duplicatedTopicService.Create(new DuplicatedTopic
                        {
                            TopicId = target.Id,
                            DuplicatedTopicId = topic.Id,
                            Percentage = step1,
                            Keywords = string.Join(",", duplica)
                        });

                    } else
                    {
                        var tf1 = FindKeyword(abstractionTarget).OrderByDescending(_ => _.Value);
                        var tf2 = FindKeyword(abstraction).OrderByDescending(_ => _.Value);

                        var result1 = FindPercentage(tf1.ToList(), tf2.ToList());
                        if (result1 > double.Parse(ConfigurationManager.AppSettings.Get("DuplicateRate")))
                        {
                            var keya = FindKeyword(tf1.ToList(), tf2.ToList());
                            var keyb = FindKeyword(tf2.ToList(), tf1.ToList());

                            var keywords = string.Join(",", keya);
                            keywords = keywords + "," + string.Join(",", keyb);
                            var distict = keywords.Split(',').Distinct();
                             keywords = string.Join(",", distict);

                            duplicatedTopicService.Create(new DuplicatedTopic
                            {
                                TopicId = target.Id,
                                DuplicatedTopicId = topic.Id,
                                Keywords = keywords,
                                Percentage = result1
                            });

                            continue;
                        }
                        //// Execute result
                        //var objResult = await FindKeyword(client, abstraction, abstractionTarget);
                        //int keyNumber = 40;

                        //var doca = objResult.ElementAt(0).OrderByDescending(x => x.Value).Take(keyNumber).ToList();
                        //var docb = objResult.ElementAt(1).OrderByDescending(x => x.Value).Take(keyNumber).ToList();

                        //// find keyword duplicate percentage
                        //var finalResult = FindPercentage(doca, docb, keyNumber);
                        //if (finalResult > double.Parse(ConfigurationManager.AppSettings.Get("DuplicateRate")))
                        //{
                        //    var duplicateWord = docb.Where(k => k.Key.ToString() != "" && (double)k.Value > docb.Average(j => (double)j.Value))
                        //        .Select(k => k.Key.ToString()).ToList();


                        //    duplicatedTopicService.Create(new DuplicatedTopic
                        //    {
                        //        TopicId = target.Id,
                        //        DuplicatedTopicId = topic.Id,
                        //        Percentage = finalResult,
                        //        Keywords = string.Join(",", duplicateWord)
                        //    });
                        //}
                    }
                }

                #endregion

                #region Main Function
                string functionTarget = topic.MainFunction.Makeup();
                if (functionTarget.Length < 20 || mainfunction.Length < 20)
                {
                    continue;
                }
                
                // Check duplicate levenshein
                var step2 = LevenshteinDistance.CalculateSimilarity(mainfunction, functionTarget);
                if (step2 >= 0.9)
                {
                    var a = mainfunction.Split(' ');
                    var b = functionTarget.Split(' ');

                    var duplica = a.Intersect(b);
                    duplicatedTopicService.Create(new DuplicatedTopic
                    {
                        TopicId = target.Id,
                        DuplicatedTopicId = topic.Id,
                        Percentage = step2,
                        Keywords = string.Join(",", duplica.ToList())
                    });

                    continue;
                } else
                {
                    var tf1 = FindKeyword(functionTarget).OrderByDescending(_ => _.Value);
                    var tf2 = FindKeyword(mainfunction).OrderByDescending(_ => _.Value);

                    var result1 = FindPercentage(tf1.ToList(), tf2.ToList());
                    if (result1 > 0.7)
                    {
                        var keya = FindKeyword(tf1.ToList(), tf2.ToList());
                        var keyb = FindKeyword(tf2.ToList(), tf1.ToList());

                        var up = duplicatedTopicService.Get(_ => _.TopicId == target.Id && _.DuplicatedTopicId == topic.Id);
                        if (up == null)
                        {
                            var keywords = string.Join(",", keya);
                            keywords = keywords + "," + string.Join(",", keyb);
                            var distict = keywords.Split(',').Distinct();
                            keywords = string.Join(",", distict);

                            duplicatedTopicService.Create(new DuplicatedTopic
                            {
                                TopicId = target.Id,
                                DuplicatedTopicId = topic.Id,
                                Keywords = keywords,
                                Percentage = result1
                            });
                        } else
                        {
                            up.Keywords = up.Keywords + "," + string.Join(",", keya, keyb);
                            var distict = up.Keywords.Split(',').Distinct();
                            up.Keywords = string.Join(",", distict);
                            up.Percentage = (result1 + up.Percentage) / 2;

                            duplicatedTopicService.Update(up);
                        }
                        

                        continue;
                    }
                    //var objResult = await FindKeyword(client, abstraction, abstractionTarget);

                    //// Execute result
                    //int keyNumber = 40;

                    //var doca = objResult.ElementAt(0).OrderByDescending(x => x.Value).Take(keyNumber).ToList();
                    //var docb = objResult.ElementAt(1).OrderByDescending(x => x.Value).Take(keyNumber).ToList();

                    //// find keyword duplicate percentage
                    //var finalResult = FindPercentage(doca, docb, keyNumber);
                    //if (finalResult > double.Parse(ConfigurationManager.AppSettings.Get("DuplicateRate")))
                    //{
                    //    var duplicateWord = docb.Where(k => k.Key.ToString() != "" && (double)k.Value > docb.Average(j => (double)j.Value))
                    //        .Select(k => k.Key.ToString()).ToList();

                    //    var up = duplicatedTopicService.Get(_ => _.TopicId == target.Id && _.DuplicatedTopicId == topic.Id);
                    //    up.Keywords = up.Keywords + "," + string.Join(",", duplicateWord);
                    //    var distict = up.Keywords.Split(',').Distinct();
                    //    up.Keywords = string.Join(",", distict);

                    //    duplicatedTopicService.Update(up);
                    //}
                }

                #endregion
            }
        }

        private static List<string> FindSentence(string source, List<string> duplicatedWord)
        {
            List<string> sentence = new List<string>();

            foreach (var item in source.Split('.'))
            {
                var a = item.Split(' ').Intersect(duplicatedWord);
                if (a.Count() >= 2)
                {
                    sentence.Add(item.Substring(item.IndexOf(a.First()), item.IndexOf(a.Last())));
                }
            }

            return sentence;
        }
        

        private static IDictionary<string, double> FindKeyword(string source)
        {
            IDictionary<string, double> result = new Dictionary<string, double>();
            var dict = source.Replace('.', ' ').SingleSpace().Split(' ').ToList();
            var distint = dict.Distinct().ToList();
            foreach (var item in distint)
            {
                result.Add(item, (double)dict.Count(_ => _ == item) / dict.Count);
            }
            
            return result;
        }
        

        private async static Task<List<Dictionary<object, object>>> FindKeyword(HttpClient client, string source, string target)
        {
            var request = JsonConvert.SerializeObject(new
            {
                doca = source,
                docb = target
            });

            var buffer = Encoding.UTF8.GetBytes(request);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            // Call api
            HttpResponseMessage respone = await client.PostAsync("", byteContent);
            var result = await respone.Content.ReadAsStringAsync();

            var objResult = JsonConvert.DeserializeObject<List<Dictionary<object, object>>>(result);

            return objResult;
        }


        private static List<string> FindDuplicate(ICollection<KeyValuePair<object, object>> keys1,
                                    ICollection<KeyValuePair<object, object>> keys2)
        {
            List<string> result = new List<string>();
            foreach (var word1 in keys1.Select(_ => _.Key))
            {
                foreach (var word2 in keys2.Select(_ => _.Key))
                {
                    PorterStemmer stemmer = new PorterStemmer();

                    var similar = LevenshteinDistance.CalculateSimilarity(stemmer.StemWord(word1.ToString()),
                        stemmer.StemWord(word2.ToString()));
                    if (similar > 0.9)
                    {
                        result.Add(word2.ToString());
                    }
                }
            }
                    
            return result;
        }
        

        private static double FindPercentage(List<KeyValuePair<object, object>> keys1,
                                    List<KeyValuePair<object, object>> keys2, int keyNumber)
        {
            double[] max = new double[keyNumber];
            int i = 0;
            foreach (var word1 in keys1.Select(_ => _.Key))
            {
                foreach (var word2 in keys2.Select(_ => _.Key))
                {
                    var similar = LevenshteinDistance.CalculateSimilarity(word1.ToString(), word2.ToString());
                    if (similar > max[i])
                        max[i] = similar;
                }
                i++;
            }
            double sum = 0;
            foreach (var item in max)
            {
                sum += item;
            }

            return sum / keyNumber;
        }

        private static double FindPercentage(List<KeyValuePair<string, double>> keys1,
                                    List<KeyValuePair<string, double>> keys2)
        {
            
            var matchingAverage = (FindMaxRowMatrix(keys1, keys2) 
                + FindMaxRowMatrix(keys2, keys1)) / (keys1.Count + keys2.Count);

            return matchingAverage;
        }

        private static double FindMaxRowMatrix(List<KeyValuePair<string, double>> keys1,
                                    List<KeyValuePair<string, double>> keys2)
        {
            double[] max = new double[keys1.Count];
            int i = 0;
            foreach (var word1 in keys1.Select(_ => _.Key))
            {
                foreach (var word2 in keys2.Select(_ => _.Key))
                {
                    var similar = LevenshteinDistance.CalculateSimilarity(word1.ToString(), word2.ToString());
                    if (similar > max[i])
                        max[i] = similar;
                }
                i++;
            }
            double sum = 0;
            foreach (var item in max)
            {
                sum += item;
            }
            return sum;
        }

        private static List<string> FindKeyword(List<KeyValuePair<string, double>> keys1,
                                    List<KeyValuePair<string, double>> keys2)
        {
            double[] max = new double[keys1.Count];
            List<string> key = new List<string>();
            PorterStemmer stemmer = new PorterStemmer();
            int i = 0;
            foreach (var word1 in keys1.Select(_ => _.Key))
            {
                foreach (var word2 in keys2.Select(_ => _.Key))
                {
                    var similar = LevenshteinDistance.CalculateSimilarity(stemmer.StemWord(word1.ToString()), 
                        stemmer.StemWord(word2.ToString()));
                    if (similar > max[i])
                        max[i] = similar;
                }
                if (max[i] > 0.5)
                {
                    key.Add(word1);
                }
                i++;
            }
            

            return key;
        }
    }

    public static class TFIDF
    {
        public static string[] GetVocabulary(string document)
        {
            return document.Split(' ');
        }


        public static string RemoveStopWord(this string document)
        {
            var stop_words = new List<string> { "between", "yourself", "but", "again", "there", "about", "once", "during",
                    "out", "very", "having", "with", "they", "own", "an", "be", "some", "for", "do", "its", "yours",
                    "such", "into", "of", "most", "itself", "other", "off", "is", "s", "am", "or", "who", "as", "from",
                    "him", "each", "the", "themselves", "until", "below", "are", "we", "these", "your", "his",
                    "through", "don", "nor", "me", "were", "her", "more", "himself", "this", "down", "should", "our",
                    "their", "while", "above", "both", "up", "to", "ours", "had", "she", "all", "no", "when", "at",
                    "any", "before", "them", "same", "and", "been", "have", "in", "will", "on", "does", "yourselves",
                    "then", "that", "because", "what", "over", "why", "so", "can", "did", "not", "now", "under", "he",
                    "you", "herself", "has", "just", "where", "too", "only", "myself", "which", "those", "i", "after",
                    "few", "whom", "being", "if", "theirs", "my", "against", "a", "by", "doing", "it", "how",
                    "further", "was", "here", "than", "n/a", "via", "main proposal content", "current situation",
                    "building an application provides following services"};

            document = document.ToLower();
            foreach (var item in stop_words)
            {
                document = Regex.Replace(document, "\\W(" + item + ")\\W", " ");

            }

            return document;
        }

        public static string Makeup(this string document)
        {
            var stop_words = new List<string> { "between", "yourself", "but", "again", "there", "about", "once", "during",
                    "out", "very", "having", "with", "they", "own", "an", "be", "some", "for", "do", "its", "yours",
                    "such", "into", "of", "most", "itself", "other", "off", "is", "s", "am", "or", "who", "as", "from",
                    "him", "each", "the", "themselves", "until", "below", "are", "we", "these", "your", "his",
                    "through", "don", "nor", "me", "were", "her", "more", "himself", "this", "down", "should", "our",
                    "their", "while", "above", "both", "up", "to", "ours", "had", "she", "all", "no", "when", "at",
                    "any", "before", "them", "same", "and", "been", "have", "in", "will", "on", "does", "yourselves",
                    "then", "that", "because", "what", "over", "why", "so", "can", "did", "not", "now", "under", "he",
                    "you", "herself", "has", "just", "where", "too", "only", "myself", "which", "those", "i", "after",
                    "few", "whom", "being", "if", "theirs", "my", "against", "a", "by", "doing", "it", "how",
                    "further", "was", "here", "than", "n/a", "main proposal content", "current situation", "via",
                    "building an application provides following services"};
            
            document = document.ToLower();
            foreach (var item in stop_words)
            {
                document = Regex.Replace(document, "\\W(" + item + ")\\W", " ");

            }
            document = Regex.Replace(document, "\\W", " ");
            document = Regex.Replace(document, "[0-9]", " ");
            document = Regex.Replace(document, " \\w ", " ");
            document = Regex.Replace(document, " \\w{2} ", " ");
            
            document = document.Normalize();
            document = document.Replace("\n", " ");
            
            return document.Trim();
        }
    }


    public class LevenshteinDistance
    {
        public static double CalculateSimilarity(string source, string target)
        {
            if ((source == null) || (target == null)) return 0.0;
            if ((source.Length == 0) || (target.Length == 0)) return 0.0;
            if (source == target) return 1.0;

            int stepsToSame = ComputeLevenshteinDistance(source, target);
            return (1.0 - ((double)stepsToSame / (double)Math.Max(source.Length, target.Length)));
        }

        static int ComputeLevenshteinDistance(string source, string target)
        {
            if ((source == null) || (target == null)) return 0;
            if ((source.Length == 0) || (target.Length == 0)) return 0;
            if (source == target) return source.Length;

            int sourceWordCount = source.Length;
            int targetWordCount = target.Length;

            // Step 1
            if (sourceWordCount == 0)
                return targetWordCount;

            if (targetWordCount == 0)
                return sourceWordCount;

            int[,] distance = new int[sourceWordCount + 1, targetWordCount + 1];

            // Step 2
            for (int i = 0; i <= sourceWordCount; distance[i, 0] = i++) ;
            for (int j = 0; j <= targetWordCount; distance[0, j] = j++) ;

            for (int i = 1; i <= sourceWordCount; i++)
            {
                for (int j = 1; j <= targetWordCount; j++)
                {
                    // Step 3
                    int cost = (target[j - 1] == source[i - 1]) ? 0 : 1;

                    // Step 4
                    distance[i, j] = Math.Min(Math.Min(distance[i - 1, j] + 1, distance[i, j - 1] + 1), distance[i - 1, j - 1] + cost);
                }
            }

            return distance[sourceWordCount, targetWordCount];
        }
    }
    
}

