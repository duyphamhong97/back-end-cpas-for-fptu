﻿using CapstoneUI.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace CapstoneUI.Utils
{
    public class ZipfileUtils
    {
        public static IHttpActionResult ZipFile(List<ZipfileUtils.ZipFileModel> inputs)
        {
            return new ZipFileAsync(inputs);
        }

        public class ZipFileModel
        {
            public ZipFileModel(string NameInput, byte[] FileBytes)
            {
                Name = NameInput;
                Body = FileBytes;
            }
            public string Name { get; set; }
            public byte[] Body { get; set; }
        }

        public class ZipFileAsync : IHttpActionResult
        {
            private readonly string _prefix = ".doc";
            private readonly string _output = "topics.zip";

            public List<ZipfileUtils.ZipFileModel> FileList;

            public ZipFileAsync(List<ZipfileUtils.ZipFileModel> FileInputList)
            {
                FileList = FileInputList;
            }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                var memoryStream = new MemoryStream();
                var response = new HttpResponseMessage(HttpStatusCode.OK);

                // Check empty list
                if (FileList.Count == 0)
                {
                    return Task.FromResult(new HttpResponseMessage(HttpStatusCode.BadRequest));
                }

                using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    foreach (var item in FileList)
                    {
                        var entry = archive.CreateEntry(item.Name + _prefix);
                        var originalFileStream = new MemoryStream(item.Body);

                        using (var zipEntryStream = entry.Open())
                        {
                            //Copy the attachment stream to the zip entry stream
                            originalFileStream.CopyTo(zipEntryStream);
                        }
                    }

                }
                // Make pointer at start of memory
                memoryStream.Position = 0;

                response.Content = new StreamContent(memoryStream);
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = _output
                };
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/zip");
                return Task.FromResult(response);
            }
        }
    }
}