﻿using CapstoneUI.Extensions;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CapstoneUI.Utils
{
    public class StudentClassifyTest
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public double Weight { get; set; }
        public string LeaderCode { get; set; }
    }

    public class GroupWeight
    {
        public double Weight { get; set; }
        public int Count { get; set; }
    }
    public class ClassifyGroupUtil
    {
        public static List<List<Student>> Classify(List<Student> students)
        {
            var listSortedStudent = new List<Student>(); 

            var sortedGroup = students.GroupBy(g => g.WeightPoint, (w, g) => new {
                w,
                g
            }).OrderBy(_ => _.w).Select(_ => _.g.ToList()).ToList();

            sortedGroup.ForEach(g =>
            {
                g.Shuffle();
            });

            listSortedStudent = sortedGroup.SelectMany(_ => _).ToList();

            int max = int.Parse(ConfigurationManager.AppSettings.Get("GroupClassifyMax"));
            int min = int.Parse(ConfigurationManager.AppSettings.Get("GroupClassifyMin"));
            int ave = int.Parse(ConfigurationManager.AppSettings.Get("GroupClassifyAverage"));

            var largeGroup = new List<List<Student>>(); // result cuoi cung


            if (students.Count <= max)
            {
                largeGroup.Add(new List<Student>());

                var smallGroup = largeGroup.ElementAt(0);
                smallGroup.AddRange(listSortedStudent);

                return largeGroup;
            }

            int cntGroup = students.Count / ave;
            var studentRemain = students.Count % ave; // hs du
            int maxGroup = ave * (max - ave + 1);

            if (studentRemain >= min)
            {
                cntGroup = cntGroup + 1;
            }

            // chia sv cho nhom 
            for (int i = 0; i < cntGroup; i++)
            {
                largeGroup.Add(new List<Student>());
            }
            int index = 0;

            foreach (var student in listSortedStudent)
            {
                var smallGroup = largeGroup.ElementAt(index);
                smallGroup.Add(student);
                index++;
                if (index == cntGroup)
                {
                    index = 0;
                    largeGroup.Reverse();
                }
            }

            if (studentRemain >= min)
            {
                largeGroup.Reverse();
            }

            return largeGroup;
        }

        public static List<List<StudentClassifyTest>> TestClassify(List<StudentClassifyTest> student)
        {
            List<StudentClassifyTest> lstStudent = student;

            var listWeights = lstStudent.GroupBy(g => g.Weight).Select(s => new GroupWeight()
            {
                Weight = s.FirstOrDefault().Weight,
                Count = s.Count()
            }).OrderBy(o => o.Weight).ToList();

            var listSortedStudent = new List<StudentClassifyTest>(); // dùng list này để xếp
            var sortedGroup = new List<List<StudentClassifyTest>>();

            foreach (var weight in listWeights)
            {
                sortedGroup.Add(lstStudent.Where(w => w.Weight == weight.Weight).ToList());
            }
            var rd = new Random();
            foreach (var item in sortedGroup)
            {
                int index;
                while (item.Count > 0)
                {
                    index = rd.Next(0, item.Count - 1);
                    listSortedStudent.Add(item[index]);
                    item.RemoveAt(index);
                }
            }
            lstStudent = lstStudent.OrderBy(o => o.Weight).ToList();
            List<List<StudentClassifyTest>> group = new List<List<StudentClassifyTest>>();
            int cntGroup = listSortedStudent.Count % 4 < 2 ? listSortedStudent.Count / 4 : listSortedStudent.Count / 4 + 1; // nếu chia lấy dư < 2 (0, 1) thì nhóm k thay đổi, > 2 nhóm = +1
            if (cntGroup < 2)
            {
                listSortedStudent = listSortedStudent.OrderBy(o => o.Weight).ToList();
                for (int i = 0; i < listSortedStudent.Count; i++)
                {
                    if (i > 0)
                    {
                        listSortedStudent[i].LeaderCode = listSortedStudent[0].Code;
                    }
                }
                group.Add(listSortedStudent);
            }
            else
            {
                for (int i = 0; i < cntGroup; i++)
                {
                    group.Add(new List<StudentClassifyTest>());
                }

                var listtop1 = listSortedStudent.Take(cntGroup).ToList();
                for (int i = 0; i < cntGroup; i++)
                {
                    listtop1[i].LeaderCode = listSortedStudent[i].Code;
                    group[i].Add(listtop1[i]);
                }
                var listtop2 = listSortedStudent.Skip(cntGroup).Take(cntGroup).OrderByDescending(o => o.Weight).ToList();
                for (int i = 0; i < cntGroup; i++)
                {
                    listtop2[i].LeaderCode = listSortedStudent[i].Code;
                    group[i].Add(listtop2[i]);
                }
                var listtop3 = listSortedStudent.Skip(cntGroup * 2).Take(cntGroup).ToList();
                for (int i = 0; i < cntGroup; i++)
                {
                    listtop3[i].LeaderCode = listSortedStudent[i].Code;
                    group[i].Add(listtop3[i]);
                }
                List<StudentClassifyTest> listtop4;

                if (listSortedStudent.Count % 4 == 0)
                {
                    listtop4 = listSortedStudent.Skip(cntGroup * 3).Take(cntGroup).OrderByDescending(o => o.Weight).ToList();
                    for (int i = 0; i < listtop4.Count; i++)
                    {
                        listtop4[i].LeaderCode = listSortedStudent[i].Code;
                        group[i].Add(listtop4[i]);
                    }
                }
                else if (listSortedStudent.Count % 4 == 1)
                {
                    listtop4 = listSortedStudent.Skip(cntGroup * 3).Take(cntGroup).OrderByDescending(o => o.Weight).ToList();
                    int i = 0;
                    for (i = 0; i < listtop4.Count; i++)
                    {
                        listtop4[i].LeaderCode = listSortedStudent[i].Code;
                        group[i].Add(listtop4[i]);
                    }
                    if (i % 2 == 0)
                    {
                        var lstLast = listSortedStudent.LastOrDefault();
                        lstLast.LeaderCode = listSortedStudent.FirstOrDefault().Code;
                        group.FirstOrDefault().Add(lstLast);
                    }
                    else
                    {
                        var lstLast = listSortedStudent.LastOrDefault();
                        lstLast.LeaderCode = listSortedStudent.FirstOrDefault().Code;
                        group.LastOrDefault().Add(lstLast);
                    }

                }
                else if (listSortedStudent.Count % 4 == 3)
                {
                    if (listSortedStudent.Count >= 15 || listSortedStudent.Count == 7)
                    {
                        listtop4 = listSortedStudent.Skip(cntGroup * 3).Take(listSortedStudent.Count - cntGroup * 3).OrderByDescending(o => o.Weight).ToList();
                        for (int i = listtop4.Count; i > 0; i--)
                        {
                            listtop4[i - 1].LeaderCode = listSortedStudent[i].Code;
                            group[i].Add(listtop4[i - 1]);
                        }
                    }
                    else
                    {
                        listtop4 = listSortedStudent.Skip(cntGroup * 3).Take(cntGroup).OrderByDescending(o => o.Weight).ToList();
                        for (int i = 0; i < listtop4.Count; i++)
                        {
                            listtop4[i].LeaderCode = listSortedStudent[i].Code;
                            group[i].Add(listtop4[i]);
                        }
                    }
                }
                else
                {
                    listtop4 = listSortedStudent.Skip(cntGroup * 3).Take(cntGroup).OrderByDescending(o => o.Weight).ToList();
                    for (int i = 0; i < listtop4.Count; i++)
                    {
                        listtop4[i].LeaderCode = listSortedStudent[i].Code;
                        group[i].Add(listtop4[i]);
                    }
                }

            }

            return group;
        }

        public static List<List<Student>> ClassfifyGroup(List<Student> student)
        {
            List<Student> lstStudent = student;
            List<List<Student>> group = new List<List<Student>>();
            int cntGroup = lstStudent.Count % 4 < 2 ? lstStudent.Count / 4 : lstStudent.Count / 4 + 1;
            if (cntGroup < 2)
            {
                lstStudent = lstStudent.OrderBy(o => o.WeightPoint).ToList();
                for (int i = 0; i < lstStudent.Count; i++)
                {
                    if (i > 0)
                    {
                        lstStudent[i].LeaderCode = lstStudent[0].Code;
                    }
                }
                group.Add(lstStudent);
            }
            else
            {
                for (int i = 0; i < cntGroup; i++)
                {
                    group.Add(new List<Student>());
                }
                lstStudent = lstStudent.OrderBy(o => o.WeightPoint).ToList();
                var listtop1 = lstStudent.Take(cntGroup).ToList();
                for (int i = 0; i < cntGroup; i++)
                {
                    group[i].Add(listtop1[i]);
                }
                var listtop2 = lstStudent.Skip(cntGroup).Take(cntGroup).OrderByDescending(o => o.WeightPoint).ToList();
                for (int i = 0; i < cntGroup; i++)
                {
                    listtop2[i].LeaderCode = lstStudent[i].Code;
                    group[i].Add(listtop2[i]);
                }
                var listtop3 = lstStudent.Skip(cntGroup * 2).Take(cntGroup).ToList();
                for (int i = 0; i < cntGroup; i++)
                {
                    listtop3[i].LeaderCode = lstStudent[i].Code;
                    group[i].Add(listtop3[i]);
                }
                List<Student> listtop4;
                if (lstStudent.Count % 4 == 1)
                {
                    listtop4 = lstStudent.Skip(cntGroup * 3).Take(cntGroup).OrderByDescending(o => o.WeightPoint).ToList();
                }
                else
                {
                    listtop4 = lstStudent.Skip(cntGroup * 3).Take(lstStudent.Count - cntGroup * 3).OrderByDescending(o => o.WeightPoint).ToList();
                }
                for (int i = 0; i < listtop4.Count; i++)
                {
                    listtop4[i].LeaderCode = lstStudent[i].Code;
                    group[i].Add(listtop4[i]);
                }
                if (lstStudent.Count % 4 == 1)
                {
                    var lstLast = lstStudent.LastOrDefault();
                    lstLast.LeaderCode = lstStudent.FirstOrDefault().Code;
                    group.FirstOrDefault().Add(lstLast);

                }
            }
            return group;
        }
    }
}