﻿using DataAccess.Entities;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using HtmlToOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CapstoneUI.Utils
{
    public class ExportWord
    {
        public static byte[] HtmlToWord(string html)
        {
            using (MemoryStream generatedDocument = new MemoryStream())
            {
                using (WordprocessingDocument package = WordprocessingDocument
                    .Create(generatedDocument, WordprocessingDocumentType.Document))
                {
                    MainDocumentPart mainPart = package.MainDocumentPart;
                    if (mainPart == null)
                    {
                        mainPart = package.AddMainDocumentPart();
                        new Document(new Body()).Save(mainPart);
                    }

                    HtmlConverter converter = new HtmlConverter(mainPart);
                    converter.HtmlStyles.DefaultStyle = converter.HtmlStyles.GetStyle("c2");
                    converter.HtmlStyles.StyleMissing += delegate (object sender, StyleEventArgs e)
                    {
                        Console.WriteLine(e.Name);
                    };
                    Body body = mainPart.Document.Body;
                    var paragraphs = converter.Parse(html);

                    for (int i = 0; i < paragraphs.Count; i++)
                    {
                        body.Append(paragraphs[i]);
                    }

                    mainPart.Document.Save();
                }

                return generatedDocument.ToArray();
            }
        }

        public static string ToHtml(Topic topic, Advisor advisor, StudentGroup group)
        {
            string url = AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\Utils\\template.html";
            string template = File.ReadAllText(url);

            template = template.Replace("&lt;Advisor_Name&gt;", advisor.Account.FullName);
            template = template.Replace("&lt;Advisor_Email&gt;", advisor.Account.Email);
            template = template.Replace("&lt;Title&gt;", advisor.Title);

            template = template.Replace("&lt;Name_En&gt;", topic.Name_En);
            template = template.Replace("&lt;Name_Vi&gt;", topic.Name_Vi);
            template = template.Replace("&lt;Name_Short&gt;", topic.ShortName);
            template = template.Replace("&lt;Abstraction&gt;", topic.AbstractionHtml);
            template = template.Replace("&lt;Theory&gt;", topic.TheoryHtml);
            template = template.Replace("&lt;Main_Function&gt;", topic.MainFunctionHtml);
            template = template.Replace("&lt;Other_Product&gt;", topic.OtherProductsHtml);
            template = template.Replace("&lt;Other_Comment&gt;", topic.OtherCommentsHtml);
            
            #region Student
            string row = "";
            var head = "<td   colspan=\"1\" rowspan=\"1\" style=\"padding: 0pt 5.8pt 0pt 5.8pt;border-right-style: solid;border-bottom-color: #000000;border-top-width: 1pt;border-right-width: 1pt;border-left-color: #000000;vertical-align: middle;border-right-color: #000000;border-left-width: 1pt;border-top-style: solid;border-left-style: solid;border-bottom-width: 1pt;width: 57.5pt;border-top-color: #000000;border-bottom-style: solid;\">" +
                    "<p  style =\"margin: 0;color: #000000;font-size: 12pt;font-family: 'Times New Roman';padding-top: 6pt;padding-bottom: 6pt;line-height: 1.0;orphans: 2;widows: 2;text-align: center;\"><span   style=\"color: #000000;font-weight: 400;text-decoration: none;vertical-align: baseline;font-size: 12pt;font-family: 'Times New Roman';font-style: normal;\">";
            var last = "</span></p></td>";

            string head80 = "<td width=\"60%\"  colspan=\"1\" rowspan=\"1\" style=\"padding: 0pt 5.8pt 0pt 5.8pt;border-right-style: solid;border-bottom-color: #000000;border-top-width: 1pt;border-right-width: 1pt;border-left-color: #000000;vertical-align: middle;border-right-color: #000000;border-left-width: 1pt;border-top-style: solid;border-left-style: solid;border-bottom-width: 1pt;width: 57.5pt;border-top-color: #000000;border-bottom-style: solid;\">" +
                    "<p  style =\"margin: 0;color: #000000;font-size: 12pt;font-family: 'Times New Roman';padding-top: 6pt;padding-bottom: 6pt;line-height: 1.0;orphans: 2;widows: 2;text-align: center;\"><span   style=\"color: #000000;font-weight: 400;text-decoration: none;vertical-align: baseline;font-size: 12pt;font-family: 'Times New Roman';font-style: normal;\">";
            int index = 1;
            if (group != null)
            {
                
                foreach (var student in group.Students)
                {
                    row += "<tr style=\"height: 1pt; \">";
                    row += head + "Student " + (index++) + last;
                    row += head80 + student.Account.FullName + last;
                    row += head + student.Code + last;
                    row += head + " " + last;
                    row += head + student.Account.Email + last;
                    row += head + (student.LeaderCode == null? "Leader" : "Member") + last;
                    row += "</tr>";
                }
            } else
            {
                for (index = 1; index < 5; index++)
                {
                    row += "<tr   style=\"height: 1pt; \">";
                    row += head + "Student " + (index) + last;
                    row += head + "<br /> " + last;
                    row += head + "<br /> " + last;
                    row += head + "<br /> " + last;
                    row += head + "<br /> " + last;
                    row += head + "<br /> " + last;
                    row += "</tr>";
                }
            }
            template = template.Replace("&lt;Student&gt;", row);

            #endregion

            return template;
        }


        // To docx file
        public static void HtmlToWord()
        {
            const string filename = "test.docx"; // output

            string url = AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\Utils\\template.html";

            string html = File.ReadAllText(url); // input


            if (File.Exists(filename)) File.Delete(filename);

            using (MemoryStream generatedDocument = new MemoryStream())
            {
                using (WordprocessingDocument package = WordprocessingDocument
                    .Create(generatedDocument, WordprocessingDocumentType.Document))
                {
                    MainDocumentPart mainPart = package.MainDocumentPart;
                    if (mainPart == null)
                    {
                        mainPart = package.AddMainDocumentPart();
                        new Document(new Body()).Save(mainPart);
                    }

                    HtmlConverter converter = new HtmlConverter(mainPart);
                    converter.HtmlStyles.DefaultStyle = converter.HtmlStyles.GetStyle("c2");
                    converter.HtmlStyles.StyleMissing += delegate (object sender, StyleEventArgs e)
                    {
                        Console.WriteLine(e.Name);
                    };
                    Body body = mainPart.Document.Body;
                    var paragraphs = converter.Parse(html);

                    for (int i = 0; i < paragraphs.Count; i++)
                    {
                        body.Append(paragraphs[i]);
                    }

                    mainPart.Document.Save();
                }

                File.WriteAllBytes(filename, generatedDocument.ToArray());
            }
        }
    }
}