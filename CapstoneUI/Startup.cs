﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using BusinessLogic.Define;
using BusinessLogic.Implement;
using DataAccess.Database;
using DataAccess.Repositories;
using DataAccess.Repository.Implement;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;

[assembly: OwinStartup(typeof(CapstoneUI.Startup))]

namespace CapstoneUI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var configuration = new HttpConfiguration();
            WebApiConfig.Register(configuration);

            app.UseCors(CorsOptions.AllowAll);

            app.UseNinjectMiddleware(CreateKernel).UseNinjectWebApi(configuration);

            
        }

        private static StandardKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(new List<NinjectModule>
            {
                new InfrastructureDependency()
            });
            return kernel;
        }
    }

    public class InfrastructureDependency : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>();
            Bind<IEntityContext>().To<CapstoneContext>();
            Bind<IAdvisorTopicService>().To<AdvisorTopicService>();
            Bind<IStudentGroupService>().To<StudentGroupService>();
            Bind<IGradeService>().To<GradeService>();
            Bind<IGradeTypeService>().To<GradeTypeService>();
            Bind<ITopicService>().To<TopicService>();
            Bind<ITechniqueService>().To<TechniqueService>();
            Bind<IProgramService>().To<ProgramService>();
            Bind<IAdvisorService>().To<AdvisorService>();
            Bind<ISemesterService>().To<SemesterService>();
            Bind<IStudentService>().To<StudentService>();
            Bind<ICommentService>().To<CommentService>();
            Bind<IDuplicatedTopicService>().To<DuplicatedTopicService>();
            Bind<IWeightPointService>().To<WeightPointService>();
            Bind<IDepartmentProgramService>().To<DepartmentProgramService>();

            Bind<IAccountService>().To<AccountService>();
        }
    }
}
