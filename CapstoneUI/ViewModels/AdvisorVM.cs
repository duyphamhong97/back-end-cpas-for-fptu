﻿namespace CapstoneUI.ViewModels
{
    public class AdvisorVM
    {
        public string FullName { get; set; }

        public string Email { get; set; }

        public string Title { get; set; }

        public string Phone { get; set; }
    }
    
    public class AdvisorTopicVM
    {
        public string Id { get; set; }
    }

    public class AdvisorViewVM : AdvisorVM
    {
        public string Id { get; set; }
        public string Role { get; set; }
    }
    

}