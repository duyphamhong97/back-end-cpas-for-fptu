﻿using System;

namespace CapstoneUI.ViewModels
{
    public class SemesterVM
    {
        public long StartDate { get; set; }
        public long EndDate { get; set; }
        public string Name { get; set; }
    }

    public class SemesterViewVM : SemesterVM
    {
        public int Id { get; set; }
    }

    public class SemesterTopicVM
    {
        public int Id { get; set; }
    }

}