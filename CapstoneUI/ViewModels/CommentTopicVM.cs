﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapstoneUI.ViewModels
{

    public class CommentVM
    {
        public int TopicId { get; set; }
        public ICollection<QuestionVM> Questions { get; set; }
    }

    public class QuestionVM
    {
        public int Id { get; set; }
        public bool IsApproved { get; set; }
        public string Comment { get; set; }
    }

    public class QuestionViewVM : QuestionVM
    {
        public string Question { get; set; }
    }

    public class CommitteeCommentDHVM
    {
        public int CommitteeId { get; set; }
        public string CommitteeName { get; set; }
        public ICollection<QuestionViewVM> Questions { get; set; }
    }

    public class SetDeadlineVM
    {
        public int TopicId { get; set; }
        public long Deadline { get; set; }
    }


}