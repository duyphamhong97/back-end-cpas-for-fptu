﻿namespace CapstoneUI.ViewModels
{
    using System;
    using System.Collections.Generic;

    public class StudentVM
    {
        public string FullName { get; set; }
        public string Code { get; set; }
        public int ProgramId { get; set; }
    }

    public class StudentCreateVM : StudentVM
    {
    }

    public class StudentViewVM : StudentVM
    {
        public string ProgramName { get; set; }
        public int Status { get; set; } 
    }

    public class StudentDHVM
    {
        public string Code { get; set; }
        public double WeightPoint { get; set; }
    }

    public class StudentAdvisorCreateVM
    {
        public string Code { get; set; }
        public int WeightPoint { get; set; }
        public string Note { get; set; }
    }

    public class AdvisorWPVM
    {
        public string AdvisorId { get; set; }
        public string StudentCode { get; set; }
        public int Point { get; set; }
    }

    public class SendToAdvisorVM
    {
        public List<string> Students { get; set; }
        public List<string> Advisors { get; set; }
        public long Deadline { get; set; }
        public int ProgramId { get; set; }
    }

    public class GroupSchedule
    {
        public long EnrollTime { get; set; }
        public long Duration { get; set; }
    }

    public class GroupSchedulev2
    {
        public long StartTime { get; set; }
        public long EndTime { get; set; }
    }
    public class StudentGroupVM
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public bool IsLeader { get; set; }
        public double WeightPoint { get; set; }
        public bool IsChangeable { get; set; }
    }

    public class StudentPointVM
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public double AveragePoint { get; set; }
        public ICollection<StudentPointReviewVM> Reviews { get; set; }
    }

    public class StudentPointReviewVM
    {
        public string AdvisorName { get; set; }
        public int WeightPoint { get; set; }
        public string Note { get; set; }
    }
    
    public class PickTopicVM
    {
        public int TopicId { get; set; }
        public string Phone { get; set; }
    }
}