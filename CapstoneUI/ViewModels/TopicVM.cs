﻿namespace CapstoneUI.ViewModels
{
    using DataAccess.Entities;
    using System.Collections.Generic;

    public class TopicVM
    {
        public string Name_En { get; set; }

        public string Name_Vi { get; set; }

        public string ShortName { get; set; }

        public string Abstraction { get; set; }

        public string Theory { get; set; }

        public string MainFunction { get; set; }

        public string OtherProducts { get; set; }

        public string OtherComments { get; set; }

        public string AbstractionHtml { get; set; }

        public string TheoryHtml { get; set; }

        public string MainFunctionHtml { get; set; }

        public string OtherProductsHtml { get; set; }

        public string OtherCommentsHtml { get; set; }


    }

    public class TopicCreateVM : TopicVM
    {
        public ICollection<TechniqueCreateTopicVM> Techniques { get; set; }
        public int ProgramId { get; set; }
        public ICollection<AdvisorTopicVM> SubAdvisors { get; set; }
        public string AdvisorId { get; set; }
        public Status Status { get; set; }
    }

    public class TopicUpdateVM : TopicCreateVM
    {
        public int Id { get; set; }
    }

    public class TopicViewVM : TopicVM
    {
        public int Id { get; set; }
        public long CreatedDate { get; set; }
        public ICollection<TechniqueViewTopicVM> Techniques { get; set; }
        public ICollection<AdvisorViewVM> SubAdvisors { get; set; }
        public AdvisorViewVM Advisor { get; set; }
        public ProgramViewVM Program { get; set; }
        public Status Status { get; set; }
        public string Note { get; set; }
    }

    public class TopicStudentViewVM
    {
        public int Id { get; set; }
        public string Name_En { get; set; }
        public long CreatedDate { get; set; }
        public AdvisorViewVM Advisor { get; set; }
        public Status Status { get; set; }
    }


    public class TopicCommitteeVM
    {
        public int Id { get; set; }
        public long CreatedDate { get; set; }
        public long Deadline { get; set; }
        public string Name_En { get; set; }
        public bool IsValidated { get; set; }
        public AdvisorViewVM Advisor { get; set; }
    }

    public class TopicTSChangeStatusVM
    {
        public int Id { get; set; }
        public int Status { get; set; }
    }

    public class TopicDHVM
    {
        public int Id { get; set; }
        public bool IsApproved { get; set; }
        public string Content { get; set; }
        public CommentVM Comments { get; set; }
    }

    public class TopicSearchVM
    {
        public int Id { get; set; }
        public string Name_En { get; set; }
        public string AdvisorName { get; set; }
        public long CreatedDate { get; set; }
        public int Status { get; set; }
        public string Content { get; set; }

    }

    public class TopicDuplicateVM
    {
        public int Id { get; set; }
        public string Name_En { get; set; }
        public long CreatedDate { get; set; }
        public string AdvisorName { get; set; }

        public ICollection<TopicDuplicateItemVM> DuplicatedTopics { get; set; }
    }

    public class TopicDuplicateItemVM
    {
        public int Id { get; set; }
        public string Name_En { get; set; }
        public long CreatedDate { get; set; }
        public string AdvisorName { get; set; }
        public double Percent { get; set; }
        public ICollection<string> DuplicateWords { get; set; }

    }

    #region Send Topic To Committee

    public class TopicDHCommitteeVM
    {
        public ICollection<int> Topics { get; set; }
        public ICollection<string> Committees { get; set; }
        public long Deadline { get; set; }

    }
    #endregion

    #region Send Topic To Training Staff
    public class TopicDHTSVM
    {
        public ICollection<int> Topics { get; set; }
    }
    #endregion

}