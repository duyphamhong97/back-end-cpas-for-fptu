﻿namespace CapstoneUI.ViewModels
{
    using System.Collections.Generic;

    public class TechniqueVM
    {
        public string Name { get; set; }
    }

    public class TechniqueCreateVM : TechniqueVM { }

    public class TechniqueCreateTopicVM
    {
        public int Id { get; set; }
    }

    public class TechniqueViewTopicVM : TechniqueVM
    {
        public int Id { get; set; }
    }

    public class TechniqueUpdateVM 
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}