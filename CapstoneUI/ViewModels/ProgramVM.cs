﻿namespace CapstoneUI.ViewModels
{
    public class ProgramVM
    {
        public string Name { get; set; }

        public string ShortName { get; set; }
    }

    public class ProgramCreateVM : ProgramVM { }

    public class ProgramTopicVM
    {
        public int Id { get; set; }
    }

    public class ProgramViewVM : ProgramVM
    {
        public int Id { get; set; }
    }

    
}