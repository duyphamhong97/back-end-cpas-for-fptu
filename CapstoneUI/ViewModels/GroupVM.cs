﻿namespace CapstoneUI.ViewModels
{
    using System.Collections.Generic;

    public class GroupVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<StudentGroupVM> Students { get; set; }
        public bool IsChangeable { get; set; }

    }

    public class GroupScheduleVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProgramName { get; set; }
        public ICollection<StudentGroupVM> Students { get; set; }
        public long EnrollTime { get; set; }
        public long Duration { get; set; }
    }

    public class GroupFullVM
    {
        public int No { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Role { get; set; }
        public string Group { get; set; }
        public string TopicName { get; set; }
        public string AdvisorName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public class GroupNotFullHelperVM
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Role { get; set; }
        public string Group { get; set; }
        public string Program { get; set; }
    }

    public class GroupNotFullVM
    {
        public int No { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Role { get; set; }
        public string Group { get; set; }
    }
}