﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapstoneUI.ViewModels
{
    public class ZipFileVM
    {
        public ZipFileVM(string NameInput, byte[] FileBytes)
        {
            Name = NameInput;
            Body = FileBytes;
        }
        public string Name { get; set; }
        public byte[] Body { get; set; }
    }
}