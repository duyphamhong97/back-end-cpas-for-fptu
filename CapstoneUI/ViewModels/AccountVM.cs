﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapstoneUI.ViewModels
{
    public class AccountVM
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Title { get; set; }
    }

    public class AccountUpdateVM : AccountVM
    {
        public string Code { get; set; }
        public bool IsActive { get; set; }
    }
}