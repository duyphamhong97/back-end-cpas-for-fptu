﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapstoneUI.ViewModels
{
    public class FireBaseVM
    {
        public int TopicId { get; set; }
        public string UserId { get; set; }
        public string TopicName { get; set; }
        public int TopicStatus { get; set; }
        public string UserName { get; set; }
        public bool IsReading { get; set; }
        public long CreatedDate { get; set; }
        public NotiStatus Status { get; set; }
        public int CountDuplicate { get; set; }
    }

    public enum NotiStatus
    {
        AD_SUBMIT_TOPIC = 1, // Advisor submit topic
        DH_SEND_COMMITEE = 2, // Department head send to committee
        DH_REJECT_AD_TOPIC = 3, // Department head reject advisor topic
        CM_DEADLINE_SEND_DH = 4, // After committee validation deadline, notify to Department head
        DH_SEND_TOPIC_TS = 5, // Department head send topic to training staff

        AD_SAVE_DRAFT_TOPIC = 6, // Advisor save draft topic

        TS_SCHEDULE_STUDENT = 7, 

        DH_SEND_ADVISOR = 8, // DH send student to Advisor for weight point
        AD_DEADLINE_SEND_DH = 9, // Advisor Deadline

        DH_SEND_STUDENT_TS = 10, // Dh send student to ts 

        TS_SEND_STUDENT_DH = 11, // Ts first time send student to DH

        DH_GROUP_STUDENT = 12, // Student receive group

        STUDENT_PICK_TOPIC_AD = 13, // Advisor receive noti about group student pick her topic

        DH_REWRITE_AD = 14, // Department head send rewrite topic request to advisor

        SYS_DEADLINE_TS = 15, // System notify ts when last group had chosen topic
    }
}