﻿namespace CapstoneUI.ViewModels
{
    using CapstoneUI.Extensions;
    using DataAccess.Entities;
    using DataAccess.Resources;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class _ModelMapping
    {
        #region Topic
        public Topic ConvertToModel(TopicCreateVM viewModel)
        {
            var topic = new Topic
            {
                Name_En = viewModel.Name_En,
                Name_Vi = viewModel.Name_Vi,
                ShortName = viewModel.ShortName,
                Abstraction = viewModel.Abstraction,
                AbstractionHtml = viewModel.AbstractionHtml,
                MainFunction = viewModel.MainFunction,
                MainFunctionHtml = viewModel.MainFunctionHtml,
                Theory = viewModel.Theory,
                TheoryHtml = viewModel.TheoryHtml,
                OtherComments = viewModel.OtherComments,
                OtherCommentsHtml = viewModel.OtherCommentsHtml,
                OtherProducts = viewModel.OtherProducts,
                OtherProductsHtml = viewModel.OtherProductsHtml,
                TopicTechniques = ConvertToTopicModel(viewModel.Techniques),
                ProgramId = viewModel.ProgramId,
                CreatedDate = DateTimeOffset.Now,
            };

            topic.AdvisorTopics = ConvertToModel(viewModel.SubAdvisors);
            topic.AdvisorTopics.Add(new AdvisorTopic
            {
                AdvisorId = viewModel.AdvisorId,
                Role = "Supervisor"
            });
            return topic;
        }

        public TopicSearchVM ConvertToSearchVM(Topic model)
        {

            return new TopicSearchVM
            {
                Id = model.Id,
                Name_En = model.Name_En,
                AdvisorName = model.AdvisorTopics.FirstOrDefault(_ => _.Role == "Supervisor").Advisor.Account.FullName,
                CreatedDate = model.CreatedDate.ToUnixTimeMilliseconds(),
                Status = (int) model.Status,
                Content = model.Abstraction.Replace(@"[\\+\-]", "").SingleSpace().MaxLength(250),
            };
        }

        public ICollection<TopicSearchVM> ConvertToSearchVM(ICollection<Topic> model)
            => model.Select(_ => ConvertToSearchVM(_)).ToList();

        public TopicViewVM ConvertToViewModel(Topic model)
        {
            var topic = new TopicViewVM
            {
                Id = model.Id,
                Name_En = model.Name_En,
                Name_Vi = model.Name_Vi,
                ShortName = model.ShortName,
                CreatedDate = model.CreatedDate.ToUnixTimeMilliseconds(),
                Abstraction = model.Abstraction,
                AbstractionHtml = model.AbstractionHtml,
                MainFunction = model.MainFunction,
                MainFunctionHtml = model.MainFunctionHtml,
                Theory = model.Theory,
                TheoryHtml = model.TheoryHtml,
                OtherComments = model.OtherComments,
                OtherCommentsHtml = model.OtherCommentsHtml,
                OtherProducts = model.OtherProducts,
                OtherProductsHtml = model.OtherProductsHtml,
                Techniques = ConvertToViewModel(model.TopicTechniques),
                Program = new ProgramViewVM
                {
                    Id = model.Program.Id,
                    Name = model.Program.Name,
                    ShortName = model.Program.ShortName
                },
                Status = model.Status,
                Note = model.Note
                

            };
            if (model.AdvisorTopics == null) return topic;
            //{{hardcode}}
            var advisor = model.AdvisorTopics.FirstOrDefault(_ => _.Role.ToLower() == "supervisor").Advisor;
            topic.Advisor = new AdvisorViewVM
            {
                Id = advisor.Code,
                Email = advisor.Account.Email,
                FullName = advisor.Account.FullName,
                Phone = advisor.Phone,
                Role = "Supervisor",
                Title = advisor.Title
            };

            topic.SubAdvisors = model.AdvisorTopics.Where(_ => !(_.Role.ToLower() == "supervisor"))
                .Select(_ => new AdvisorViewVM
                {
                    Id = _.Advisor.Code,
                    Email = _.Advisor.Account.Email,
                    FullName = _.Advisor.Account.FullName,
                    Phone = _.Advisor.Phone,
                    Role = "Idea Owner",
                    Title = _.Advisor.Title
                }).ToList();
            return topic;
        }

        public TopicStudentViewVM ConvertToStudentVM(Topic model)
            => new TopicStudentViewVM
            {
                Id = model.Id,
                Name_En = model.Name_En,
                Status = model.Status,
                CreatedDate = model.CreatedDate.ToUnixTimeMilliseconds(),
                Advisor = new AdvisorViewVM
                {
                    FullName = model.AdvisorTopics.FirstOrDefault(_ => _.Role == "Supervisor").Advisor.Account.FullName
                }
            };

        public ICollection<TopicStudentViewVM> ConvertToStudentVM(ICollection<Topic> model)
            => model.Select(m => ConvertToStudentVM(m)).ToList();

        public ICollection<TopicViewVM> ConvertToViewModel(ICollection<Topic> model)
            => model.Select(m => ConvertToViewModel(m)).ToList();
        
        

        public TopicCommitteeVM ConvertToCommitteeVM(Topic model, string committeeId)
        {
            var topic = new TopicCommitteeVM
            {
                Id = model.Id,
                CreatedDate = model.CreatedDate.ToUnixTimeMilliseconds(),
                Name_En = model.Name_En,
                IsValidated = !model.Comments.Where(_ => _.CommitteeId == committeeId)
                .Any(_ => !_.IsApproved && _.Content == null),
                Deadline = model.Comments.FirstOrDefault().Deadline.ToUnixTimeMilliseconds()
            };
            
            var advisor = model.AdvisorTopics.FirstOrDefault(_ => _.Role.ToLower() == "supervisor").Advisor;
            topic.Advisor = new AdvisorViewVM
            {
                Id = advisor.Code,
                Email = advisor.Account.Email,
                FullName = advisor.Account.FullName,
                Phone = advisor.Phone,
                Role = "Supervisor",
                Title = advisor.Title
            };
            return topic;
        }

        public ICollection<TopicCommitteeVM> ConvertToCommitteeVM(ICollection<Topic> model, string committeeId)
            => model.Select(m => ConvertToCommitteeVM(m, committeeId)).ToList();

        #endregion

        #region Technique

        public TechniqueViewTopicVM ConvertToViewModel(TopicTechnique model)
            => new TechniqueViewTopicVM
            {
                Id = model.TechniqueId,
                Name = model.Technique.Name
            };

        public ICollection<TechniqueViewTopicVM> ConvertToViewModel(ICollection<TopicTechnique> model)
            => model.Select(m => ConvertToViewModel(m)).ToList();


        public Technique ConvertToModel(TechniqueCreateVM viewModel)
            => new Technique
            {
                Name = viewModel.Name
            };

        public TopicTechnique ConvertToTopicModel(TechniqueCreateTopicVM viewModel)
            => new TopicTechnique
            {
                TechniqueId = viewModel.Id
            };

        public ICollection<TopicTechnique> ConvertToTopicModel(ICollection<TechniqueCreateTopicVM> viewModel)
            => viewModel.Select(m => ConvertToTopicModel(m)).ToList();

        public ICollection<Technique> ConvertToModel(ICollection<TechniqueCreateVM> viewModels)
            => viewModels.Select(v => ConvertToModel(v)).ToList();

        public TechniqueViewTopicVM ConvertToViewModel(Technique model)
            => new TechniqueViewTopicVM
            {
                Id = model.Id,
                Name = model.Name
            };

        public ICollection<TechniqueViewTopicVM> ConvertToViewModel(ICollection<Technique> model)
            => model.Select(m => ConvertToViewModel(m)).ToList();


        #endregion

        #region Program
        public Program ConvertToModel(ProgramCreateVM viewModel)
            => new Program
            {
                Name = viewModel.Name,
                ShortName = viewModel.ShortName
            };

        public ProgramViewVM ConvertToViewModel(Program model)
            => new ProgramViewVM
            {
                Id = model.Id,
                Name = model.Name,
                ShortName = model.ShortName
            };

        public ICollection<ProgramViewVM> ConvertToViewModel(ICollection<Program> model)
            => model.Select(m => ConvertToViewModel(m)).ToList();

        public Program ConvertToTopicModel(ProgramTopicVM viewModel)
            => new Program
            {
                Id = viewModel.Id
            };


        #endregion

        #region Semester

        public Semester ConvertToModel(SemesterVM viewModel)
            => new Semester
            {
                StartDate = DateTimeOffset.FromUnixTimeMilliseconds(viewModel.StartDate),
                EndDate = DateTimeOffset.FromUnixTimeMilliseconds(viewModel.EndDate),
                Name = viewModel.Name
            };

        public SemesterViewVM ConvertToViewModel(Semester model)
        {
            return new SemesterViewVM
            {
                Id = model.Id,
                StartDate = model.StartDate.ToUnixTimeMilliseconds(),
                EndDate = model.EndDate.ToUnixTimeMilliseconds(),
                Name = model.Name
            };
        }

        public ICollection<SemesterViewVM> ConvertToViewModel(ICollection<Semester> model)
            => model.Select(m => ConvertToViewModel(m)).ToList();

        #endregion

        #region Advisor

        public AdvisorTopic ConvertToModel(AdvisorTopicVM viewModel)
            => new AdvisorTopic
            {
                AdvisorId = viewModel.Id,
                Role = "Idea owner"
            };

        public ICollection<AdvisorTopic> ConvertToModel(ICollection<AdvisorTopicVM> viewmodel)
            => viewmodel.Select(_ => ConvertToModel(_)).ToList();
        #endregion

        #region TopicTechnique

        #endregion

        #region Student
        public Student ConvertToModel(StudentCreateVM viewModel)
        => new Student
        {
             // {fix}ten
            Code = viewModel.Code,
            ProgramId = viewModel.ProgramId,
        };

        public ICollection<Student> ConvertToStudentModel(ICollection<StudentCreateVM> viewModel)
        => viewModel.Select(m => ConvertToModel(m)).ToList();

        public StudentViewVM ConvertToViewModel(Student model)
            => new StudentViewVM
            {
                Code = model.Code,
                FullName = model.Account.FullName,
                ProgramId = model.ProgramId,
                ProgramName = model.Program.ShortName,
                Status = (int) model.Status
            };

        public ICollection<StudentViewVM> ConvertToViewModel(ICollection<Student> model)
            => model.Select(m => ConvertToViewModel(m)).ToList();

        #endregion

        #region Comment

        public QuestionViewVM ConvertToViewModel(Comment model)
            => new QuestionViewVM
            {
                Id = model.QuestionType,
                Question = CommitteeQuestion.QUESTIONS[model.QuestionType].Question,
                Comment = model.Content,
                IsApproved = model.IsApproved
            };

        public ICollection<QuestionViewVM> ConvertToViewModel(ICollection<Comment> model)
            => model.Select(_ => ConvertToViewModel(_)).ToList();

        #endregion

    }
}