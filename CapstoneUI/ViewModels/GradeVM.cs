﻿using System.Collections.Generic;

namespace CapstoneUI.ViewModels
{
    public class GradeVM
    {
        public string StudentCode { get; set; }
        public string Note { get; set; }
        public double Value { get; set; }
        public int GradeTypeId { get; set; }
    }

    public class GradeStudentVM
    {
        public GradeItemVM FinalProject { get; set; }
        public ICollection<GradeItemVM> Reports { get; set; }
        public GradeItemVM FinalProjectResit { get; set; }
        public int Status { get; set; }
    }

    public class GradeItemVM
    {
        public int GradeId { get; set; }
        public string Name { get; set; }
        public double Percent { get; set; }
        public double Value { get; set; }
        public string Note { get; set; }
    }
    
    public class StudentGradeVM
    {
        public int GradeId { get; set; }
        public double Value { get; set; }
    }

    
}