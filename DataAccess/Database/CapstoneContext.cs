﻿namespace DataAccess.Database
{
    using DataAccess.Entities;
    using DataAccess.EntityConfiguration;
    using System.Data.Entity;

    public class CapstoneContext : DbContext, IEntityContext
    {
        public CapstoneContext() : base("Capstone") { }

        public DbSet<Topic> Topics { get; set; }
        public DbSet<Advisor> Advisors { get; set; }
        public DbSet<AdvisorTopic> AdvisorTopics { get; set; }
        public DbSet<Program> Programs { get; set; }
        public DbSet<Technique> Techniques { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentGroup> StudentGroups { get; set; }
        public DbSet<Semester> Semesters { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<GradeType> GradeTypes { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<DuplicatedTopic> DuplicatedTopics { get; set; }


        public object GetContext => this;

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new AdvisorConfig()).Add(new AdvisorTopicConfig())
                .Add(new ProgramConfig()).Add(new CommentConfig()).Add(new TechniqueConfig())
                .Add(new TopicConfig()).Add(new StudentConfig()).Add(new DuplicatedTopicConfig())
                .Add(new StudentGroupConfig()).Add(new SemesterConfig()).Add(new GradeConfig())
                .Add(new GradeTypeConfig()).Add(new AccountConfig()).Add(new DepartmentProgramConfig())
                .Add(new WeightPointConfig());
        }
    }
}
