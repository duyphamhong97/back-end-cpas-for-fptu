namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class studentDouble : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Student", "WeightPoint", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Student", "WeightPoint", c => c.Int());
        }
    }
}
