namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Advisor",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 50),
                        Title = c.String(maxLength: 50),
                        Phone = c.String(maxLength: 50),
                        DepartmentProgramId = c.Int(),
                    })
                .PrimaryKey(t => t.Code)
                .ForeignKey("dbo.Account", t => t.Code, cascadeDelete: true)
                .ForeignKey("dbo.DepartmentProgram", t => t.DepartmentProgramId)
                .Index(t => t.Code)
                .Index(t => t.DepartmentProgramId);
            
            CreateTable(
                "dbo.Account",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 50),
                        Uid = c.String(maxLength: 255),
                        FullName = c.String(maxLength: 255),
                        Email = c.String(maxLength: 255),
                        IsActive = c.Boolean(nullable: false),
                        AccountRoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Code)
                .ForeignKey("dbo.AccountRoles", t => t.AccountRoleId)
                .Index(t => t.AccountRoleId);
            
            CreateTable(
                "dbo.AccountRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Student",
                c => new
                    {
                        Code = c.String(nullable: false, maxLength: 50),
                        Phone = c.String(maxLength: 20),
                        WeightPoint = c.Int(),
                        LeaderCode = c.String(maxLength: 50),
                        Status = c.Int(nullable: false),
                        TopicPickId = c.Int(),
                        GradeStatus = c.Int(),
                        StudentGroupId = c.Int(),
                        ProgramId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Code)
                .ForeignKey("dbo.Student", t => t.LeaderCode)
                .ForeignKey("dbo.Program", t => t.ProgramId)
                .ForeignKey("dbo.StudentGroup", t => t.StudentGroupId)
                .ForeignKey("dbo.Account", t => t.Code, cascadeDelete: true)
                .Index(t => t.Code)
                .Index(t => t.LeaderCode)
                .Index(t => t.StudentGroupId)
                .Index(t => t.ProgramId);
            
            CreateTable(
                "dbo.Grade",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Note = c.String(),
                        Value = c.Double(),
                        GradeTypeId = c.Int(nullable: false),
                        StudentCode = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GradeType", t => t.GradeTypeId)
                .ForeignKey("dbo.Student", t => t.StudentCode)
                .Index(t => t.GradeTypeId)
                .Index(t => t.StudentCode);
            
            CreateTable(
                "dbo.GradeType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Percent = c.Double(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Program",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        ShortName = c.String(maxLength: 255),
                        DepartmentProgramId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DepartmentProgram", t => t.DepartmentProgramId)
                .Index(t => t.DepartmentProgramId);
            
            CreateTable(
                "dbo.DepartmentProgram",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Topic",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name_En = c.String(maxLength: 255),
                        Name_Vi = c.String(maxLength: 255),
                        ShortName = c.String(maxLength: 255),
                        Abstraction = c.String(),
                        Theory = c.String(),
                        MainFunction = c.String(),
                        OtherProducts = c.String(),
                        OtherComments = c.String(),
                        AbstractionHtml = c.String(),
                        TheoryHtml = c.String(),
                        MainFunctionHtml = c.String(),
                        OtherProductsHtml = c.String(),
                        OtherCommentsHtml = c.String(),
                        CreatedDate = c.DateTimeOffset(precision: 7),
                        ProgramId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Note = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Program", t => t.ProgramId)
                .Index(t => t.ProgramId);
            
            CreateTable(
                "dbo.AdvisorTopic",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TopicId = c.Int(nullable: false),
                        AdvisorId = c.String(nullable: false, maxLength: 50),
                        Role = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Advisor", t => t.AdvisorId)
                .ForeignKey("dbo.Topic", t => t.TopicId)
                .Index(t => t.TopicId)
                .Index(t => t.AdvisorId);
            
            CreateTable(
                "dbo.Comment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        TopicId = c.Int(nullable: false),
                        Deadline = c.DateTimeOffset(precision: 7),
                        IsApproved = c.Boolean(),
                        CommitteeId = c.String(maxLength: 50),
                        QuestionType = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Advisor", t => t.CommitteeId)
                .ForeignKey("dbo.Topic", t => t.TopicId)
                .Index(t => t.TopicId)
                .Index(t => t.CommitteeId);
            
            CreateTable(
                "dbo.StudentGroup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        EnrollTime = c.DateTimeOffset(nullable: false, precision: 7),
                        Duration = c.Time(nullable: false, precision: 7),
                        TopicId = c.Int(),
                        SemesterId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Semester", t => t.SemesterId)
                .ForeignKey("dbo.Topic", t => t.TopicId)
                .Index(t => t.TopicId)
                .Index(t => t.SemesterId);
            
            CreateTable(
                "dbo.Semester",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTimeOffset(precision: 7),
                        EndDate = c.DateTimeOffset(precision: 7),
                        Name = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TopicTechniques",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TopicId = c.Int(nullable: false),
                        TechniqueId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Technique", t => t.TechniqueId, cascadeDelete: true)
                .ForeignKey("dbo.Topic", t => t.TopicId, cascadeDelete: true)
                .Index(t => t.TopicId)
                .Index(t => t.TechniqueId);
            
            CreateTable(
                "dbo.Technique",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WeightPoint",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Point = c.Int(nullable: false),
                        Note = c.String(),
                        Deadline = c.DateTimeOffset(nullable: false, precision: 7),
                        StudentCode = c.String(maxLength: 50),
                        AdvisorId = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Advisor", t => t.AdvisorId)
                .ForeignKey("dbo.Student", t => t.StudentCode)
                .Index(t => t.StudentCode)
                .Index(t => t.AdvisorId);
            
            CreateTable(
                "dbo.DuplicatedTopic",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TopicId = c.Int(nullable: false),
                        DuplicatedTopicId = c.Int(nullable: false),
                        Percentage = c.Double(nullable: false),
                        Keywords = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Advisor", "DepartmentProgramId", "dbo.DepartmentProgram");
            DropForeignKey("dbo.Student", "Code", "dbo.Account");
            DropForeignKey("dbo.WeightPoint", "StudentCode", "dbo.Student");
            DropForeignKey("dbo.WeightPoint", "AdvisorId", "dbo.Advisor");
            DropForeignKey("dbo.Student", "StudentGroupId", "dbo.StudentGroup");
            DropForeignKey("dbo.Student", "ProgramId", "dbo.Program");
            DropForeignKey("dbo.TopicTechniques", "TopicId", "dbo.Topic");
            DropForeignKey("dbo.TopicTechniques", "TechniqueId", "dbo.Technique");
            DropForeignKey("dbo.StudentGroup", "TopicId", "dbo.Topic");
            DropForeignKey("dbo.StudentGroup", "SemesterId", "dbo.Semester");
            DropForeignKey("dbo.Topic", "ProgramId", "dbo.Program");
            DropForeignKey("dbo.Comment", "TopicId", "dbo.Topic");
            DropForeignKey("dbo.Comment", "CommitteeId", "dbo.Advisor");
            DropForeignKey("dbo.AdvisorTopic", "TopicId", "dbo.Topic");
            DropForeignKey("dbo.AdvisorTopic", "AdvisorId", "dbo.Advisor");
            DropForeignKey("dbo.Program", "DepartmentProgramId", "dbo.DepartmentProgram");
            DropForeignKey("dbo.Student", "LeaderCode", "dbo.Student");
            DropForeignKey("dbo.Grade", "StudentCode", "dbo.Student");
            DropForeignKey("dbo.Grade", "GradeTypeId", "dbo.GradeType");
            DropForeignKey("dbo.Advisor", "Code", "dbo.Account");
            DropForeignKey("dbo.Account", "AccountRoleId", "dbo.AccountRoles");
            DropIndex("dbo.WeightPoint", new[] { "AdvisorId" });
            DropIndex("dbo.WeightPoint", new[] { "StudentCode" });
            DropIndex("dbo.TopicTechniques", new[] { "TechniqueId" });
            DropIndex("dbo.TopicTechniques", new[] { "TopicId" });
            DropIndex("dbo.StudentGroup", new[] { "SemesterId" });
            DropIndex("dbo.StudentGroup", new[] { "TopicId" });
            DropIndex("dbo.Comment", new[] { "CommitteeId" });
            DropIndex("dbo.Comment", new[] { "TopicId" });
            DropIndex("dbo.AdvisorTopic", new[] { "AdvisorId" });
            DropIndex("dbo.AdvisorTopic", new[] { "TopicId" });
            DropIndex("dbo.Topic", new[] { "ProgramId" });
            DropIndex("dbo.Program", new[] { "DepartmentProgramId" });
            DropIndex("dbo.Grade", new[] { "StudentCode" });
            DropIndex("dbo.Grade", new[] { "GradeTypeId" });
            DropIndex("dbo.Student", new[] { "ProgramId" });
            DropIndex("dbo.Student", new[] { "StudentGroupId" });
            DropIndex("dbo.Student", new[] { "LeaderCode" });
            DropIndex("dbo.Student", new[] { "Code" });
            DropIndex("dbo.Account", new[] { "AccountRoleId" });
            DropIndex("dbo.Advisor", new[] { "DepartmentProgramId" });
            DropIndex("dbo.Advisor", new[] { "Code" });
            DropTable("dbo.DuplicatedTopic");
            DropTable("dbo.WeightPoint");
            DropTable("dbo.Technique");
            DropTable("dbo.TopicTechniques");
            DropTable("dbo.Semester");
            DropTable("dbo.StudentGroup");
            DropTable("dbo.Comment");
            DropTable("dbo.AdvisorTopic");
            DropTable("dbo.Topic");
            DropTable("dbo.DepartmentProgram");
            DropTable("dbo.Program");
            DropTable("dbo.GradeType");
            DropTable("dbo.Grade");
            DropTable("dbo.Student");
            DropTable("dbo.AccountRoles");
            DropTable("dbo.Account");
            DropTable("dbo.Advisor");
        }
    }
}
