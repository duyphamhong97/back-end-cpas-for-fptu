namespace DataAccess.Migrations
{
    using DataAccess.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataAccess.Database.CapstoneContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataAccess.Database.CapstoneContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            var topics = context.Topics.ToList();
            var advisor = context.Advisors.FirstOrDefault(_ => _.Code == "khanhkt");
            topics.ForEach(_ =>
            {
                context.AdvisorTopics.Add(new AdvisorTopic
                {
                    AdvisorId = advisor.Code,
                    Role = "Supervisor",
                    TopicId = _.Id,
                });
            });

            context.SaveChanges();
        }
    }
}
