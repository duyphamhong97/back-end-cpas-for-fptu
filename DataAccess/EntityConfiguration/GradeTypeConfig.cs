﻿namespace DataAccess.EntityConfiguration
{
    using DataAccess.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class GradeTypeConfig : EntityTypeConfiguration<GradeType>
    {
        public GradeTypeConfig()
        {
            ToTable("GradeType").HasKey(_ => _.Id);

            Property(p => p.Name).HasMaxLength(255).IsUnicode().IsOptional();
            Property(p => p.Percent).IsOptional();
        }
    }
}
