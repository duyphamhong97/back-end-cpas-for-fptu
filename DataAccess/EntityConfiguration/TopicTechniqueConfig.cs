﻿using DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.EntityConfiguration
{
    public class TopicTechniqueConfig : EntityTypeConfiguration<TopicTechnique>
    {
        public TopicTechniqueConfig()
        {
            ToTable("TopicTechnique").HasKey(k => k.Id);

            HasRequired(at => at.Technique)
               .WithMany(t => t.TopicTechniques)
               .HasForeignKey(pk => pk.TechniqueId)
               .WillCascadeOnDelete(true);

            HasRequired(at => at.Topic)
               .WithMany(t => t.TopicTechniques)
               .HasForeignKey(pk => pk.TopicId)
               .WillCascadeOnDelete(true);

        }
    }
}
