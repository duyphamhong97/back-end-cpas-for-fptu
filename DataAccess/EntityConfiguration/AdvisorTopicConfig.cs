﻿namespace DataAccess.EntityConfiguration
{
    using DataAccess.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class AdvisorTopicConfig : EntityTypeConfiguration<AdvisorTopic>
    {
        public AdvisorTopicConfig()
        {
            ToTable("AdvisorTopic").HasKey(k => k.Id);

            Property(p => p.Role).HasMaxLength(255).IsUnicode().IsOptional();

            HasRequired(at => at.Topic)
                .WithMany(t => t.AdvisorTopics)
                .HasForeignKey(pk => pk.TopicId)
                .WillCascadeOnDelete(false);

            HasRequired(at => at.Advisor)
                .WithMany(t => t.AdvisorTopics)
                .HasForeignKey(pk => pk.AdvisorId)
                .WillCascadeOnDelete(false);


        }
    }
}
