﻿namespace DataAccess.EntityConfiguration
{
    using DataAccess.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class DepartmentProgramConfig : EntityTypeConfiguration<DepartmentProgram>
    {
        public DepartmentProgramConfig()
        {
            ToTable("DepartmentProgram").HasKey(_ => _.Id);

            Property(p => p.Name).HasMaxLength(255).IsUnicode().IsOptional();

            //HasMany(_ => _.Programs)
            //    .WithRequired(_ => _.DepartmentProgram)
            //    .HasForeignKey(_ => _.DepartmentProgramId)
            //    .WillCascadeOnDelete(true);

        }
    }
}
