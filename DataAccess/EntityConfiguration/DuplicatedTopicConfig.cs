﻿namespace DataAccess.EntityConfiguration
{
    using DataAccess.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class DuplicatedTopicConfig : EntityTypeConfiguration<DuplicatedTopic>
    {
        public DuplicatedTopicConfig()
        {
            ToTable("DuplicatedTopic").HasKey(_ => _.Id);

            Property(p => p.Keywords).IsMaxLength().IsUnicode().IsOptional();
        }
    }
}
