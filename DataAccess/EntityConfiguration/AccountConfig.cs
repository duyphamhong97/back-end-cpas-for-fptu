﻿namespace DataAccess.EntityConfiguration
{
    using DataAccess.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class AccountConfig : EntityTypeConfiguration<Account>
    {
        public AccountConfig()
        {
            ToTable("Account").HasKey(_ => _.Code);

            Property(p => p.Code).HasMaxLength(50);
            Property(p => p.Uid).HasMaxLength(255).IsOptional();
            Property(p => p.FullName).HasMaxLength(255).IsUnicode().IsOptional();
            Property(p => p.Email).HasMaxLength(255).IsOptional();

            HasRequired(s => s.AccountRole)
                .WithMany(z => z.Accounts)
                .HasForeignKey(s => s.AccountRoleId)
                .WillCascadeOnDelete(false);

            // Advisor account + student account config

            HasOptional(a => a.Advisor)
                .WithRequired(r => r.Account)
                .WillCascadeOnDelete(true);

            HasOptional(a => a.Student)
                .WithRequired(r => r.Account)
                .WillCascadeOnDelete(true);
        }
    }
}
