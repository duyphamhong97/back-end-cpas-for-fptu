﻿namespace DataAccess.EntityConfiguration
{
    using DataAccess.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class AdvisorConfig : EntityTypeConfiguration<Advisor>
    {
        public AdvisorConfig()
        {
            ToTable("Advisor").HasKey(k => k.Code);

            Property(p => p.Code).HasMaxLength(50);
            Property(p => p.Title).HasMaxLength(50).IsUnicode().IsOptional();
            Property(p => p.Phone).HasMaxLength(50).IsOptional();

            HasOptional(_ => _.DepartmentProgram)
                .WithMany(_ => _.Advisors)
                .HasForeignKey(_ => _.DepartmentProgramId)
                .WillCascadeOnDelete(false);
        }
    }
}
