﻿namespace DataAccess.EntityConfiguration
{
    using DataAccess.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class TopicConfig : EntityTypeConfiguration<Topic>
    {
        public TopicConfig()
        {
            ToTable("Topic").HasKey(k => k.Id);

            Property(p => p.Abstraction).IsMaxLength().IsUnicode().IsOptional();
            Property(p => p.AbstractionHtml).IsMaxLength().IsUnicode().IsOptional();
            Property(p => p.MainFunction).IsMaxLength().IsUnicode().IsOptional();
            Property(p => p.MainFunctionHtml).IsMaxLength().IsUnicode().IsOptional();
            Property(p => p.Theory).IsMaxLength().IsUnicode().IsOptional();
            Property(p => p.TheoryHtml).IsMaxLength().IsUnicode().IsOptional();
            Property(p => p.OtherComments).IsMaxLength().IsUnicode().IsOptional();
            Property(p => p.OtherCommentsHtml).IsMaxLength().IsUnicode().IsOptional();
            Property(p => p.OtherProducts).IsMaxLength().IsUnicode().IsOptional();
            Property(p => p.OtherProductsHtml).IsMaxLength().IsUnicode().IsOptional();
            Property(p => p.Name_En).HasMaxLength(255).IsUnicode().IsOptional();
            Property(p => p.Name_Vi).HasMaxLength(255).IsUnicode().IsOptional();
            Property(p => p.ShortName).HasMaxLength(255).IsUnicode().IsOptional();
            Property(p => p.CreatedDate).IsOptional();

            Property(p => p.Note).IsMaxLength().IsUnicode().IsOptional();
            

            HasRequired(at => at.Program)
               .WithMany(t => t.Topics)
               .HasForeignKey(pk => pk.ProgramId)
               .WillCascadeOnDelete(false);

        }

    }
}
