﻿using DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.EntityConfiguration
{
    public class GradeConfig : EntityTypeConfiguration<Grade>
    {
        public GradeConfig()
        {
            ToTable("Grade").HasKey(_ => _.Id);

            Property(p => p.Value).IsOptional();
            Property(p => p.Note).IsMaxLength().IsUnicode().IsOptional();

            HasRequired(at => at.Student)
                .WithMany(t => t.Grades)
                .HasForeignKey(pk => pk.StudentCode)
                .WillCascadeOnDelete(false);

            HasRequired(at => at.GradeType)
                .WithMany(t => t.Grades)
                .HasForeignKey(pk => pk.GradeTypeId)
                .WillCascadeOnDelete(false);


        }
    }
}
