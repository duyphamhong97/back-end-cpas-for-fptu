﻿namespace DataAccess.EntityConfiguration
{
    using DataAccess.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class TechniqueConfig : EntityTypeConfiguration<Technique>
    {
        public TechniqueConfig()
        {
            ToTable("Technique").HasKey(k => k.Id);

            Property(p => p.Name).HasMaxLength(255).IsUnicode().IsOptional();

        }
    }
}
