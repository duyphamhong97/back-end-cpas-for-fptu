﻿namespace DataAccess.EntityConfiguration
{
    using DataAccess.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class CommentConfig : EntityTypeConfiguration<Comment>
    {
        public CommentConfig()
        {
            ToTable("Comment").HasKey(_ => _.Id);

            Property(p => p.Content).IsMaxLength().IsUnicode().IsOptional();
            Property(p => p.Deadline).IsOptional();
            Property(p => p.QuestionType).IsOptional();
            Property(p => p.IsApproved).IsOptional();


            HasRequired(at => at.Topic)
                .WithMany(t => t.Comments)
                .HasForeignKey(pk => pk.TopicId)
                .WillCascadeOnDelete(false);

            HasOptional(at => at.Committee)
                .WithMany(t => t.Comments)
                .HasForeignKey(pk => pk.CommitteeId)
                .WillCascadeOnDelete(false);

        }
    }
}
