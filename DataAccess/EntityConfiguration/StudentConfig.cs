﻿using DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.EntityConfiguration
{
    public class StudentConfig : EntityTypeConfiguration<Student>
    {
        public StudentConfig()
        {
            ToTable("Student").HasKey(_ => _.Code);

            Property(p => p.Code).HasMaxLength(50);
            Property(p => p.Phone).HasMaxLength(20).IsOptional();
            Property(p => p.WeightPoint).IsOptional();  
            Property(p => p.TopicPickId).IsOptional();  
            Property(p => p.GradeStatus).IsOptional();  

            HasOptional(at => at.Leader)
                .WithMany()
                .HasForeignKey(pk => pk.LeaderCode)
                .WillCascadeOnDelete(false);

            HasOptional(at => at.StudentGroup)
                .WithMany(t => t.Students)
                .HasForeignKey(pk => pk.StudentGroupId)
                .WillCascadeOnDelete(false);

            HasRequired(at => at.Program)
                .WithMany(t => t.Students)
                .HasForeignKey(pk => pk.ProgramId)
                .WillCascadeOnDelete(false);
            
        }
    }
}