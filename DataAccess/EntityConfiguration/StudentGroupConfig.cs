﻿namespace DataAccess.EntityConfiguration
{
    using DataAccess.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class StudentGroupConfig : EntityTypeConfiguration<StudentGroup>
    {
        public StudentGroupConfig()
        {
            ToTable("StudentGroup").HasKey(_ => _.Id);

            HasOptional(at => at.Topic)
                .WithMany(t => t.StudentGroups)
                .HasForeignKey(pk => pk.TopicId)
                .WillCascadeOnDelete(false);

            HasRequired(at => at.Semester)
                .WithMany(t => t.StudentGroups)
                .HasForeignKey(pk => pk.SemesterId)
                .WillCascadeOnDelete(false);
        }
    }
}
