﻿namespace DataAccess.EntityConfiguration
{
    using DataAccess.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class ProgramConfig : EntityTypeConfiguration<Program>
    {
        public ProgramConfig()
        {
            ToTable("Program").HasKey(k => k.Id);

            Property(p => p.Name).HasMaxLength(255).IsUnicode().IsOptional();
            Property(p => p.ShortName).HasMaxLength(255).IsUnicode().IsOptional();

            HasRequired(a => a.DepartmentProgram)
                .WithMany(b => b.Programs)
                .HasForeignKey(a => a.DepartmentProgramId)
                .WillCascadeOnDelete(false);
        }

    }
}
