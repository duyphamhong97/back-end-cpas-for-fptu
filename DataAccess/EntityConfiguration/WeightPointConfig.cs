﻿using DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.EntityConfiguration
{
    public class WeightPointConfig : EntityTypeConfiguration<WeightPoint>
    {
        public WeightPointConfig()
        {
            ToTable("WeightPoint").HasKey(_ => _.Id);

            Property(p => p.Note).IsMaxLength().IsUnicode().IsOptional();

            HasOptional(p => p.Student)
                .WithMany(s => s.WeightPoints)
                .HasForeignKey(p => p.StudentCode)
                .WillCascadeOnDelete(false);

            HasOptional(p => p.Advisor)
                .WithMany(s => s.WeightPoints)
                .HasForeignKey(p => p.AdvisorId)
                .WillCascadeOnDelete(false);
        }
    }
}
