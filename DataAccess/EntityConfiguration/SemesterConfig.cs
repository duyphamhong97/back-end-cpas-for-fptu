﻿using DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.EntityConfiguration
{
    public class SemesterConfig : EntityTypeConfiguration<Semester>
    {
        public SemesterConfig()
        {
            ToTable("Semester").HasKey(_ => _.Id);

            Property(p => p.Name).HasMaxLength(255).IsUnicode().IsOptional();

            Property(p => p.StartDate).IsOptional();
            Property(p => p.EndDate).IsOptional();
        }
    }
}