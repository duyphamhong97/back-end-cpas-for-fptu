﻿namespace DataAccess.EntityConfiguration
{
    using DataAccess.Entities;
    using System.Data.Entity.ModelConfiguration;

    public class AccountRoleConfig : EntityTypeConfiguration<AccountRole>
    {
        public AccountRoleConfig()
        {
            ToTable("AccountRole").HasKey(_ => _.Id);

            Property(p => p.Name).HasMaxLength(255).IsOptional();
        }
    }
}
