﻿using System.Collections.Generic;
using System.Configuration;

namespace DataAccess.Resources
{
    public static class CommitteeQuestion
    {
        public static IDictionary<int, QuestionType> QUESTIONS = new Dictionary<int, QuestionType>
        {
            { 1, new QuestionType {
                Question = ConfigurationManager.AppSettings.Get("CommitteeQuestion1"),
                Weight = double.Parse(ConfigurationManager.AppSettings.Get("CQValue1"))
            } },
            { 2, new QuestionType {
                Question = ConfigurationManager.AppSettings.Get("CommitteeQuestion2"),
                Weight = double.Parse(ConfigurationManager.AppSettings.Get("CQValue2"))
            } },
            { 3, new QuestionType {
                Question = ConfigurationManager.AppSettings.Get("CommitteeQuestion3"),
                Weight = double.Parse(ConfigurationManager.AppSettings.Get("CQValue3"))
            } },
            { 4, new QuestionType {
                Question = ConfigurationManager.AppSettings.Get("CommitteeQuestion4"),
                Weight = double.Parse(ConfigurationManager.AppSettings.Get("CQValue4"))
            } },
            { 5, new QuestionType {
                Question = ConfigurationManager.AppSettings.Get("CommitteeQuestion5"),
                Weight = double.Parse(ConfigurationManager.AppSettings.Get("CQValue5"))
            } }
        };
    }

    public class QuestionType
    {
        public string Question { get; set; }
        public double Weight { get; set; }
    }
}
