﻿namespace DataAccess.Entities
{
    using System;

    public class WeightPoint : _BaseEntity
    {
        public int Point { get; set; }
        public string Note { get; set; }
        public DateTimeOffset Deadline { get; set; }
        public string StudentCode { get; set; }
        public Student Student { get; set; }

        public string AdvisorId { get; set; }
        public Advisor Advisor { get; set; }
    }
}
