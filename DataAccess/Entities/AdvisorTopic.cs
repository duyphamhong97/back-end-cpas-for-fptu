﻿namespace DataAccess.Entities
{
    public class AdvisorTopic : _BaseEntity
    {
        public Topic Topic { get; set; }

        public int TopicId { get; set; }

        public Advisor Advisor { get; set; }

        public string AdvisorId { get; set; }

        public string Role { get; set; }

    }
}
