﻿namespace DataAccess.Entities
{
    using System.Collections.Generic;

    public class GradeType : _BaseEntity
    {
        public string Name { get; set; }
        public double Percent { get; set; }
        public ICollection<Grade> Grades { get; set; }
    }
}
