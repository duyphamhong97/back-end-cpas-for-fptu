﻿namespace DataAccess.Entities
{
    using System.Collections.Generic;

    public class DepartmentProgram : _BaseEntity
    {
        public string Name { get; set; }

        public ICollection<Program> Programs { get; set; }
        public ICollection<Advisor> Advisors { get; set; }
    }
}
