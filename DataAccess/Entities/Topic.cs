﻿namespace DataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Topic : _BaseEntity
    {
        public string Name_En { get; set; }

        public string Name_Vi { get; set; }

        public string ShortName { get; set; }

        public string Abstraction { get; set; }

        public string Theory { get; set; }

        public string MainFunction { get; set; }

        public string OtherProducts { get; set; }

        public string OtherComments { get; set; }

        public string AbstractionHtml { get; set; }

        public string TheoryHtml { get; set; }

        public string MainFunctionHtml { get; set; }

        public string OtherProductsHtml { get; set; }

        public string OtherCommentsHtml { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public ICollection<TopicTechnique> TopicTechniques { get; set; }

        public ICollection<AdvisorTopic> AdvisorTopics { get; set; }

        public ICollection<StudentGroup> StudentGroups { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public int ProgramId { get; set; }
        public Program Program { get; set; }
        public Status Status { get; set; }

        public string Note { get; set; }
    }

    public enum Status
    {
        Draft = 1,
        Pending = 2,
        DuplicateRejected = 3,
        DuplicateApproved = 4,
        ValidateRejected = 5,
        ValidateApproved = 6,
        Unpublished = 7,
        Published = 8,
        Taken = 9,
        Passed = 10,
        Failed = 11,
        ValidatePending = 12,
        PublishPending = 13,
        Disable = 14,
        Available = 15,
        Rewrite = 16, // Advisor receive topic from DH to rewrite
    }
}
