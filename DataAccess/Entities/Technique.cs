﻿namespace DataAccess.Entities
{
    using System.Collections.Generic;

    public class Technique : _BaseEntity
    {
        public string Name { get; set; }

        public ICollection<TopicTechnique> TopicTechniques { get; set; }
    }
}
