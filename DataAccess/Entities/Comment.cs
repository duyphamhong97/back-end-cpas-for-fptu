﻿using System;

namespace DataAccess.Entities
{
    public class Comment : _BaseEntity
    {
      
        public string Content { get; set; }
        public int TopicId { get; set; }
        public Topic Topic { get; set; }
        public DateTimeOffset Deadline { get; set; }
        public bool IsApproved { get; set; }
        public string CommitteeId { get; set; }
        public Advisor Committee { get; set; }
        public int QuestionType { get; set; }

    }
}
