﻿using System;
using System.Collections.Generic;

namespace DataAccess.Entities
{
    public class StudentGroup : _BaseEntity
    {
        public string Name { get; set; }
        public DateTimeOffset EnrollTime { get; set; }
        public TimeSpan Duration { get; set; }
        public int? TopicId { get; set; }
        public Topic Topic { get; set; }
        public int SemesterId { get; set; }
        public Semester Semester { get; set; }
        public ICollection<Student> Students { get; set; }
    }
}
