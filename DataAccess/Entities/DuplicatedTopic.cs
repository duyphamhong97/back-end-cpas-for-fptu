﻿namespace DataAccess.Entities
{
    public class DuplicatedTopic : _BaseEntity
    {
        public int TopicId { get; set; }
        public int DuplicatedTopicId { get; set; }
        public double Percentage { get; set; }
        public string Keywords { get; set; }
    }
}
