﻿namespace DataAccess.Entities
{
    using System.Collections.Generic;

    public class Program : _BaseEntity
    {
        public string Name { get; set; }

        public string ShortName { get; set; }

        public int DepartmentProgramId { get; set; }
        public DepartmentProgram DepartmentProgram { get; set; }

        public ICollection<Topic> Topics { get; set; }
        public ICollection<Student> Students { get; set; }
    }
}
