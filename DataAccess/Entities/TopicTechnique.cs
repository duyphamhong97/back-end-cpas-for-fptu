﻿namespace DataAccess.Entities
{
    using System;

    public class TopicTechnique : IEquatable<TopicTechnique>
    {
        public int Id { get; set; }
        public int TopicId { get; set; }
        public Topic Topic { get; set; }
        public int TechniqueId { get; set; }
        public Technique Technique { get; set; }

        public bool Equals(TopicTechnique other)
            => this.TopicId == other.TopicId 
            && this.TechniqueId == other.TechniqueId;
    }
}