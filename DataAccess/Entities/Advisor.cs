﻿namespace DataAccess.Entities
{
    using System.Collections.Generic;

    public class Advisor
    {
        public string Code { get; set; }

        public string Title { get; set; }
        public string Phone { get; set; }

        public ICollection<AdvisorTopic> AdvisorTopics { get; set; }

        public int? DepartmentProgramId { get; set; }
        public DepartmentProgram DepartmentProgram { get; set; }

        public ICollection<Comment> Comments { get; set; }
        public ICollection<WeightPoint> WeightPoints { get; set; }
        public Account Account { get; set; }
    }
}
