﻿namespace DataAccess.Entities
{
    public class Grade : _BaseEntity
    {
        public string Note { get; set; }
        public double Value { get; set; }
        public int GradeTypeId { get; set; }
        public GradeType GradeType { get; set; }
        public string StudentCode { get; set; }
        public Student Student { get; set; }

    }
}
