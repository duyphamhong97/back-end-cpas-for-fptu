﻿using System;
using System.Collections.Generic;

namespace DataAccess.Entities
{

    public class Semester : _BaseEntity
    {
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public string Name { get; set; }
        public ICollection<StudentGroup> StudentGroups { get; set; }
    }
}