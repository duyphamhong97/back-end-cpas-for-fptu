﻿namespace DataAccess.Entities
{
    public class Account 
    {
        public string Code { get; set; }
        public string Uid { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }

        // Role 
        public int AccountRoleId { get; set; }
        public AccountRole AccountRole { get; set; }

        // Advisor
        public Advisor Advisor { get; set; }
        public Student Student { get; set; }

    }
    
}
