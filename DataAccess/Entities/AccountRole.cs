﻿namespace DataAccess.Entities
{
    using System.Collections.Generic;

    public class AccountRole : _BaseEntity
    {
        public string Name { get; set; }

        // Account
        public ICollection<Account> Accounts { get; set; }
    }
}
