﻿using System.Collections.Generic;

namespace DataAccess.Entities
{
    public class Student
    {
        public string Code { get; set; }

        public string Phone { get; set; }
        public double WeightPoint { get; set; }
        public string LeaderCode { get; set; }
        public Student Leader { get; set; }
        public ICollection<Grade> Grades { get; set; }
        public StudentStatus Status { get; set; }
        public int? TopicPickId { get; set; }

        public GradeStatus? GradeStatus { get; set; }
        public int? StudentGroupId { get; set; }
        public StudentGroup StudentGroup { get; set; }

        public int ProgramId { get; set; }
        public Program Program { get; set; }

        public ICollection<WeightPoint> WeightPoints { get; set; }
        public Account Account { get; set; }
    }

    public enum StudentStatus
    {
        New = 1,
        Grouping = 2, // Send to department head
        PointPending = 3, // Send to advisor
        PointDeadline = 4, // Advisor set point done 
        PointDone = 5, // Department Head approve weight point
        GroupDone = 6, // Unchangeable
        TopicTaken = 7,
        Doing = 8,
        Passed = 9,
        Disable = 10
    }

    public enum GradeStatus
    {
        Studying = 1,
        Passed = 2,
        Failed = 3,
        PrepareToDefend = 4,
        Resit = 5, // Đang bv lại
    }
}